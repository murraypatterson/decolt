#include "basicRoutines.h"
#include "fonctionsCout.h"
#include "parcoursArriere.h"
#include "Reconcil.h"

// auxiliary routine containing a set of steps used by many routines below
void setNodeProps(Node * &n, string prop, Node * v1, Node * v2, bool v1dansA1) {

  n->setId(id_arbres_adj);
  id_arbres_adj++;
  BppString * Espece = dynamic_cast<BppString*> (v1->getNodeProperty(esp));
  n->setNodeProperty(esp,*Espece);
  BppString * TimeSlice = dynamic_cast<BppString*> (v1->getNodeProperty(tsl));
  n->setNodeProperty(tsl,*TimeSlice);
  n->setBranchProperty(typ,BppString(prop));
  n->setBranchProperty("D",BppString("?"));

  // branch will contain (id1 : id2)
  convert<int> c;
  string branch = "(" + c.to_s(v1->getId()) + " : " + c.to_s(v2->getId()) + ")";
  n->setBranchProperty(brn,BppString(branch));
  string bootie = "(" + getProperty(v1,bsv,true) + " : " + getProperty(v2,bsv,true) + ")";
  n->setBranchProperty(bsv,BppString(bootie));

  string nom;
  if(prop == bk) nom = NomCass+sep;
  else if(prop == per) nom = NomPer+sep;
  else if(prop == Aper) nom = NomPerA+sep;

  if (v1dansA1) nom += c.to_s(v1->getId())+sepAdj+c.to_s(v2->getId());
  else nom += c.to_s(v2->getId())+sepAdj+c.to_s(v1->getId());

  n->setName(nom);
}

// for a break event (creates a leaf node labeled with 'break')
Node * parcoursArriereCassure(Node * v1, Node * v2, bool v1dansA1) {
  nb_Bk++;

  if(affich_ParcoursArriere) cout <<"Je suis dans parcoursArriereCassure"<<",id v1="<<v1->getId()<<",id v2="<<v2->getId()<<endl;

  Node * n = new Node();
  setNodeProps(n,bk,v1,v2,v1dansA1);

  return n;
}

// updating the co-transfer count, etc., when a co-transfer occurs
void addCoTrans(Node * v1, Node * v2, bool v1dansA1) {
  nb_ATrf++; // update co-transfer count

  // for tracing genes that transferred together
  convert<int> c;
  adjacence a;
  if(v1dansA1) {
    a.gene1=c.to_s(v1->getId());
    a.gene2=c.to_s(v2->getId());
  }
  else {
    a.gene1=c.to_s(v2->getId());
    a.gene2=c.to_s(v1->getId());
  }
  TRF.push_back(a);
}

//
// cases for C1
//

// 1
Node * parcoursArriereC1ExtantWithExtant(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1) {

  if(affich_ParcoursArriere) cout <<"Je suis dans parcoursArriereC1ExtantWithExtant"<<",id v1="<<v1->getId()<<",id v2="<<v2->getId()<<endl;

  Node * n = new Node();
  n->setId(id_arbres_adj);
  id_arbres_adj++;
  BppString * Espece = dynamic_cast<BppString*> (v1->getNodeProperty(esp));
  n->setNodeProperty(esp,*Espece);
  BppString * TimeSlice = dynamic_cast<BppString*> (v1->getNodeProperty(tsl));
  n->setNodeProperty(tsl,*TimeSlice);

  // branch will contain (id1 : id2)
  convert<int> c;
  string branch = "(" + c.to_s(v1->getId()) + " : " + c.to_s(v2->getId()) + ")";
  n->setBranchProperty(brn,BppString(branch));
   
  string nomV1 = v1->getName();
  string nomV2 = v2->getName();
  string nom_gene1=nomV1.substr(0,nomV1.rfind(sep,nomV1.length()-1));
  string nom_gene2=nomV2.substr(0,nomV2.rfind(sep,nomV2.length()-1));
  string espece=genes_especes[nom_gene1]; //nomV2.substr(nomV2.rfind(sep,nomV2.length())+1,nomV2.length());

  // bootie will contain (name1 : name2)
  string bootie = "(" + nomV1 + " : " + nomV2 + ")";
  n->setBranchProperty(bsv,BppString(bootie));

  //On cherche d'adj dans les deux sens g1-g2 ou g2-g1
  adjacence a12,a21;
  a12.gene1=nom_gene1;
  a12.gene2=nom_gene2;
  a21.gene1=nom_gene2;
  a21.gene2=nom_gene1;

  if (appAdj(a12,Adj_classe) || appAdj(a21,Adj_classe)) {
    n->setBranchProperty(typ,BppString(ga));
    n->setBranchProperty("D", BppString("?"));
    if (v1dansA1) n->setName(nom_gene1+sepAdj+nom_gene2+sep+espece);
    else n->setName(nom_gene2+sepAdj+nom_gene1+sep+espece);
  }
  else {
    //IMPOSSIBLE AVEC CO�T INFINI SAUF SI ARBRE R�DUIT � UNE FEUILLE
    // n->setBranchProperty(typ,BppString(bk));
    // n->setBranchProperty("D", BppString("?"));
    // n->setName(NomCass+sep+espece);
    n->setBranchProperty(typ,BppString("IMPOSSIBLE"));
    n->setBranchProperty("D", BppString("?"));
    n->setName("IMPOSSIBLE"+sep+espece);
  }

  return n;
}

// 2
Node * parcoursArriereC1GLosWithAny(Node * v1, Node * v2, vector<adjacence> * Adj_classe,vector<Tree *> * ArbresDAdjacences, bool v1dansA1) {

  if(affich_ParcoursArriere) cout <<"Je suis dans parcoursArriereC1GLosWithAny"<<",id v1="<<v1->getId()<<",id v2="<<v2->getId()<<endl;

  Node * n = new Node();

  setNodeProps(n,per,v1,v2,v1dansA1);
  
  return n;
}

// 3
Node * parcoursArriereC1GLosWithGLos(Node * v1, Node * v2, vector<adjacence> * Adj_classe,vector<Tree *> * ArbresDAdjacences, bool v1dansA1) {

  if(affich_ParcoursArriere) cout <<"Je suis dans parcoursArriereC1GLosWithGLos"<<",id v1="<<v1->getId()<<",id v2="<<v2->getId()<<endl;

  Node * n = new Node();
  setNodeProps(n,Aper,v1,v2,v1dansA1);

  return n;
}

// D1
Node * parcoursArriereD1(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1) {

  Node * n = new Node();
  float C = recupCoutC1(v1,v2);

  if(C==recupCoutC1(v1->getSon(0),v2)+recupCoutC0(v1->getSon(1),v2)) {	 
    n->addSon(0,parcoursArriereC1(v1->getSon(0),v2,Adj_classe,ArbresDAdjacences, v1dansA1));
    parcoursArriereC0(v1->getSon(1),v2,Adj_classe,ArbresDAdjacences, v1dansA1);
  }
  else if(C==recupCoutC0(v1->getSon(0),v2)+recupCoutC1(v1->getSon(1),v2)) {
    n->addSon(0,parcoursArriereC1(v1->getSon(1),v2,Adj_classe,ArbresDAdjacences, v1dansA1));
    parcoursArriereC0(v1->getSon(0),v2,Adj_classe,ArbresDAdjacences, v1dansA1);
  }
  else if(C==recupCoutC1(v1->getSon(0),v2)+recupCoutC1(v1->getSon(1),v2)+Crea) {	 
    n->addSon(0,parcoursArriereC1(v1->getSon(0),v2,Adj_classe,ArbresDAdjacences, v1dansA1));
    nb_Crea++;
    TreeTemplate<Node> * T = new TreeTemplate<Node>();
    T->setRootNode(parcoursArriereC1(v1->getSon(1),v2,Adj_classe,ArbresDAdjacences, v1dansA1));
    ArbresDAdjacences->push_back(T);
  }
  else if(C==recupCoutC0(v1->getSon(0),v2)+recupCoutC0(v1->getSon(1),v2)+Break) {	 
    n->addSon(0,parcoursArriereCassure(v1->getSon(0),v2, v1dansA1));
    parcoursArriereC0(v1->getSon(0),v2,Adj_classe,ArbresDAdjacences, v1dansA1);
    parcoursArriereC0(v1->getSon(1),v2,Adj_classe,ArbresDAdjacences, v1dansA1);
  }

  return n;
}

// 4
Node * parcoursArriereC1GDupWithBTClass(Node * v1, Node * v2, vector<adjacence> * Adj_classe,vector<Tree *> * ArbresDAdjacences, bool v1dansA1) {

  if(affich_ParcoursArriere) cout <<"Je suis dans parcoursArriereC1GDupWithBTClass"<<",id v1="<<v1->getId()<<",id v2="<<v2->getId()<<endl;

  Node * n = parcoursArriereD1(v1, v2, Adj_classe, ArbresDAdjacences, v1dansA1);

  if(n->getNumberOfSons() == 0) cout << "PB dans parcoursArriereC1GDupWithBTClass : aucun des 4 couts reconnus" << endl;

  nb_GDup++;
  setNodeProps(n,dupl,v1,v2,v1dansA1);
  n->setBranchProperty("D", BppString("T"));

  return n;
}

// D2
Node * parcoursArriereD2(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1) {

  Node * n = new Node();
  float C = recupCoutC1(v1,v2);

  if(C==recupCoutC1(v1,v2->getSon(0))+recupCoutC0(v1,v2->getSon(1))) {
    n->addSon(0,parcoursArriereC1(v1,v2->getSon(0),Adj_classe,ArbresDAdjacences, v1dansA1));
    parcoursArriereC0(v1,v2->getSon(1),Adj_classe,ArbresDAdjacences, v1dansA1);
  }
  else if(C==recupCoutC0(v1,v2->getSon(0))+recupCoutC1(v1,v2->getSon(1))) {
    n->addSon(0,parcoursArriereC1(v1,v2->getSon(1),Adj_classe,ArbresDAdjacences, v1dansA1));
    parcoursArriereC0(v1,v2->getSon(0),Adj_classe,ArbresDAdjacences, v1dansA1);
  }
  else if(C==recupCoutC1(v1,v2->getSon(0))+recupCoutC1(v1,v2->getSon(1))+Crea) {
    n->addSon(0,parcoursArriereC1(v1,v2->getSon(0),Adj_classe,ArbresDAdjacences, v1dansA1));
    nb_Crea++;
    TreeTemplate<Node> * T = new TreeTemplate<Node>();
    T->setRootNode(parcoursArriereC1(v1,v2->getSon(1),Adj_classe,ArbresDAdjacences, v1dansA1));
    ArbresDAdjacences->push_back(T);
  }
  else if(C==recupCoutC0(v1,v2->getSon(0))+recupCoutC0(v1,v2->getSon(1))+Break) {
    n->addSon(0,parcoursArriereCassure(v1,v2->getSon(0), v1dansA1));
    parcoursArriereC0(v1,v2->getSon(0),Adj_classe,ArbresDAdjacences, v1dansA1);
    parcoursArriereC0(v1,v2->getSon(1),Adj_classe,ArbresDAdjacences, v1dansA1);
  }

  return n;
}

// D12
Node * parcoursArriereD12(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1) {

  //Pour tracer les g�nes qui se dupliquent ensemble :
  convert<int> c;
  adjacence a;
  if(v1dansA1) {
    a.gene1=c.to_s(v1->getId());
    a.gene2=c.to_s(v2->getId());
  }
  else {
    a.gene1=c.to_s(v2->getId());
    a.gene2=c.to_s(v1->getId());
  }
  DUP.push_back(a);

  Node * n = new Node();
  float C = recupCoutC1(v1,v2);

  // now, the 16 cases ...

  // (1)
  if(C==recupCoutC1(v1->getSon(0),v2->getSon(0))+recupCoutC1(v1->getSon(1),v2->getSon(1))+
     recupCoutC0(v1->getSon(0),v2->getSon(1))+recupCoutC0(v1->getSon(1),v2->getSon(0))) {
    n->addSon(0,parcoursArriereC1(v1->getSon(0),v2->getSon(0),Adj_classe,ArbresDAdjacences, v1dansA1));
    n->addSon(1,parcoursArriereC1(v1->getSon(1),v2->getSon(1),Adj_classe,ArbresDAdjacences, v1dansA1));
    parcoursArriereC0(v1->getSon(0),v2->getSon(1),Adj_classe,ArbresDAdjacences, v1dansA1);
    parcoursArriereC0(v1->getSon(1),v2->getSon(0),Adj_classe,ArbresDAdjacences, v1dansA1);
  }
  // (2)
  else if(C==recupCoutC1(v1->getSon(0),v2->getSon(0))+recupCoutC1(v1->getSon(1),v2->getSon(1))+
	  recupCoutC0(v1->getSon(0),v2->getSon(1))+recupCoutC1(v1->getSon(1),v2->getSon(0))+Crea) {
    n->addSon(0,parcoursArriereC1(v1->getSon(0),v2->getSon(0),Adj_classe,ArbresDAdjacences, v1dansA1));
    n->addSon(1,parcoursArriereC1(v1->getSon(1),v2->getSon(1),Adj_classe,ArbresDAdjacences, v1dansA1));
    parcoursArriereC0(v1->getSon(0),v2->getSon(1),Adj_classe,ArbresDAdjacences, v1dansA1);
    //Cr�ation
    nb_Crea++;
    TreeTemplate<Node> * T = new TreeTemplate<Node>();
    T->setRootNode(parcoursArriereC1(v1->getSon(1),v2->getSon(0),Adj_classe,ArbresDAdjacences, v1dansA1));
    ArbresDAdjacences->push_back(T);
  }
  // (3)
  else if(C==recupCoutC1(v1->getSon(0),v2->getSon(0))+recupCoutC1(v1->getSon(1),v2->getSon(1))+
	  recupCoutC1(v1->getSon(0),v2->getSon(1))+recupCoutC0(v1->getSon(1),v2->getSon(0))+Crea) {
    n->addSon(0,parcoursArriereC1(v1->getSon(0),v2->getSon(0),Adj_classe,ArbresDAdjacences, v1dansA1));
    n->addSon(1,parcoursArriereC1(v1->getSon(1),v2->getSon(1),Adj_classe,ArbresDAdjacences, v1dansA1));
    //Cr�ation
    nb_Crea++;
    TreeTemplate<Node> * T = new TreeTemplate<Node>();
    T->setRootNode(parcoursArriereC1(v1->getSon(0),v2->getSon(1),Adj_classe,ArbresDAdjacences, v1dansA1));
    ArbresDAdjacences->push_back(T);
    parcoursArriereC0(v1->getSon(1),v2->getSon(0),Adj_classe,ArbresDAdjacences, v1dansA1);
  }
  // (4)
  else if(C==recupCoutC1(v1->getSon(0),v2->getSon(0))+recupCoutC1(v1->getSon(1),v2->getSon(1))+
	  recupCoutC1(v1->getSon(0),v2->getSon(1))+recupCoutC1(v1->getSon(1),v2->getSon(0))+2*Crea) {
    n->addSon(0,parcoursArriereC1(v1->getSon(0),v2->getSon(0),Adj_classe,ArbresDAdjacences, v1dansA1));
    n->addSon(1,parcoursArriereC1(v1->getSon(1),v2->getSon(1),Adj_classe,ArbresDAdjacences, v1dansA1));
    //Cr�ation
    nb_Crea+=2;
    TreeTemplate<Node> * T1 = new TreeTemplate<Node>();
    T1->setRootNode(parcoursArriereC1(v1->getSon(0),v2->getSon(1),Adj_classe,ArbresDAdjacences, v1dansA1));
    ArbresDAdjacences->push_back(T1);
    //Cr�ation
    TreeTemplate<Node> * T2 = new TreeTemplate<Node>();
    T2->setRootNode(parcoursArriereC1(v1->getSon(1),v2->getSon(0),Adj_classe,ArbresDAdjacences, v1dansA1));
    ArbresDAdjacences->push_back(T2);
  }
  // (5)
  else if(C==recupCoutC1(v1->getSon(0),v2->getSon(0))+recupCoutC0(v1->getSon(1),v2->getSon(1))+
	  recupCoutC0(v1->getSon(0),v2->getSon(1))+recupCoutC0(v1->getSon(1),v2->getSon(0))+Break) {
    n->addSon(0,parcoursArriereC1(v1->getSon(0),v2->getSon(0),Adj_classe,ArbresDAdjacences, v1dansA1));
    //Cassure
    n->addSon(1,parcoursArriereCassure(v1->getSon(1),v2->getSon(1), v1dansA1));
    parcoursArriereC0(v1->getSon(0),v2->getSon(1),Adj_classe,ArbresDAdjacences, v1dansA1);
    parcoursArriereC0(v1->getSon(1),v2->getSon(0),Adj_classe,ArbresDAdjacences, v1dansA1);
  }
  // (6)
  else if(C==recupCoutC1(v1->getSon(0),v2->getSon(0))+recupCoutC0(v1->getSon(1),v2->getSon(1))+
	  recupCoutC0(v1->getSon(0),v2->getSon(1))+recupCoutC1(v1->getSon(1),v2->getSon(0))+Break+Crea) {
    n->addSon(0,parcoursArriereC1(v1->getSon(0),v2->getSon(0),Adj_classe,ArbresDAdjacences, v1dansA1));
    //Cassure
    n->addSon(1,parcoursArriereCassure(v1->getSon(1),v2->getSon(1), v1dansA1));
    parcoursArriereC0(v1->getSon(0),v2->getSon(1),Adj_classe,ArbresDAdjacences, v1dansA1);
    //Cr�ation
    nb_Crea++;
    TreeTemplate<Node> * T = new TreeTemplate<Node>();
    T->setRootNode(parcoursArriereC1(v1->getSon(1),v2->getSon(0),Adj_classe,ArbresDAdjacences, v1dansA1));
    ArbresDAdjacences->push_back(T);
  }
  // (7)
  else if(C==recupCoutC1(v1->getSon(0),v2->getSon(0))+recupCoutC0(v1->getSon(1),v2->getSon(1))+
	  recupCoutC1(v1->getSon(0),v2->getSon(1))+recupCoutC0(v1->getSon(1),v2->getSon(0))+Break+Crea) {
    n->addSon(0,parcoursArriereC1(v1->getSon(0),v2->getSon(0),Adj_classe,ArbresDAdjacences, v1dansA1));
    //Cassure
    n->addSon(1,parcoursArriereCassure(v1->getSon(1),v2->getSon(1), v1dansA1));
    //Cr�ation
    nb_Crea++;
    TreeTemplate<Node> * T = new TreeTemplate<Node>();
    T->setRootNode(parcoursArriereC1(v1->getSon(0),v2->getSon(1),Adj_classe,ArbresDAdjacences, v1dansA1));
    ArbresDAdjacences->push_back(T);
    parcoursArriereC0(v1->getSon(1),v2->getSon(0),Adj_classe,ArbresDAdjacences, v1dansA1);
  }
  // (8) 
  else if(C==recupCoutC0(v1->getSon(0),v2->getSon(0))+recupCoutC1(v1->getSon(1),v2->getSon(1))+
	  recupCoutC0(v1->getSon(0),v2->getSon(1))+recupCoutC0(v1->getSon(1),v2->getSon(0))+Break) {
    //Cassure	 
    n->addSon(0,parcoursArriereCassure(v1->getSon(0),v2->getSon(0), v1dansA1));
    n->addSon(1,parcoursArriereC1(v1->getSon(1),v2->getSon(1),Adj_classe,ArbresDAdjacences, v1dansA1));
    parcoursArriereC0(v1->getSon(0),v2->getSon(1),Adj_classe,ArbresDAdjacences, v1dansA1);
    parcoursArriereC0(v1->getSon(1),v2->getSon(0),Adj_classe,ArbresDAdjacences, v1dansA1);
  }
  // (9)   
  else if(C==recupCoutC0(v1->getSon(0),v2->getSon(0))+recupCoutC1(v1->getSon(1),v2->getSon(1))+
	  recupCoutC0(v1->getSon(0),v2->getSon(1))+recupCoutC1(v1->getSon(1),v2->getSon(0))+Break+Crea) {
    //Cassure	 
    n->addSon(0,parcoursArriereCassure(v1->getSon(0),v2->getSon(0), v1dansA1));
    n->addSon(1,parcoursArriereC1(v1->getSon(1),v2->getSon(1),Adj_classe,ArbresDAdjacences, v1dansA1));
    parcoursArriereC0(v1->getSon(0),v2->getSon(1),Adj_classe,ArbresDAdjacences, v1dansA1);
    //Cr�ation
    nb_Crea++;
    TreeTemplate<Node> * T = new TreeTemplate<Node>();
    T->setRootNode(parcoursArriereC1(v1->getSon(1),v2->getSon(0),Adj_classe,ArbresDAdjacences, v1dansA1));
    ArbresDAdjacences->push_back(T);
  }
  // (10)
  else if(C==recupCoutC0(v1->getSon(0),v2->getSon(0))+recupCoutC1(v1->getSon(1),v2->getSon(1))+
	  recupCoutC1(v1->getSon(0),v2->getSon(1))+recupCoutC0(v1->getSon(1),v2->getSon(0))+Break+Crea) {
    //Cassure	 
    n->addSon(0,parcoursArriereCassure(v1->getSon(0),v2->getSon(0), v1dansA1));
    n->addSon(1,parcoursArriereC1(v1->getSon(1),v2->getSon(1),Adj_classe,ArbresDAdjacences, v1dansA1));
    //Cr�ation
    nb_Crea++;
    TreeTemplate<Node> * T = new TreeTemplate<Node>();
    T->setRootNode(parcoursArriereC1(v1->getSon(0),v2->getSon(1),Adj_classe,ArbresDAdjacences, v1dansA1));
    ArbresDAdjacences->push_back(T);
    parcoursArriereC0(v1->getSon(1),v2->getSon(0),Adj_classe,ArbresDAdjacences, v1dansA1);
  }
  // (11)
  else if(C==recupCoutC0(v1->getSon(0),v2->getSon(0))+recupCoutC0(v1->getSon(1),v2->getSon(1))+
	  recupCoutC1(v1->getSon(0),v2->getSon(1))+recupCoutC1(v1->getSon(1),v2->getSon(0))) {
    parcoursArriereC0(v1->getSon(0),v2->getSon(0),Adj_classe,ArbresDAdjacences, v1dansA1);
    parcoursArriereC0(v1->getSon(1),v2->getSon(1),Adj_classe,ArbresDAdjacences, v1dansA1);
    n->addSon(0,parcoursArriereC1(v1->getSon(0),v2->getSon(1),Adj_classe,ArbresDAdjacences, v1dansA1));
    n->addSon(1,parcoursArriereC1(v1->getSon(1),v2->getSon(0),Adj_classe,ArbresDAdjacences, v1dansA1));
  }
  // (12)
  else if(C==recupCoutC0(v1->getSon(0),v2->getSon(0))+recupCoutC1(v1->getSon(1),v2->getSon(1))+
	  recupCoutC1(v1->getSon(0),v2->getSon(1))+recupCoutC1(v1->getSon(1),v2->getSon(0))+Crea) {
    parcoursArriereC0(v1->getSon(0),v2->getSon(0),Adj_classe,ArbresDAdjacences, v1dansA1);
    //Cr�ation
    nb_Crea++;
    TreeTemplate<Node> * T = new TreeTemplate<Node>();
    T->setRootNode(parcoursArriereC1(v1->getSon(1),v2->getSon(1),Adj_classe,ArbresDAdjacences, v1dansA1));
    ArbresDAdjacences->push_back(T);
    n->addSon(0,parcoursArriereC1(v1->getSon(0),v2->getSon(1),Adj_classe,ArbresDAdjacences, v1dansA1));
    n->addSon(1,parcoursArriereC1(v1->getSon(1),v2->getSon(0),Adj_classe,ArbresDAdjacences, v1dansA1));
  }
  // (13)
  else if(C==recupCoutC1(v1->getSon(0),v2->getSon(0))+recupCoutC0(v1->getSon(1),v2->getSon(1))+
	  recupCoutC1(v1->getSon(0),v2->getSon(1))+recupCoutC1(v1->getSon(1),v2->getSon(0))+Crea) {
    //Cr�ation
    nb_Crea++;
    TreeTemplate<Node> * T = new TreeTemplate<Node>();
    T->setRootNode(parcoursArriereC1(v1->getSon(0),v2->getSon(0),Adj_classe,ArbresDAdjacences, v1dansA1));
    ArbresDAdjacences->push_back(T);
    parcoursArriereC0(v1->getSon(1),v2->getSon(1),Adj_classe,ArbresDAdjacences, v1dansA1);
    n->addSon(0,parcoursArriereC1(v1->getSon(0),v2->getSon(1),Adj_classe,ArbresDAdjacences, v1dansA1));
    n->addSon(1,parcoursArriereC1(v1->getSon(1),v2->getSon(0),Adj_classe,ArbresDAdjacences, v1dansA1));
  }
  // (14)
  else if(C==recupCoutC0(v1->getSon(0),v2->getSon(0))+recupCoutC0(v1->getSon(1),v2->getSon(1))+
	  recupCoutC1(v1->getSon(0),v2->getSon(1))+recupCoutC0(v1->getSon(1),v2->getSon(0))+Break) {
    parcoursArriereC0(v1->getSon(0),v2->getSon(0),Adj_classe,ArbresDAdjacences, v1dansA1);
    parcoursArriereC0(v1->getSon(1),v2->getSon(1),Adj_classe,ArbresDAdjacences, v1dansA1);
    n->addSon(0,parcoursArriereC1(v1->getSon(0),v2->getSon(1),Adj_classe,ArbresDAdjacences, v1dansA1));
    n->addSon(1,parcoursArriereCassure(v1->getSon(1),v2->getSon(0), v1dansA1));
  }
  // (15)
  else if(C==recupCoutC0(v1->getSon(0),v2->getSon(0))+recupCoutC0(v1->getSon(1),v2->getSon(1))+
	  recupCoutC0(v1->getSon(0),v2->getSon(1))+recupCoutC1(v1->getSon(1),v2->getSon(0))+Break) {
    parcoursArriereC0(v1->getSon(0),v2->getSon(0),Adj_classe,ArbresDAdjacences, v1dansA1);
    parcoursArriereC0(v1->getSon(1),v2->getSon(1),Adj_classe,ArbresDAdjacences, v1dansA1);
    n->addSon(0,parcoursArriereCassure(v1->getSon(0),v2->getSon(1), v1dansA1));
    n->addSon(1,parcoursArriereC1(v1->getSon(1),v2->getSon(0),Adj_classe,ArbresDAdjacences, v1dansA1));
  }
  // (16) ajout�
  else if(C==recupCoutC0(v1->getSon(0),v2->getSon(0))+recupCoutC0(v1->getSon(1),v2->getSon(1))+
	  recupCoutC0(v1->getSon(0),v2->getSon(1))+recupCoutC0(v1->getSon(1),v2->getSon(0))+2*Break) {
    cout<<"\t\t\tJe suis dans parcoursArriereD12, je passe dans le cas C0 + C0 + C0 + C0 + 2*Break"<<endl;
    parcoursArriereC0(v1->getSon(0),v2->getSon(0),Adj_classe,ArbresDAdjacences, v1dansA1);
    parcoursArriereC0(v1->getSon(1),v2->getSon(1),Adj_classe,ArbresDAdjacences, v1dansA1);
    n->addSon(0,parcoursArriereCassure(v1->getSon(0),v2->getSon(1), v1dansA1));
    n->addSon(1,parcoursArriereCassure(v1->getSon(1),v2->getSon(0), v1dansA1));
  }

  return n;
}

// 5
Node * parcoursArriereC1GDupWithGDup(Node * v1, Node * v2, vector<adjacence> * Adj_classe,vector<Tree *> * ArbresDAdjacences, bool v1dansA1) {

  if(affich_ParcoursArriere) cout <<"Je suis dans parcoursArriereC1GDupWithGDup"<<",id v1="<<v1->getId()<<",id v2="<<v2->getId()<<endl;

  Node * n = parcoursArriereD1(v1, v2, Adj_classe, ArbresDAdjacences, v1dansA1);

  if(n->getNumberOfSons() == 0)
    n = parcoursArriereD2(v1, v2, Adj_classe, ArbresDAdjacences, v1dansA1);

  if(n->getNumberOfSons() > 0) { // one of the above 8 gdup events has happened
    nb_GDup++;
    setNodeProps(n,dupl,v1,v2,v1dansA1);
  }
  else { // try for an adup event
    n = parcoursArriereD12(v1, v2, Adj_classe, ArbresDAdjacences, v1dansA1);

    if(n->getNumberOfSons() == 0) cout << "PB dans parcoursArriereC1GDupWithGDup : aucun des 24 couts reconnus" << endl;

    nb_ADup++;
    setNodeProps(n,Adupl,v1,v2,v1dansA1);
  }

  n->setBranchProperty("D", BppString("T"));

  return n;
}

// 6
Node * parcoursArriereC1SpecWithSpec(Node * v1, Node * v2, vector<adjacence> * Adj_classe,vector<Tree *> * ArbresDAdjacences, bool v1dansA1) {

  if(affich_ParcoursArriere) cout <<"Je suis dans parcoursArriereC1SpecWithSpec"<<",id v1="<<v1->getId()<<",id v2="<<v2->getId()<<endl;

  Node * fgV1=v1->getSon(0);
  Node * fdV1=v1->getSon(1);
  Node * fgV2; Node * fdV2;

  if (Esp(fgV1)==Esp(v2->getSon(0))) {
    fgV2=v2->getSon(0);
    fdV2=v2->getSon(1);
  }
  else {
    fgV2=v2->getSon(1);
    fdV2=v2->getSon(0);
  }

  Node * n = new Node();

  float C = recupCoutC1(v1,v2);
  if(affich_ParcoursArriere) {
    cout << "C1(x,y) = " << C << endl << endl;
    cout << "C1(xa,ya) + C1(xb,yb) = " << recupCoutC1(fgV1,fgV2) << " + " << recupCoutC1(fdV1,fdV2) << endl;
    cout << "C1(xa,ya) + C0(xb,yb) + Br = " << recupCoutC1(fgV1,fgV2) << " + " << recupCoutC0(fdV1,fdV2) << " + " << Break << endl;
    cout << "C0(xa,ya) + C1(xb,yb) + Br = " << recupCoutC0(fgV1,fgV2) << " + " << recupCoutC1(fdV1,fdV2) << " + " << Break << endl;
    cout << "C0(xa,ya) + C0(xb,yb) + 2*Br = " << recupCoutC0(fgV1,fgV2) << " + " << recupCoutC0(fdV1,fdV2) << " + " << 2*Break << endl;
  }

  if(C==recupCoutC1(fgV1,fgV2) + recupCoutC1(fdV1,fdV2)) {
    n->addSon(0,parcoursArriereC1(fgV1,fgV2,Adj_classe,ArbresDAdjacences, v1dansA1));
    n->addSon(1,parcoursArriereC1(fdV1,fdV2,Adj_classe,ArbresDAdjacences, v1dansA1));
  }
  else if(C==recupCoutC1(fgV1,fgV2) + recupCoutC0(fdV1,fdV2) + Break) {
    n->addSon(0,parcoursArriereC1(fgV1,fgV2,Adj_classe,ArbresDAdjacences, v1dansA1));
    n->addSon(1,parcoursArriereCassure(fdV1,fdV2, v1dansA1));
    parcoursArriereC0(fdV1,fdV2,Adj_classe,ArbresDAdjacences, v1dansA1);
  }
  else if(C==recupCoutC0(fgV1,fgV2) + recupCoutC1(fdV1,fdV2) + Break) {
    n->addSon(0,parcoursArriereC1(fdV1,fdV2,Adj_classe,ArbresDAdjacences, v1dansA1));
    n->addSon(1,parcoursArriereCassure(fgV1,fgV2, v1dansA1));
    parcoursArriereC0(fgV1,fgV2,Adj_classe,ArbresDAdjacences, v1dansA1);
  }
  else if(C==recupCoutC0(fgV1,fgV2) + recupCoutC0(fdV1,fdV2) + 2*Break) {
    n->addSon(0,parcoursArriereCassure(fgV1,fgV2, v1dansA1));
    n->addSon(1,parcoursArriereCassure(fdV1,fdV2, v1dansA1));
    parcoursArriereC0(fgV1,fgV2,Adj_classe,ArbresDAdjacences, v1dansA1);
    parcoursArriereC0(fdV1,fdV2,Adj_classe,ArbresDAdjacences, v1dansA1);
    cout<<"\t\t\tJe suis dans parcoursArriereC1SpecWithSpec, je passe dans le cas C0 + C0 + 2*Break"<<endl;
  }
  else 
    cout<<"PB dans parcoursArriereC1SpecWithSpec : aucun des 4 co�ts reconnus"<<endl;

  setNodeProps(n,spe,v1,v2,v1dansA1);
  n->setBranchProperty("D", BppString("F"));

  return n;
}

// 7
Node * parcoursArriereC1BClassWithTClass(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1) {
  
  if(affich_ParcoursArriere) cout <<"Je suis dans parcoursArriereC1BClassWithTClass"<<",id v1="<<v1->getId()<<",id v2="<<v2->getId()<<endl;  

  Node * n = new Node();
  n->addSon(0,parcoursArriereC1(v1,v2->getSon(0),Adj_classe,ArbresDAdjacences,v1dansA1));
  setNodeProps(n,null,v1,v2,v1dansA1); // the setting to 'null' may change

  return n;
}

// 8 
Node * parcoursArriereC1TOutWithBClassGDup(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1) {
  
  if(affich_ParcoursArriere) cout <<"Je suis dans parcoursArriereC1TOutWithBClassGDupRec"<<",id v1="<<v1->getId()<<",id v2="<<v2->getId()<<endl;  

  Node * n = new Node();
  setNodeProps(n,Aper,v1,v2,v1dansA1); // the setting to 'Aper' may change

  return n;
}

// 9
Node * parcoursArriereC1NullWithNull(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1) {

  if(affich_ParcoursArriere) cout <<"Je suis dans parcoursArriereC1NullWithNull"<<",id v1="<<v1->getId()<<",id v2="<<v2->getId()<<endl;  

  Node * n = new Node();
  n->addSon(0,parcoursArriereC1(v1->getSon(0),v2->getSon(0),Adj_classe,ArbresDAdjacences,v1dansA1));
  setNodeProps(n,null,v1,v2,v1dansA1);

  return n;
}

// 10
Node * parcoursArriereC1RecWithRecTB(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1) {

  if(affich_ParcoursArriere) cout <<"Je suis dans parcoursArriereC1RecWithRecTB"<<",id v1="<<v1->getId()<<",id v2="<<v2->getId()<<endl;

  Node * n = new Node();

  float C = recupCoutC1(v1,v2);
  if(Esp(v1) == Esp(v2)) {
    if(C==recupCoutC1(v1->getSon(0),v2->getSon(0))) {
      n->addSon(0,parcoursArriereC1(v1->getSon(0),v2->getSon(0),Adj_classe,ArbresDAdjacences,v1dansA1));
      setNodeProps(n,rc,v1,v2,v1dansA1); // the setting to 'rc' may change
      addCoTrans(v1,v2,v1dansA1); // update co-transfer info
    }
    else // C = CO(ca(v1),ca(v2))
      setNodeProps(n,Aper,v1,v2,v1dansA1); // the setting to 'Aper' may change
  }
  else // Esp(v1) != Esp(v2)
    setNodeProps(n,Aper,v1,v2,v1dansA1); // the setting to 'Aper' may change

  return n;
}

// 11
Node * parcoursArriereC1RecWithTrans(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1) {

  if(affich_ParcoursArriere) cout <<"Je suis dans parcoursArriereC1RecWithTrans"<<",id v1="<<v1->getId()<<",id v2="<<v2->getId()<<endl;  

  Node * n = new Node();
  n->addSon(0,parcoursArriereC1(v1->getSon(0),v2->getSon(0),Adj_classe,ArbresDAdjacences,v1dansA1));
  setNodeProps(n,null,v1,v2,v1dansA1); // the setting to 'null' may change

  return n;
}

// 12
Node * parcoursArriereC1TransWithTransTB(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1) {

  if(affich_ParcoursArriere) cout <<"Je suis dans parcoursArriereC1TransWithTransTB"<<",id v1="<<v1->getId()<<",id v2="<<v2->getId()<<endl;  

  //Modification by W

  //first son stays in the species tree, second son goes outside the species tree
  Node * fgV1=v1->getSon(0);
  Node * fdV1=v1->getSon(1);
  Node * fgV2=v2->getSon(0);
  Node * fdV2=v2->getSon(1);



  Node * n = new Node();


  float C = recupCoutC1(v1,v2);
  if(affich_ParcoursArriere) {
    cout << "C1(x,y) = " << C << endl << endl;
    cout << "C1(xa,ya) + C1(xb,yb) = " << recupCoutC1(fgV1,fgV2) << " + " << recupCoutC1(fdV1,fdV2) << endl;
    cout << "C0(xa,ya) + C1(xb,yb) + Br = " << recupCoutC0(fgV1,fgV2) << " + " << recupCoutC1(fdV1,fdV2) << " + " << Break << endl;
  }

  if(C==recupCoutC1(fgV1,fgV2) + recupCoutC1(fdV1,fdV2)) {
    n->addSon(0,parcoursArriereC1(fgV1,fgV2,Adj_classe,ArbresDAdjacences, v1dansA1));
    n->addSon(1,parcoursArriereC1(fdV1,fdV2,Adj_classe,ArbresDAdjacences, v1dansA1));
  }
  else if(C==recupCoutC0(fgV1,fgV2) + recupCoutC1(fdV1,fdV2) + Break) {//the son that stayed in the species tree undergoe a break
    n->addSon(0,parcoursArriereC1(fdV1,fdV2,Adj_classe,ArbresDAdjacences, v1dansA1));
    n->addSon(1,parcoursArriereCassure(fgV1,fgV2, v1dansA1));
    parcoursArriereC0(fgV1,fgV2,Adj_classe,ArbresDAdjacences, v1dansA1);
  }
  else 
    cout<<"PB dans parcoursArriereC1TransWithTransTB : aucun des 2 co�ts reconnus"<<endl;


  setNodeProps(n,trn,v1,v2,v1dansA1);

  //end modification by W

  return n;
}

// 13
Node * parcoursArriereC1TBWithTB(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1) {

  if(affich_ParcoursArriere) cout <<"Je suis dans parcoursArriereC1TBWithTB"<<",id v1="<<v1->getId()<<",id v2="<<v2->getId()<<endl;  

  Node * n = new Node();

  float C = recupCoutC1(v1,v2);
  if(Esp(v1) == Esp(v2)) {
    if(C==recupCoutC1(v1->getSon(0),v2->getSon(0))+recupCoutC1(v1->getSon(1),v2->getSon(1))) {
      n->addSon(0,parcoursArriereC1(v1->getSon(0),v2->getSon(0),Adj_classe,ArbresDAdjacences,v1dansA1));
      n->addSon(1,parcoursArriereC1(v1->getSon(1),v2->getSon(1),Adj_classe,ArbresDAdjacences,v1dansA1));
      setNodeProps(n,tb,v1,v2,v1dansA1); // the setting to 'tb' may change
      addCoTrans(v1,v2,v1dansA1); // update co-transfer info
    }
    else if(C==recupCoutC0(v1->getSon(0),v2->getSon(0))+recupCoutC1(v1->getSon(1),v2->getSon(1))) {
      Node * nc = new Node(); // add a null first child
      setNodeProps(nc,Aper,v1->getSon(0),v2->getSon(0),v1dansA1);
      n->addSon(0,nc);
      n->addSon(1,parcoursArriereC1(v1->getSon(1),v2->getSon(1),Adj_classe,ArbresDAdjacences,v1dansA1));
      setNodeProps(n,tb,v1,v2,v1dansA1); // the setting to 'tb' may change
    }
    else // C != one of these above
      setNodeProps(n,Aper,v1,v2,v1dansA1); // the setting to 'Aper' may change
  }
  else // Esp(v1) != Esp(v2)
    setNodeProps(n,Aper,v1,v2,v1dansA1); // the setting to 'Aper' may change

  return n;
}

// 14
Node * parcoursArriereC1TransWithTOut(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1) {

  if(affich_ParcoursArriere) cout <<"Je suis dans parcoursArriereC1TransWithTOut"<<",id v1="<<v1->getId()<<",id v2="<<v2->getId()<<endl;  

  Node * n = new Node();
  n->addSon(0,parcoursArriereC1(v1->getSon(1),v2->getSon(0),Adj_classe,ArbresDAdjacences,v1dansA1));
  setNodeProps(n,trn,v1,v2,v1dansA1); // the setting to 'trans' may change

  return n;
}

// 15
Node * parcoursArriereC1TOutWithRecTB(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1) {

  if(affich_ParcoursArriere) cout <<"Je suis dans parcoursArriereC1TOutWithRecTB"<<",id v1="<<v1->getId()<<",id v2="<<v2->getId()<<endl;

  Node * n = new Node();
  n->addSon(0,parcoursArriereC1(v1->getSon(0),v2,Adj_classe,ArbresDAdjacences,v1dansA1));
  setNodeProps(n,to,v1,v2,v1dansA1); // the setting to 'to' may change

  return n;
}

// O1
Node * parcoursArriereO1(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1) {

  Node * n = new Node();
  float C = recupCoutC1(v1,v2);

  if(C==recupCoutC0(v1->getSon(0),v2)+recupCoutC0(v1->getSon(1),v2))
    setNodeProps(n,Aper,v1,v2,v1dansA1); /* we don't pursue this pair further, rather, we treat it like an ALos
					    (but calling it 'Aper' may change) */
  else if(C==recupCoutC1(v1->getSon(0),v2)+recupCoutC0(v1->getSon(1),v2))
    n->addSon(0,parcoursArriereC1(v1->getSon(0),v2,Adj_classe,ArbresDAdjacences,v1dansA1));
  else if(C==recupCoutC0(v1->getSon(0),v2)+recupCoutC1(v1->getSon(1),v2))
    n->addSon(0,parcoursArriereC1(v1->getSon(1),v2,Adj_classe,ArbresDAdjacences,v1dansA1));
  else if(C==recupCoutC1(v1->getSon(0),v2)+recupCoutC1(v1->getSon(1),v2)+Crea) {
    n->addSon(0,parcoursArriereC1(v1->getSon(0),v2,Adj_classe,ArbresDAdjacences,v1dansA1));
    nb_Crea++;
    TreeTemplate<Node> * T = new TreeTemplate<Node>();
    T->setRootNode(parcoursArriereC1(v1->getSon(1),v2,Adj_classe,ArbresDAdjacences,v1dansA1));
    ArbresDAdjacences->push_back(T);
  }

  return n;
}

// 16
Node * parcoursArriereC1OutWithRecTB(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1) {

  if(affich_ParcoursArriere) cout <<"Je suis dans parcoursArriereC1OutWithRecTB"<<",id v1="<<v1->getId()<<",id v2="<<v2->getId()<<endl;

  Node * n = parcoursArriereO1(v1, v2, Adj_classe, ArbresDAdjacences, v1dansA1);

  if(n->getNumberOfSons() == 0 && n->hasBranchProperty(typ) == false) cout << "PB dans parcoursArriereC1OutWithRecTB : aucun des 4 couts reconnus" << endl;

  if(n->hasBranchProperty(typ) == false) setNodeProps(n,out,v1,v2,v1dansA1); // the setting to 'out' may change

  return n;
}

// 17
Node * parcoursArriereC1TOutWithTOut(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1) {

  if(affich_ParcoursArriere) cout <<"Je suis dans parcoursArriereC1TOutWithTOut"<<",id v1="<<v1->getId()<<",id v2="<<v2->getId()<<endl;

  Node * n = new Node();
  n->addSon(0,parcoursArriereC1(v1->getSon(0),v2->getSon(0),Adj_classe,ArbresDAdjacences,v1dansA1));
  setNodeProps(n,to,v1,v2,v1dansA1);

  return n;
}

// O2
Node * parcoursArriereO2(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1) {

  Node * n = new Node();
  float C = recupCoutC1(v1,v2);

  if(C==recupCoutC0(v1,v2->getSon(0))+recupCoutC0(v1,v2->getSon(1))+Break)
    setNodeProps(n,Aper,v1,v2,v1dansA1);
  if(C==recupCoutC1(v1,v2->getSon(0))+recupCoutC0(v1,v2->getSon(1)))
    n->addSon(0,parcoursArriereC1(v1,v2->getSon(0),Adj_classe,ArbresDAdjacences,v1dansA1));
  else if(C==recupCoutC0(v1,v2->getSon(0))+recupCoutC1(v1,v2->getSon(1)))
    n->addSon(0,parcoursArriereC1(v1,v2->getSon(1),Adj_classe,ArbresDAdjacences,v1dansA1));
  else if(C==recupCoutC1(v1,v2->getSon(0))+recupCoutC1(v1,v2->getSon(1))+Crea) {
    n->addSon(0,parcoursArriereC1(v1,v2->getSon(0),Adj_classe,ArbresDAdjacences,v1dansA1));
    nb_Crea++;
    TreeTemplate<Node> * T = new TreeTemplate<Node>();
    T->setRootNode(parcoursArriereC1(v1,v2->getSon(1),Adj_classe,ArbresDAdjacences,v1dansA1));
    ArbresDAdjacences->push_back(T);
  }

  return n;
}

// O12
Node * parcoursArriereO12(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1) {

  Node * n = new Node();
  float C = recupCoutC1(v1,v2);

  // now, the 16 cases ...

  // (1)
  if(C==recupCoutC1(v1->getSon(0),v2->getSon(0))+recupCoutC1(v1->getSon(1),v2->getSon(1))+
     recupCoutC0(v1->getSon(0),v2->getSon(1))+recupCoutC0(v1->getSon(1),v2->getSon(0))) {
    n->addSon(0,parcoursArriereC1(v1->getSon(0),v2->getSon(0),Adj_classe,ArbresDAdjacences, v1dansA1));
    n->addSon(1,parcoursArriereC1(v1->getSon(1),v2->getSon(1),Adj_classe,ArbresDAdjacences, v1dansA1));
  }
  // (2)
  else if(C==recupCoutC1(v1->getSon(0),v2->getSon(0))+recupCoutC1(v1->getSon(1),v2->getSon(1))+
	  recupCoutC0(v1->getSon(0),v2->getSon(1))+recupCoutC1(v1->getSon(1),v2->getSon(0))+Crea) {
    n->addSon(0,parcoursArriereC1(v1->getSon(0),v2->getSon(0),Adj_classe,ArbresDAdjacences, v1dansA1));
    n->addSon(1,parcoursArriereC1(v1->getSon(1),v2->getSon(1),Adj_classe,ArbresDAdjacences, v1dansA1));
    //Cr�ation
    nb_Crea++;
    TreeTemplate<Node> * T = new TreeTemplate<Node>();
    T->setRootNode(parcoursArriereC1(v1->getSon(1),v2->getSon(0),Adj_classe,ArbresDAdjacences, v1dansA1));
    ArbresDAdjacences->push_back(T);
  }
  // (3)
  else if(C==recupCoutC1(v1->getSon(0),v2->getSon(0))+recupCoutC1(v1->getSon(1),v2->getSon(1))+
	  recupCoutC1(v1->getSon(0),v2->getSon(1))+recupCoutC0(v1->getSon(1),v2->getSon(0))+Crea) {
    n->addSon(0,parcoursArriereC1(v1->getSon(0),v2->getSon(0),Adj_classe,ArbresDAdjacences, v1dansA1));
    n->addSon(1,parcoursArriereC1(v1->getSon(1),v2->getSon(1),Adj_classe,ArbresDAdjacences, v1dansA1));
    //Cr�ation
    nb_Crea++;
    TreeTemplate<Node> * T = new TreeTemplate<Node>();
    T->setRootNode(parcoursArriereC1(v1->getSon(0),v2->getSon(1),Adj_classe,ArbresDAdjacences, v1dansA1));
    ArbresDAdjacences->push_back(T);
  }
  // (4)
  else if(C==recupCoutC1(v1->getSon(0),v2->getSon(0))+recupCoutC1(v1->getSon(1),v2->getSon(1))+
	  recupCoutC1(v1->getSon(0),v2->getSon(1))+recupCoutC1(v1->getSon(1),v2->getSon(0))+2*Crea) {
    n->addSon(0,parcoursArriereC1(v1->getSon(0),v2->getSon(0),Adj_classe,ArbresDAdjacences, v1dansA1));
    n->addSon(1,parcoursArriereC1(v1->getSon(1),v2->getSon(1),Adj_classe,ArbresDAdjacences, v1dansA1));
    //Cr�ation
    nb_Crea+=2;
    TreeTemplate<Node> * T1 = new TreeTemplate<Node>();
    T1->setRootNode(parcoursArriereC1(v1->getSon(0),v2->getSon(1),Adj_classe,ArbresDAdjacences, v1dansA1));
    ArbresDAdjacences->push_back(T1);
    //Cr�ation
    TreeTemplate<Node> * T2 = new TreeTemplate<Node>();
    T2->setRootNode(parcoursArriereC1(v1->getSon(1),v2->getSon(0),Adj_classe,ArbresDAdjacences, v1dansA1));
    ArbresDAdjacences->push_back(T2);
  }
  // (5)
  else if(C==recupCoutC1(v1->getSon(0),v2->getSon(0))+recupCoutC0(v1->getSon(1),v2->getSon(1))+
	  recupCoutC0(v1->getSon(0),v2->getSon(1))+recupCoutC0(v1->getSon(1),v2->getSon(0))) {
    n->addSon(0,parcoursArriereC1(v1->getSon(0),v2->getSon(0),Adj_classe,ArbresDAdjacences, v1dansA1));
    // Cassure (do nothing) :
    /* even though an adjacency was "lost" here, we do nothing
       ... perhaps later we will add a special ALos node for events
       happening outside the tree */
  }
  // (6)
  else if(C==recupCoutC1(v1->getSon(0),v2->getSon(0))+recupCoutC0(v1->getSon(1),v2->getSon(1))+
	  recupCoutC0(v1->getSon(0),v2->getSon(1))+recupCoutC1(v1->getSon(1),v2->getSon(0))+Crea) {
    n->addSon(0,parcoursArriereC1(v1->getSon(0),v2->getSon(0),Adj_classe,ArbresDAdjacences, v1dansA1));
    //Cassure (do nothing)
    //Cr�ation
    nb_Crea++;
    TreeTemplate<Node> * T = new TreeTemplate<Node>();
    T->setRootNode(parcoursArriereC1(v1->getSon(1),v2->getSon(0),Adj_classe,ArbresDAdjacences, v1dansA1));
    ArbresDAdjacences->push_back(T);
  }
  // (7)
  else if(C==recupCoutC1(v1->getSon(0),v2->getSon(0))+recupCoutC0(v1->getSon(1),v2->getSon(1))+
	  recupCoutC1(v1->getSon(0),v2->getSon(1))+recupCoutC0(v1->getSon(1),v2->getSon(0))+Crea) {
    n->addSon(0,parcoursArriereC1(v1->getSon(0),v2->getSon(0),Adj_classe,ArbresDAdjacences, v1dansA1));
    //Cassure (do nothing)
    //Cr�ation
    nb_Crea++;
    TreeTemplate<Node> * T = new TreeTemplate<Node>();
    T->setRootNode(parcoursArriereC1(v1->getSon(0),v2->getSon(1),Adj_classe,ArbresDAdjacences, v1dansA1));
    ArbresDAdjacences->push_back(T);
  }
  // (8) 
  else if(C==recupCoutC0(v1->getSon(0),v2->getSon(0))+recupCoutC1(v1->getSon(1),v2->getSon(1))+
	  recupCoutC0(v1->getSon(0),v2->getSon(1))+recupCoutC0(v1->getSon(1),v2->getSon(0))) {
    //Cassure (do nothing)
    n->addSon(1,parcoursArriereC1(v1->getSon(1),v2->getSon(1),Adj_classe,ArbresDAdjacences, v1dansA1));
  }
  // (9)   
  else if(C==recupCoutC0(v1->getSon(0),v2->getSon(0))+recupCoutC1(v1->getSon(1),v2->getSon(1))+
	  recupCoutC0(v1->getSon(0),v2->getSon(1))+recupCoutC1(v1->getSon(1),v2->getSon(0))+Crea) {
    //Cassure (do nothing)
    n->addSon(1,parcoursArriereC1(v1->getSon(1),v2->getSon(1),Adj_classe,ArbresDAdjacences, v1dansA1));
    //Cr�ation
    nb_Crea++;
    TreeTemplate<Node> * T = new TreeTemplate<Node>();
    T->setRootNode(parcoursArriereC1(v1->getSon(1),v2->getSon(0),Adj_classe,ArbresDAdjacences, v1dansA1));
    ArbresDAdjacences->push_back(T);
  }
  // (10)
  else if(C==recupCoutC0(v1->getSon(0),v2->getSon(0))+recupCoutC1(v1->getSon(1),v2->getSon(1))+
	  recupCoutC1(v1->getSon(0),v2->getSon(1))+recupCoutC0(v1->getSon(1),v2->getSon(0))+Crea) {
    //Cassure (do nothing)
    n->addSon(1,parcoursArriereC1(v1->getSon(1),v2->getSon(1),Adj_classe,ArbresDAdjacences, v1dansA1));
    //Cr�ation
    nb_Crea++;
    TreeTemplate<Node> * T = new TreeTemplate<Node>();
    T->setRootNode(parcoursArriereC1(v1->getSon(0),v2->getSon(1),Adj_classe,ArbresDAdjacences, v1dansA1));
    ArbresDAdjacences->push_back(T);
  }
  // (11)
  else if(C==recupCoutC0(v1->getSon(0),v2->getSon(0))+recupCoutC0(v1->getSon(1),v2->getSon(1))+
	  recupCoutC1(v1->getSon(0),v2->getSon(1))+recupCoutC1(v1->getSon(1),v2->getSon(0))) {
    n->addSon(0,parcoursArriereC1(v1->getSon(0),v2->getSon(1),Adj_classe,ArbresDAdjacences, v1dansA1));
    n->addSon(1,parcoursArriereC1(v1->getSon(1),v2->getSon(0),Adj_classe,ArbresDAdjacences, v1dansA1));
  }
  // (12)
  else if(C==recupCoutC0(v1->getSon(0),v2->getSon(0))+recupCoutC1(v1->getSon(1),v2->getSon(1))+
	  recupCoutC1(v1->getSon(0),v2->getSon(1))+recupCoutC1(v1->getSon(1),v2->getSon(0))+Crea) {
    //Cr�ation
    nb_Crea++;
    TreeTemplate<Node> * T = new TreeTemplate<Node>();
    T->setRootNode(parcoursArriereC1(v1->getSon(1),v2->getSon(1),Adj_classe,ArbresDAdjacences, v1dansA1));
    ArbresDAdjacences->push_back(T);
    n->addSon(0,parcoursArriereC1(v1->getSon(0),v2->getSon(1),Adj_classe,ArbresDAdjacences, v1dansA1));
    n->addSon(1,parcoursArriereC1(v1->getSon(1),v2->getSon(0),Adj_classe,ArbresDAdjacences, v1dansA1));
  }
  // (13)
  else if(C==recupCoutC1(v1->getSon(0),v2->getSon(0))+recupCoutC0(v1->getSon(1),v2->getSon(1))+
	  recupCoutC1(v1->getSon(0),v2->getSon(1))+recupCoutC1(v1->getSon(1),v2->getSon(0))+Crea) {
    //Cr�ation
    nb_Crea++;
    TreeTemplate<Node> * T = new TreeTemplate<Node>();
    T->setRootNode(parcoursArriereC1(v1->getSon(0),v2->getSon(0),Adj_classe,ArbresDAdjacences, v1dansA1));
    ArbresDAdjacences->push_back(T);
    n->addSon(0,parcoursArriereC1(v1->getSon(0),v2->getSon(1),Adj_classe,ArbresDAdjacences, v1dansA1));
    n->addSon(1,parcoursArriereC1(v1->getSon(1),v2->getSon(0),Adj_classe,ArbresDAdjacences, v1dansA1));
  }
  // (14)
  else if(C==recupCoutC0(v1->getSon(0),v2->getSon(0))+recupCoutC0(v1->getSon(1),v2->getSon(1))+
	  recupCoutC1(v1->getSon(0),v2->getSon(1))+recupCoutC0(v1->getSon(1),v2->getSon(0))) {
    n->addSon(0,parcoursArriereC1(v1->getSon(0),v2->getSon(1),Adj_classe,ArbresDAdjacences, v1dansA1));
  }
  // (15)
  else if(C==recupCoutC0(v1->getSon(0),v2->getSon(0))+recupCoutC0(v1->getSon(1),v2->getSon(1))+
	  recupCoutC0(v1->getSon(0),v2->getSon(1))+recupCoutC1(v1->getSon(1),v2->getSon(0))) {
    n->addSon(1,parcoursArriereC1(v1->getSon(1),v2->getSon(0),Adj_classe,ArbresDAdjacences, v1dansA1));
  }
  // (16) ajout�
  else if(C==recupCoutC0(v1->getSon(0),v2->getSon(0))+recupCoutC0(v1->getSon(1),v2->getSon(1))+
	  recupCoutC0(v1->getSon(0),v2->getSon(1))+recupCoutC0(v1->getSon(1),v2->getSon(0))) {
    cout<<"\t\t\tJe suis dans parcoursArriereO12, je passe dans le cas C0 + C0 + C0 + C0"<<endl;
    setNodeProps(n,Aper,v1,v2,v1dansA1);
  }

  return n;
}

// 18
Node * parcoursArriereC1OutWithOut(Node * v1, Node * v2, vector<adjacence> * Adj_classe,vector<Tree *> * ArbresDAdjacences, bool v1dansA1) {

  if(affich_ParcoursArriere) cout <<"Je suis dans parcoursArriereC1OutWithOut"<<",id v1="<<v1->getId()<<",id v2="<<v2->getId()<<endl;

  Node * n = parcoursArriereO1(v1, v2, Adj_classe, ArbresDAdjacences, v1dansA1);

  if(n->getNumberOfSons() == 0 && n->hasBranchProperty(typ) == false)
    n = parcoursArriereO2(v1, v2, Adj_classe, ArbresDAdjacences, v1dansA1);

  if(n->getNumberOfSons() > 0 || n->hasBranchProperty(typ) == true) { // one of the above 8 out events has happened
    if(n->hasBranchProperty(typ) == false) setNodeProps(n,out,v1,v2,v1dansA1); // the setting to 'out' may change
  }
  else { // try for a tandem out event
    n = parcoursArriereO12(v1, v2, Adj_classe, ArbresDAdjacences, v1dansA1);

    if(n->getNumberOfSons() == 0 && n->hasBranchProperty(typ) == false) cout << "PB dans parcoursArriereC1OutWithOut : aucun 24 couts reconnus" << endl;

    if(n->hasBranchProperty(typ) == false) setNodeProps(n,out,v1,v2,v1dansA1); // the setting to 'out' may change
  }

  return n;
}

//
// cases for C0
//

// 1
void parcoursArriereC0ExtantWithExtant(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1) {

  if(affich_ParcoursArriere) cout <<"Je suis dans parcoursArriereC0ExtantWithExtant"<<",id v1="<<v1->getId()<<",id v2="<<v2->getId()<<endl;
   
  string nomV1 = v1->getName();
  string nomV2 = v2->getName();
  string nom_gene1=nomV1.substr(0,nomV1.rfind(sep,nomV1.length()-1));
  string nom_gene2=nomV2.substr(0,nomV2.rfind(sep,nomV2.length()-1)); 
  string espece=nomV2.substr(nomV2.rfind(sep,nomV2.length())+1,nomV2.length());

  //On cherche d'adj dans les deux sens g1-g2 ou g2-g1
  adjacence a12,a21;
  a12.gene1=nom_gene1;
  a12.gene2=nom_gene2;
  a21.gene1=nom_gene2;
  a21.gene2=nom_gene1;
   
  if(appAdj(a12,Adj_classe) || appAdj(a21,Adj_classe)) {
    //IMPOSSIBLE AVEC CO�T INFINI SAUF SI ARBRE R�DUIT � UNE FEUILLE
    cout<<"parcoursArriereC0ExtantExtant : je suis dans le cas IMPOSSIBLE !!!"<<endl;
    // //On cr�e un arbre contenant juste une feuille
    // Node * n = new Node();
    // n->setId(id_arbres_adj);
    // id_arbres_adj++;
    // BppString * Espece = dynamic_cast<BppString*> (v1->getNodeProperty(esp));
    // n->setNodeProperty(esp,*Espece);
    // n->setBranchProperty(typ,BppString(ga));
    // n->setBranchProperty("D", BppString("?"));
    // if(v1dansA1)
    //    n->setName(nom_gene1+sepAdj+nom_gene2+sep+espece);
    // else
    //    n->setName(nom_gene2+sepAdj+nom_gene1+sep+espece);
    // //Cr�ation de l'arbre
    // nb_Crea++;
    // TreeTemplate<Node> * T = new TreeTemplate<Node>();
    // T->setRootNode(n);
    // ArbresDAdjacences->push_back(T);
  }
  else {} // On ne fait rien
}

// 2
void parcoursArriereC0GLosWithAny(Node * v1, Node * v2, vector<adjacence> * Adj_classe,vector<Tree *> * ArbresDAdjacences, bool v1dansA1) {

  //On ne fait rien
  if(affich_ParcoursArriere) cout <<"Je suis dans parcoursArriereC0GLosWithAny"<<",id v1="<<v1->getId()<<",id v2="<<v2->getId()<<endl;
}

void parcoursArriereC0GLosWithGLos(Node * v1, Node * v2, vector<adjacence> * Adj_classe,vector<Tree *> * ArbresDAdjacences, bool v1dansA1) {

  //On ne fait rien
  if(affich_ParcoursArriere) cout <<"Je suis dans parcoursArriereC0GLosWithGLos"<<",id v1="<<v1->getId()<<",id v2="<<v2->getId()<<endl;
}

// D0
void parcoursArriereD0(Node * v1, Node * v2, vector<adjacence> * Adj_classe,vector<Tree *> * ArbresDAdjacences, bool v1dansA1, bool & indicator) {

  indicator = false;
  float C = recupCoutC0(v1,v2);

  if(C==recupCoutC0(v1->getSon(0),v2)+recupCoutC0(v1->getSon(1),v2)) {
    indicator = true;
    parcoursArriereC0(v1->getSon(0), v2, Adj_classe, ArbresDAdjacences, v1dansA1);
    parcoursArriereC0(v1->getSon(1), v2, Adj_classe, ArbresDAdjacences, v1dansA1);
 }
  else if(C==recupCoutC1(v1->getSon(0),v2)+recupCoutC0(v1->getSon(1),v2)+Crea) {
    indicator = true;
    nb_Crea++;
    TreeTemplate<Node> * T = new TreeTemplate<Node>();
    T->setRootNode(parcoursArriereC1(v1->getSon(0),v2,Adj_classe,ArbresDAdjacences, v1dansA1));
    ArbresDAdjacences->push_back(T);
    parcoursArriereC0(v1->getSon(1), v2, Adj_classe, ArbresDAdjacences, v1dansA1);
  }
  else if(C==recupCoutC0(v1->getSon(0),v2)+recupCoutC1(v1->getSon(1),v2)+Crea) {
    indicator = true;
    parcoursArriereC0(v1->getSon(0),v2,Adj_classe,ArbresDAdjacences, v1dansA1);
    nb_Crea++;
    TreeTemplate<Node> * T = new TreeTemplate<Node>();
    T->setRootNode(parcoursArriereC1(v1->getSon(1),v2,Adj_classe,ArbresDAdjacences, v1dansA1));
    ArbresDAdjacences->push_back(T);
  }
  else if(recupCoutC1(v1->getSon(0),v2)+recupCoutC1(v1->getSon(1),v2)+2*Crea) {
    indicator = true;
    nb_Crea+=2;
    TreeTemplate<Node> * T1 = new TreeTemplate<Node>();
    T1->setRootNode(parcoursArriereC1(v1->getSon(0),v2,Adj_classe,ArbresDAdjacences, v1dansA1));
    ArbresDAdjacences->push_back(T1);
    TreeTemplate<Node> * T2 = new TreeTemplate<Node>();
    T2->setRootNode(parcoursArriereC1(v1->getSon(1),v2,Adj_classe,ArbresDAdjacences, v1dansA1));
    ArbresDAdjacences->push_back(T2);
  }
}

// 4
void parcoursArriereC0GDupWithBTClass(Node * v1, Node * v2, vector<adjacence> * Adj_classe,vector<Tree *> * ArbresDAdjacences, bool v1dansA1) {

  if(affich_ParcoursArriere) cout <<"Je suis dans parcoursArriereC0GDupWithBTClass"<<",id v1="<<v1->getId()<<",id v2="<<v2->getId()<<endl;

  bool indicator;
  parcoursArriereD0(v1, v2, Adj_classe, ArbresDAdjacences, v1dansA1, indicator);
  
  if(indicator == false) cout << "PB dans parcoursArriereC0GDupWithBTClass : aucun des 4 couts reconnus" << endl;
  
  nb_GDup++;
}

// D00
void parcoursArriereD00(Node * v1, Node * v2, vector<adjacence> * Adj_classe,vector<Tree *> * ArbresDAdjacences, bool v1dansA1, bool & indicator) {

  // try to go down v1 branch
  parcoursArriereD0(v1, v2, Adj_classe, ArbresDAdjacences, v1dansA1, indicator);

  if(indicator == false) { // try to go down v2 branch

    float C = recupCoutC0(v1,v2);

    if(C==recupCoutC0(v1,v2->getSon(0)) + recupCoutC0(v1,v2->getSon(1))) {
      indicator = true;
      parcoursArriereC0(v1, v2->getSon(0), Adj_classe, ArbresDAdjacences, v1dansA1);
      parcoursArriereC0(v1, v2->getSon(1), Adj_classe, ArbresDAdjacences, v1dansA1);
    }
    else if(C==recupCoutC0(v1,v2->getSon(0)) + recupCoutC1(v1,v2->getSon(1))+Crea) {
      indicator = true;
      parcoursArriereC0(v1, v2->getSon(0), Adj_classe, ArbresDAdjacences, v1dansA1);
      nb_Crea++;
      TreeTemplate<Node> * T = new TreeTemplate<Node>();
      T->setRootNode(parcoursArriereC1(v1,v2->getSon(1),Adj_classe,ArbresDAdjacences, v1dansA1));
      ArbresDAdjacences->push_back(T);
    }
    else if(C==recupCoutC1(v1,v2->getSon(0)) + recupCoutC0(v1,v2->getSon(1))+Crea) {
      indicator = true;
      nb_Crea++;
      TreeTemplate<Node> * T = new TreeTemplate<Node>();
      T->setRootNode(parcoursArriereC1(v1,v2->getSon(0),Adj_classe,ArbresDAdjacences, v1dansA1));
      ArbresDAdjacences->push_back(T);
      parcoursArriereC0(v1, v2->getSon(1), Adj_classe, ArbresDAdjacences, v1dansA1);
    }
    else if(C==recupCoutC1(v1,v2->getSon(0)) + recupCoutC1(v1,v2->getSon(1))+2*Crea) {
      indicator = true;
      nb_Crea+=2;
      TreeTemplate<Node> * T1 = new TreeTemplate<Node>();
      T1->setRootNode(parcoursArriereC1(v1,v2->getSon(0),Adj_classe,ArbresDAdjacences, v1dansA1));
      ArbresDAdjacences->push_back(T1);
      TreeTemplate<Node> * T2 = new TreeTemplate<Node>();
      T2->setRootNode(parcoursArriereC1(v1,v2->getSon(1),Adj_classe,ArbresDAdjacences, v1dansA1));
      ArbresDAdjacences->push_back(T2);
    }
  }
}

// 5
void parcoursArriereC0GDupWithGDup(Node * v1, Node * v2, vector<adjacence> * Adj_classe,vector<Tree *> * ArbresDAdjacences, bool v1dansA1) {

  if(affich_ParcoursArriere) cout <<"Je suis dans parcoursArriereC0GDupWithGDup"<<",id v1="<<v1->getId()<<",id v2="<<v2->getId()<<endl;

  bool indicator;
  parcoursArriereD00(v1, v2, Adj_classe, ArbresDAdjacences, v1dansA1, indicator);

  if(indicator == false) cout << "PB dans parcoursArriereC0GDupWithGDup : aucun des 8 couts reconnus" << endl;

  nb_GDup++;
}
  
// 6
void parcoursArriereC0SpecWithSpec(Node * v1, Node * v2, vector<adjacence> * Adj_classe,vector<Tree *> * ArbresDAdjacences, bool v1dansA1) {

  if(affich_ParcoursArriere) cout <<"Je suis dans parcoursArriereC0SpecWithSpec"<<",id v1="<<v1->getId()<<",id v2="<<v2->getId()<<endl;

  Node * fgV1=v1->getSon(0);
  Node * fdV1=v1->getSon(1);
  Node * fgV2; Node * fdV2;

  if (Esp(fgV1)==Esp(v2->getSon(0))) {
    fgV2=v2->getSon(0);
    fdV2=v2->getSon(1);
  }
  else {
    fgV2=v2->getSon(1);
    fdV2=v2->getSon(0);
  }
   
  float C = recupCoutC0(v1,v2);
  if(C==recupCoutC0(fgV1,fgV2) + recupCoutC0(fdV1,fdV2)) {
    parcoursArriereC0(fgV1,fgV2,Adj_classe,ArbresDAdjacences, v1dansA1);
    parcoursArriereC0(fdV1,fdV2,Adj_classe,ArbresDAdjacences, v1dansA1);
  }
  else if(C==recupCoutC1(fgV1,fgV2) + recupCoutC0(fdV1,fdV2) + Crea) {
    nb_Crea++;
    TreeTemplate<Node> * T = new TreeTemplate<Node>();
    T->setRootNode(parcoursArriereC1(fgV1,fgV2,Adj_classe,ArbresDAdjacences, v1dansA1));
    ArbresDAdjacences->push_back(T);
    parcoursArriereC0(fdV1,fdV2,Adj_classe,ArbresDAdjacences, v1dansA1);
  }
  else if(C==recupCoutC0(fgV1,fgV2) + recupCoutC1(fdV1,fdV2) + Crea) {
    parcoursArriereC0(fgV1,fgV2,Adj_classe,ArbresDAdjacences, v1dansA1);
    nb_Crea++;
    TreeTemplate<Node> * T = new TreeTemplate<Node>();
    T->setRootNode(parcoursArriereC1(fdV1,fdV2,Adj_classe,ArbresDAdjacences, v1dansA1));
    ArbresDAdjacences->push_back(T);
  }
  else if(C==recupCoutC1(fgV1,fgV2) + recupCoutC1(fdV1,fdV2) + 2*Crea) {
    cout<<"\t\t\tJe suis dans parcoursArriereC0SpecSpec, je passe dans le cas C1 + C1 + 2*Crea"<<endl;
    nb_Crea+=2;
    TreeTemplate<Node> * T1 = new TreeTemplate<Node>();
    T1->setRootNode(parcoursArriereC1(fgV1,fgV2,Adj_classe,ArbresDAdjacences, v1dansA1));
    ArbresDAdjacences->push_back(T1);
    TreeTemplate<Node> * T2 = new TreeTemplate<Node>();
    T2->setRootNode(parcoursArriereC1(fdV1,fdV2,Adj_classe,ArbresDAdjacences, v1dansA1));
    ArbresDAdjacences->push_back(T2);
  }
  else cout<<"PB dans parcoursArriereC0SpecWithSpec : aucun des 4 co�ts reconnus"<<endl; 
}

// 7
void parcoursArriereC0BClassWithTClass(Node * v1, Node * v2, vector<adjacence> * Adj_classe,vector<Tree *> * ArbresDAdjacences, bool v1dansA1) {

  if(affich_ParcoursArriere) cout <<"Je suis dans parcoursArriereC0BClassWithTClass"<<",id v1="<<v1->getId()<<",id v2="<<v2->getId()<<endl;
  
  parcoursArriereC0(v1, v2->getSon(0), Adj_classe, ArbresDAdjacences, v1dansA1);
}

// 8
void parcoursArriereC0TOutWithBClassGDup(Node * v1, Node * v2, vector<adjacence> * Adj_classe,vector<Tree *> * ArbresDAdjacences, bool v1dansA1) {

  // do nothing
  if(affich_ParcoursArriere) cout <<"Je suis dans parcoursArriereC0TOutWithBClassGDupRec"<<",id v1="<<v1->getId()<<",id v2="<<v2->getId()<<endl;
}

// 9
void parcoursArriereC0NullWithNull(Node * v1, Node * v2, vector<adjacence> * Adj_classe,vector<Tree *> * ArbresDAdjacences, bool v1dansA1) {

  if(affich_ParcoursArriere) cout <<"Je suis dans parcoursArriereC0NullWithNull"<<",id v1="<<v1->getId()<<",id v2="<<v2->getId()<<endl;
  
  parcoursArriereC0(v1->getSon(0), v2->getSon(0), Adj_classe, ArbresDAdjacences, v1dansA1);
}

// 10
void parcoursArriereC0RecWithRecTB(Node * v1, Node * v2, vector<adjacence> * Adj_classe,vector<Tree *> * ArbresDAdjacences, bool v1dansA1) {

  if(affich_ParcoursArriere) cout <<"Je suis dans parcoursArriereC0RecWithRecTB"<<",id v1="<<v1->getId()<<",id v2="<<v2->getId()<<endl;

  if(Esp(v1) == Esp(v2)) {
    float C = recupCoutC0(v1,v2);
    if(C == recupCoutC0(v1->getSon(0), v2->getSon(0)))
      parcoursArriereC0(v1->getSon(0), v2->getSon(0), Adj_classe, ArbresDAdjacences, v1dansA1);
    else if(C == recupCoutC1(v1->getSon(0), v2->getSon(0)) + Crea) {
      nb_Crea++;
      TreeTemplate<Node> * T = new TreeTemplate<Node>();
      T->setRootNode(parcoursArriereC1(v1->getSon(0), v2->getSon(0), Adj_classe, ArbresDAdjacences, v1dansA1));
      ArbresDAdjacences->push_back(T);
      /* we handle the gain here, rather than passing the buck to the
	 children of these two rec nodes, because we want to make note
	 of each tree that starts with a horinzontal transfer */
    }
    else cout <<"PB dans parcoursArriereC0RecWithRecTB : aucun des 2 co�ts reconnus"<<endl;
  }
  // else we do nothing (cost is 0)
}

// 11
void parcoursArriereC0RecWithTrans(Node * v1, Node * v2, vector<adjacence> * Adj_classe,vector<Tree *> * ArbresDAdjacences, bool v1dansA1) {

  if(affich_ParcoursArriere) cout <<"Je suis dans parcoursArriereC0RecWithTrans"<<",id v1="<<v1->getId()<<",id v2="<<v2->getId()<<endl;
  
  parcoursArriereC0(v1->getSon(0), v2->getSon(0), Adj_classe, ArbresDAdjacences, v1dansA1);
}

// 12
void parcoursArriereC0TransWithTransTB(Node * v1, Node * v2, vector<adjacence> * Adj_classe,vector<Tree *> * ArbresDAdjacences, bool v1dansA1) {

  if(affich_ParcoursArriere) cout <<"Je suis dans parcoursArriereC0TransWithTransTB"<<",id v1="<<v1->getId()<<",id v2="<<v2->getId()<<endl;
  
  parcoursArriereC0(v1->getSon(0), v2->getSon(0), Adj_classe, ArbresDAdjacences, v1dansA1);
  parcoursArriereC0(v1->getSon(1), v2->getSon(1), Adj_classe, ArbresDAdjacences, v1dansA1);
}

// 13
void parcoursArriereC0TBWithTB(Node * v1, Node * v2, vector<adjacence> * Adj_classe,vector<Tree *> * ArbresDAdjacences, bool v1dansA1) {

  if(affich_ParcoursArriere) cout <<"Je suis dans parcoursArriereC0TBWithTB"<<",id v1="<<v1->getId()<<",id v2="<<v2->getId()<<endl;

  float C = recupCoutC0(v1,v2);
  if(Esp(v1) == Esp(v2)) {
    if(C==recupCoutC0(v1->getSon(0),v2->getSon(0))+recupCoutC0(v1->getSon(1),v2->getSon(1))) {
      parcoursArriereC0(v1->getSon(0), v2->getSon(0), Adj_classe, ArbresDAdjacences, v1dansA1);
      parcoursArriereC0(v1->getSon(1), v2->getSon(1), Adj_classe, ArbresDAdjacences, v1dansA1);
    }
    else if(C==recupCoutC1(v1->getSon(0),v2->getSon(0))+recupCoutC0(v1->getSon(1),v2->getSon(1))+Crea) {
      nb_Crea++;
      TreeTemplate<Node> * T = new TreeTemplate<Node>();
      T->setRootNode(parcoursArriereC1(v1->getSon(0),v2->getSon(0),Adj_classe,ArbresDAdjacences,v1dansA1));
      ArbresDAdjacences->push_back(T);
    }
    else cout <<"PB dans parcoursArriereC0TBWithTB : aucun des 2 co�ts reconnus"<<endl;
  }
  // else we do nothing (cost is 0)
}

// 14
void parcoursArriereC0TransWithTOut(Node * v1, Node * v2, vector<adjacence> * Adj_classe,vector<Tree *> * ArbresDAdjacences, bool v1dansA1) {

  if(affich_ParcoursArriere) cout <<"Je suis dans parcoursArriereC0TransWithTOut"<<",id v1="<<v1->getId()<<",id v2="<<v2->getId()<<endl;
  
  parcoursArriereC0(v1->getSon(1), v2->getSon(0), Adj_classe, ArbresDAdjacences, v1dansA1);
}

// 15
void parcoursArriereC0TOutWithRecTB(Node * v1, Node * v2, vector<adjacence> * Adj_classe,vector<Tree *> * ArbresDAdjacences, bool v1dansA1) {

  if(affich_ParcoursArriere) cout <<"Je suis dans parcoursArriereC0TOutWithRecTB"<<",id v1="<<v1->getId()<<",id v2="<<v2->getId()<<endl;
    
  parcoursArriereC0(v1->getSon(0), v2, Adj_classe, ArbresDAdjacences, v1dansA1);
}

// 16
void parcoursArriereC0OutWithRecTB(Node * v1, Node * v2, vector<adjacence> * Adj_classe,vector<Tree *> * ArbresDAdjacences, bool v1dansA1) {

  if(affich_ParcoursArriere) cout <<"Je suis dans parcoursArriereC0OutWithRecTB"<<",id v1="<<v1->getId()<<",id v2="<<v2->getId()<<endl;
  
  bool indicator;
  parcoursArriereD0(v1, v2, Adj_classe, ArbresDAdjacences, v1dansA1, indicator);

  if(indicator == false) cout << "PB dans parcoursArriereC0OutWithRecTB : aucun des 4 couts reconnus" << endl;
  // else we do nothing (cost is 0)
}

// 17
void parcoursArriereC0TOutWithTOut(Node * v1, Node * v2, vector<adjacence> * Adj_classe,vector<Tree *> * ArbresDAdjacences, bool v1dansA1) {

  if(affich_ParcoursArriere) cout <<"Je suis dans parcoursArriereC0TOutWithTOut"<<",id v1="<<v1->getId()<<",id v2="<<v2->getId()<<endl;
  
  parcoursArriereC0(v1->getSon(0), v2->getSon(0), Adj_classe, ArbresDAdjacences, v1dansA1);
}

// 18
void parcoursArriereC0OutWithOut(Node * v1, Node * v2, vector<adjacence> * Adj_classe,vector<Tree *> * ArbresDAdjacences, bool v1dansA1) {

  if(affich_ParcoursArriere) cout <<"Je suis dans parcoursArriereC0OutWithOut"<<",id v1="<<v1->getId()<<",id v2="<<v2->getId()<<endl;
  
  bool indicator;
  parcoursArriereD00(v1, v2, Adj_classe, ArbresDAdjacences, v1dansA1, indicator);

  if(indicator == false) cout << "PB dans parcoursArriereC0OutWithOut : aucun des 8 couts reconnus" << endl;
  // else we do nothing (cost is 0)
}

/*********************************************************************************/

// C1 backtracking
Node * parcoursArriereC1(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1) {
  /* set up so that the order of v1 and v2 don't matter (to make the function symmetric) */

  bool indicator;
  Node * n;

  if(affich_ParcoursArriere)
    cout << "\tid v1 (ESP; BSV; TSL; TYP) = " << v1->getId() << " (" << Esp(v1) << "; " << Bootie(v1) << "; " << TimeSl(v1) << "; " << E(v1) << "), v2 = " << v2->getId() << " (" << Esp(v2) << "; " << Bootie(v2) << "; " << TimeSl(v2) << "; " << E(v2) << ")" << endl;
  
  // try to recurse with v1,v2
  n = parcoursArriereC1_asym(v1, v2, Adj_classe, ArbresDAdjacences, v1dansA1, indicator);

  // if that failed, try v2,v1
  if(indicator == false) n = parcoursArriereC1_asym(v2, v1, Adj_classe, ArbresDAdjacences, !v1dansA1, indicator);
  
  // if it still failed, then there's something wrong
  if(indicator == false) { cout << "parcoursArriereC1 Error : no case matches this pair of nodes" << endl; exit(0); }
  else return n;
}
				   
/* 
   asymmetric version of the parcoursArriereC1 function (handles only
   "half" of the cases), i.e., if
   parcoursArriereC1_asym(v1,v2,...,v1dansA1) does not contain the
   case for v1,v2, then parcoursArriereC1_asym(v2,v1,...,!v1dansA1)
   will contain the case for this (symmetric) function (otherwise
   there is an error for this symmetric function) */
Node * parcoursArriereC1_asym(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1, bool & indicator) {

  indicator = false;
  int iv1 = EvtToInt(E(v1));
  int iv2 = EvtToInt(E(v2));
  
  // the cases where S(v1) = S(v2) need not hold
  if(iv1 == 6) { // Rec
    if(iv2 == 6 || iv2 == 8) { // with Rec or TB
      if(affich_ParcoursArriere) cout << "v1 Rec et v2 Rec ou TB" << endl;
      indicator = true;
      return parcoursArriereC1RecWithRecTB(v1,v2, Adj_classe, ArbresDAdjacences, v1dansA1); } }
  else if(iv1 == 8 && iv2 == 8) { // TB with TB
    if(affich_ParcoursArriere) cout << "v1 TB et v2 TB" << endl;
    indicator = true;
    return parcoursArriereC1TBWithTB(v1,v2, Adj_classe, ArbresDAdjacences, v1dansA1); }
  else if(iv1 == 9) { // TOut
    if(iv2 == 6 || iv2 == 8) { // with Rec or TB
      if(affich_ParcoursArriere) cout << "v1 Tout et v2 Rec ou TB" << endl;
      indicator = true;
      return parcoursArriereC1TOutWithRecTB(v1,v2,Adj_classe,ArbresDAdjacences,v1dansA1); } }
  else if(iv1 == 10) { // Out
    if(iv2 == 6 || iv2 == 8) { // with Rec or TB
      if(affich_ParcoursArriere) cout << "v1 Out et v2 Rec ou TB" << endl;
      indicator = true;
      return parcoursArriereC1OutWithRecTB(v1,v2, Adj_classe, ArbresDAdjacences, v1dansA1); } }

  // check if it is a case where S(v1) = S(v2) must hold
  if(Esp(v1) == Esp(v2)) {
    if(iv1 == 1 && iv2 == 1) { // Extant with Extant
      if(affich_ParcoursArriere) cout << "v1 Extant et v2 Extant" << endl;
      indicator = true;
      return parcoursArriereC1ExtantWithExtant(v1,v2,Adj_classe, ArbresDAdjacences, v1dansA1); }
    else if(iv1 == 5) { // GLos
      if(iv2 == 5) { // with GLos
	if(affich_ParcoursArriere) cout << "v1 GLos et v2 GLos" << endl;
	indicator = true;
	return parcoursArriereC1GLosWithGLos(v1,v2, Adj_classe, ArbresDAdjacences, v1dansA1); }
      else { // with Any
	if(affich_ParcoursArriere) cout << "v1 GLos et v2 Any" << endl;
	indicator = true;
	return parcoursArriereC1GLosWithAny(v1,v2, Adj_classe, ArbresDAdjacences, v1dansA1); } }
    else if(iv1 == 4) { // GDup
      if(BClass(iv2) || TClass(iv2)) { // with BTClass 
	if(affich_ParcoursArriere) cout << "v1 GDup et v2 BTClass" << endl;
	indicator = true;
	return parcoursArriereC1GDupWithBTClass(v1,v2, Adj_classe, ArbresDAdjacences, v1dansA1); }
      else if(iv2 == 4) { // with GDup
	if(affich_ParcoursArriere) cout << "v1 GDup et v2 GDup" << endl;
	indicator = true;
	return parcoursArriereC1GDupWithGDup(v1,v2, Adj_classe, ArbresDAdjacences, v1dansA1); } }
    else if(iv1 == 2 && iv2 == 2) { // Spec with Spec
      if(affich_ParcoursArriere) cout << "v1 Spec et v2 Spec" << endl;
      indicator = true;
      return parcoursArriereC1SpecWithSpec(v1,v2, Adj_classe, ArbresDAdjacences, v1dansA1); }
    else if(BClass(iv1) && TClass(iv2)) { // BClass with TClass
      if(affich_ParcoursArriere) cout << "v1 BClass et v2 TClass" << endl;
      indicator = true;
      return parcoursArriereC1BClassWithTClass(v1,v2, Adj_classe, ArbresDAdjacences, v1dansA1); }
    else if(iv1 == 9) { // TOut
      if(BClass(iv2) || iv2 == 4) { // with BClass or GDup
	if(affich_ParcoursArriere) cout << "v1 TOut et v2 BCLass ou GDup" << endl;
	indicator = true;
	return parcoursArriereC1TOutWithBClassGDup(v1,v2, Adj_classe, ArbresDAdjacences, v1dansA1); }
      else if(iv2 == 9) { // with TOut
	if(affich_ParcoursArriere) cout << "v1 TOut et v2 TOut" << endl;
	indicator = true;
	return parcoursArriereC1TOutWithTOut(v1,v2, Adj_classe, ArbresDAdjacences, v1dansA1); } }
    else if(iv1 == 3 && iv2 == 3) { // Null with Null
      if(affich_ParcoursArriere) cout << "v1 Null et v2 Null" << endl;
      indicator = true;
      return parcoursArriereC1NullWithNull(v1,v2, Adj_classe, ArbresDAdjacences, v1dansA1); }
    else if(iv1 == 6) { // Rec
      if(iv2 == 7) { // with Trans
	if(affich_ParcoursArriere) cout << "v1 Rec et v2 Trans" << endl;
	indicator = true;
	return parcoursArriereC1RecWithTrans(v1,v2, Adj_classe, ArbresDAdjacences, v1dansA1); } }
    else if(iv1 == 7) { // Trans
      if(iv2 == 7 || iv2 == 8) { // with Trans or TB
	if(affich_ParcoursArriere) cout << "v1 Trans et v2 Trans ou TB" << endl;
	indicator = true;
	return parcoursArriereC1TransWithTransTB(v1,v2, Adj_classe, ArbresDAdjacences, v1dansA1); }
      else if(iv2 == 9) { // with TOut
	if(affich_ParcoursArriere) cout << "v1 Trans et v2 TOut" << endl;
	indicator = true;
	return parcoursArriereC1TransWithTOut(v1,v2, Adj_classe, ArbresDAdjacences, v1dansA1); } }  
    else if(iv1 == 10 && iv2 == 10) { // Out with Out
      if(affich_ParcoursArriere) cout << "v1 Out et v2 Out" << endl;
      indicator = true;
      return parcoursArriereC1OutWithOut(v1,v2, Adj_classe, ArbresDAdjacences, v1dansA1); } }
  return NULL;
}

// C0 backtracking
void parcoursArriereC0(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1) {
  /* set up so that the order of v1 and v2 don't matter (to make the function symmetric) */

  bool indicator;

  if(affich_ParcoursArriere)
    cout << "\tid v1 (ESP; BSV; TSL; TYP) = " << v1->getId() << " (" << Esp(v1) << "; " << Bootie(v1) << "; " << TimeSl(v1) << "; " << E(v1) << "), v2 = " << v2->getId() << " (" << Esp(v2) << "; " << Bootie(v2) << "; " << TimeSl(v2) << "; " << E(v2) << ")" << endl;

  // try to recurse with v1,v2
  parcoursArriereC0_asym(v1, v2, Adj_classe, ArbresDAdjacences, v1dansA1, indicator);

  // if that failed, try v2,v1
  if(indicator == false) parcoursArriereC0_asym(v2, v1, Adj_classe, ArbresDAdjacences, !v1dansA1, indicator);
  
  // if it still failed, then there's something wrong
  if(indicator == false) { cout << "parcoursArriereC0 Error : no case matches this pair of nodes" << endl; exit(0); }
}

// asymmetric version of the parcoursArriereC0 function
void parcoursArriereC0_asym(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1, bool & indicator) {

  indicator = false;
  int iv1 = EvtToInt(E(v1));
  int iv2 = EvtToInt(E(v2));

  // the cases where S(v1) = S(v2) need not hold
  if(iv1 == 6) { // Rec
    if(iv2 == 6 || iv2 == 8) { // with Rec or TB
      if(affich_ParcoursArriere) cout << "v1 Rec et v2 Rec ou TB" << endl;
      indicator = true;
      parcoursArriereC0RecWithRecTB(v1,v2, Adj_classe, ArbresDAdjacences, v1dansA1); } }
  else if(iv1 == 8 && iv2 == 8) { // TB with TB
    if(affich_ParcoursArriere) cout << "v1 TB et v2 TB" << endl;
    indicator = true;
    parcoursArriereC0TBWithTB(v1,v2, Adj_classe, ArbresDAdjacences, v1dansA1); }
  else if(iv1 == 9) { // TOut
    if(iv2 == 6 || iv2 == 8) { // with Rec or TB
      if(affich_ParcoursArriere) cout << "v1 Tout et v2 Rec ou TB" << endl;
      indicator = true;
      return parcoursArriereC0TOutWithRecTB(v1,v2,Adj_classe,ArbresDAdjacences,v1dansA1); } }
  else if(iv1 == 10) { // Out
    if(iv2 == 6 || iv2 == 8) { // with Rec or TB
      if(affich_ParcoursArriere) cout << "v1 Out et v2 Rec ou TB" << endl;
      indicator = true;
      parcoursArriereC0OutWithRecTB(v1,v2, Adj_classe, ArbresDAdjacences, v1dansA1); } }

  // check if it is a case where S(v1) = S(v2) must hold
  if(Esp(v1) == Esp(v2)) {
    if(iv1 == 1 && iv2 == 1) { // Extant with Extant
      if(affich_ParcoursArriere) cout << "v1 Extant et v2 Extant" << endl;
      indicator = true;
      parcoursArriereC0ExtantWithExtant(v1,v2,Adj_classe, ArbresDAdjacences, v1dansA1);}
    else if(iv1 == 5) { // GLos
      if(iv2 == 5) { // with GLos
	if(affich_ParcoursArriere) cout << "v1 GLos et v2 GLos" << endl;
	indicator = true;
	parcoursArriereC0GLosWithGLos(v1,v2, Adj_classe, ArbresDAdjacences, v1dansA1); }
      else { // with Any
	if(affich_ParcoursArriere) cout << "v1 GLos et v2 Any" << endl;
	indicator = true;
	parcoursArriereC0GLosWithAny(v1,v2, Adj_classe, ArbresDAdjacences, v1dansA1); } }
    else if(iv1 == 4) { // GDup
      if(BClass(iv2) || TClass(iv2)) { // with BTClass 
	if(affich_ParcoursArriere) cout << "v1 GDup et v2 BTClass" << endl;
	indicator = true;
	parcoursArriereC0GDupWithBTClass(v1,v2, Adj_classe, ArbresDAdjacences, v1dansA1); }
      else if(iv2 == 4) { // with GDup
	if(affich_ParcoursArriere) cout << "v1 GDup et v2 GDup" << endl;
	indicator = true;
	parcoursArriereC0GDupWithGDup(v1,v2, Adj_classe, ArbresDAdjacences, v1dansA1); } }
    else if(iv1 == 2 && iv2 == 2) { // Spec with Spec
      if(affich_ParcoursArriere) cout << "v1 Spec et v2 Spec" << endl;
      indicator = true;
      parcoursArriereC0SpecWithSpec(v1,v2, Adj_classe, ArbresDAdjacences, v1dansA1); }
    else if(BClass(iv1) && TClass(iv2)) { // BClass with TClass
      if(affich_ParcoursArriere) cout << "v1 BClass et v2 TClass" << endl;
      indicator = true;
      parcoursArriereC0BClassWithTClass(v1,v2, Adj_classe, ArbresDAdjacences, v1dansA1); }
    else if(iv1 == 9) { // TOut
      if(BClass(iv2) || iv2 == 4 || iv2 == 6) { // with BClass GDup or Rec
	if(affich_ParcoursArriere) cout << "v1 TOut et v2 BCLass ou GDup" << endl;
	indicator = true;
	parcoursArriereC0TOutWithBClassGDup(v1,v2, Adj_classe, ArbresDAdjacences, v1dansA1); }
      else if(iv2 == 6 || iv2 == 8) { // with Rec or TB
	if(affich_ParcoursArriere) cout << "v1 TOut et v2 Rec ou TB" << endl;
	indicator = true;
	return parcoursArriereC0TOutWithRecTB(v1,v2,Adj_classe,ArbresDAdjacences,v1dansA1); }
      else if(iv2 == 9) { // with TOut
	if(affich_ParcoursArriere) cout << "v1 TOut et v2 TOut" << endl;
	indicator = true;
	parcoursArriereC0TOutWithTOut(v1,v2, Adj_classe, ArbresDAdjacences, v1dansA1); } }
    else if(iv1 == 3 && iv2 == 3) { // Null with Null
      if(affich_ParcoursArriere) cout << "v1 Null et v2 Null" << endl;
      indicator = true;
      parcoursArriereC0NullWithNull(v1,v2, Adj_classe, ArbresDAdjacences, v1dansA1); }
    else if(iv1 == 6) { // Rec
      if(iv2 == 7) { // with Trans
	if(affich_ParcoursArriere) cout << "v1 Rec et v2 Trans" << endl;
	indicator = true;
	parcoursArriereC0RecWithTrans(v1,v2, Adj_classe, ArbresDAdjacences, v1dansA1); } }
    else if(iv1 == 7) { // Trans
      if(iv2 == 7 || iv2 == 8) { // with Trans or TB
	if(affich_ParcoursArriere) cout << "v1 Trans et v2 Trans ou TB" << endl;
	indicator = true;
	parcoursArriereC0TransWithTransTB(v1,v2, Adj_classe, ArbresDAdjacences, v1dansA1); }
      else if(iv2 == 9) { // with TOut
	if(affich_ParcoursArriere) cout << "v1 Trans et v2 TOut" << endl;
	indicator = true;
	parcoursArriereC0TransWithTOut(v1,v2, Adj_classe, ArbresDAdjacences, v1dansA1); } }
    else if(iv1 == 10 && iv2 == 10) { // Out with Out
      if(affich_ParcoursArriere) cout << "v1 Out et v2 Out" << endl;
      indicator = true;
      parcoursArriereC0OutWithOut(v1,v2, Adj_classe, ArbresDAdjacences, v1dansA1); } }
  // default : do nothing
}
