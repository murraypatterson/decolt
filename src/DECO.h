#ifndef _DECO_H_
#define _DECO_H_


//#include <Phyl/Tree.h>
#include <Phyl/TreeTemplate.h>
//#include <Phyl/TreeTemplateTools.h>
//#include <Phyl/NodeTemplate.h>
//#include <Phyl/Node.h>

using namespace bpp;

// The STL library:
//#include <cstdlib>
#include <iostream>
//#include <stringstream>
//#include <string>
//#include <vector>

using namespace std;

void ordonnePostfixe(Node * n, vector<Node *> & v);

int compteFeuilles(Node * n);
void retourneFeuilles(Node * n, vector<string> & feuilles);
void retourneFeuillesNomsGenes(Node * n, vector<string> & feuilles);
void retourneFeuillesId(Node* n, vector<string>& feuilles);
void ecritAdjAncestrale(Node * n, int num1, int num2, TreeTemplate<Node> * A1, TreeTemplate<Node> * A2, ofstream & OfficAdj2);
void ecritAdjAncestrales(Node * n, int num1, int num2, TreeTemplate<Node> * A1, TreeTemplate<Node> * A2, ofstream & OfficAdj2);
void ecritSORTIEAdj(Node * n, int num1, int num2, ofstream & OfficAdj2);
void ecritSORTIEDup(vector<adjacence> DUP, int num1, TreeTemplate<Node>* Arbre1, int num2, ofstream & OfficSORTIE_dup);
void DECO(TreeTemplate<Node>* S, TreeTemplate<Node>* Arbre1, TreeTemplate<Node>* Arbre2, vector<adjacence> * Adj_classe, ofstream & OfficAdj2, ofstream & OfficArbresAdj, ofstream & OfficSORTIE_dup, ofstream & OfficSORTIE_trf, string nom_classe);

#endif //_DECO_H_
