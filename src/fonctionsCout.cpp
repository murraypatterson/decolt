#include "basicRoutines.h"
#include "fonctionsCout.h"
#include "Reconcil.h"

// defines the standard for event to int
int EvtToInt(string e) {
  if (e=="Extant") return 1; // b-class ("bifurcation"-type events)
  else if (e=="Spec") return 2; // ...
  else if (e=="Null") return 3; // end b-class
  else if (e=="GDup") return 4;
  else if (e=="GLos") return 5;
  else if (e=="Reception") return 6; // t-class ("transfer"-type events)
  else if (e=="Transfer") return 7; // ...
  else if (e=="TransferBack") return 8; // end t-class
  else if (e=="TransferOut") return 9;
  else if (e=="Outside") return 10;
  else if (e=="ADup") return 11; // extras only for adj trees
  else if (e=="ALos") return 12;
  else if (e=="Crea") return 13;
  else if (e=="Break") return 14;
  else { cout<<"Probl�me de EvtToInt() : l'�v�nement "<<e<<" n'est pas connu"<<endl; return -1; }
}

// if event (to int) is in the b-class
bool BClass(int ev) {
  if(ev >= 1 && ev <=3)
    return true;
  return false;
}

// if event (to int) is in the t-class
bool TClass(int ev) {
  if(ev >=6 && ev <= 8)
    return true;
  return false;
}

// true if a is in set Adjacences of adjacencies, false, o.w.
bool appAdj(adjacence a, vector<adjacence> * Adjacences) { 
  //cout <<"Je suis dans appAdj"<<endl;
  bool trouve=false;
  vector<adjacence>::iterator it=Adjacences->begin();
  while (!trouve && it!=Adjacences->end()) {
    if (a.gene1==(*it).gene1 && a.gene2==(*it).gene2)
      trouve=true;
    it++;
  }
  return trouve;
}

float recupCoutC1(Node * v1, Node * v2) {
  pair<Node *,Node *> p(v1,v2);
  return C1[p];
}

float recupCoutC0(Node * v1, Node * v2) {
  pair<Node *,Node *> p(v1,v2);
  return C0[p];
}

//
// cases for C1
//

// 1
float C1ExtantWithExtant(Node * v1, Node * v2, vector<adjacence> * Adj_classe) {
  if(affich_CalculCout)
    cout <<"Je suis dans C1ExtantWithExtant"<<endl;
  string nomV1 = v1->getName();
  string nomV2 = v2->getName();
  string nom_gene1=nomV1.substr(0,nomV1.rfind(sep,nomV1.length()-1));
  string nom_gene2=nomV2.substr(0,nomV2.rfind(sep,nomV2.length()-1));

  string espece=nomV2.substr(nomV2.rfind(sep,nomV2.length())+1,nomV2.length());
  espece=genes_especes[nom_gene1];
  
  cout << nom_gene1+sepAdj+nom_gene2+sep+espece << endl;

  //On cherche d'adj dans les deux sens g1-g2 ou g2-g1
  adjacence a12,a21;
  a12.gene1=nom_gene1;
  a12.gene2=nom_gene2;
  a21.gene1=nom_gene2;
  a21.gene2=nom_gene1;
  if (appAdj(a12,Adj_classe) || appAdj(a21,Adj_classe))
    return 0.0;
  else
    return INFINI;//Break;
}

// 2
float C1GLosWithAny(Node * v1, Node * v2) {
  if(affich_CalculCout)
    cout <<"Je suis dans C1GLosWithAny"<<endl;
  return 0.0;
}

// 3
float C1GLosWithGLos(Node * v1, Node * v2) {
  if(affich_CalculCout)
    cout <<"Je suis dans C1GLosWithGLos"<<endl;
  return 0.0;
}

// D1
float D1(Node * v1, Node * v2) {
  // Severine used forms like this for debugging (keep them in mind if
  // you are stuck)
  //
  //Pour savoir si on passe dans les cas rajout�s :
  // if (min_pos(min(recupCoutC1(v1->getSon(0),v2)+recupCoutC0(v1->getSon(1),v2),
  // 		   recupCoutC0(v1->getSon(0),v2)+recupCoutC1(v1->getSon(1),v2),
  // 		   recupCoutC1(v1->getSon(0),v2)+recupCoutC1(v1->getSon(1),v2)+Crea),
  // 	       recupCoutC0(v1->getSon(0),v2)+recupCoutC0(v1->getSon(1),v2)+Break)==2)
  //    cout<<"\t\t\tJe suis dans C1GDupGDup D1, je passe dans le cas C0 + C0 + Break"<<endl;
  return min(
	     recupCoutC1(v1->getSon(0),v2)+recupCoutC0(v1->getSon(1),v2),
	     recupCoutC0(v1->getSon(0),v2)+recupCoutC1(v1->getSon(1),v2),
	     recupCoutC1(v1->getSon(0),v2)+recupCoutC1(v1->getSon(1),v2)+Crea,
	     recupCoutC0(v1->getSon(0),v2)+recupCoutC0(v1->getSon(1),v2)+Break
	     );
}

// 4
float C1GDupWithBTClass(Node * v1, Node * v2) {
  if(affich_CalculCout)
    cout <<"Je suis dans C1GDupWithBTClass"<<endl;
  return(D1(v1,v2));
}

// D2
float D2(Node * v1, Node * v2) {
  return min(
	     recupCoutC1(v1,v2->getSon(0))+recupCoutC0(v1,v2->getSon(1)),
	     recupCoutC0(v1,v2->getSon(0))+recupCoutC1(v1,v2->getSon(1)),
	     recupCoutC1(v1,v2->getSon(0))+recupCoutC1(v1,v2->getSon(1))+Crea,
	     recupCoutC0(v1,v2->getSon(0))+recupCoutC0(v1,v2->getSon(1))+Break
	     );
}

// D12
float D12(Node * v1, Node * v2) {
  return min(
	     recupCoutC1(v1->getSon(0),v2->getSon(0))+recupCoutC1(v1->getSon(1),v2->getSon(1))+
	     recupCoutC0(v1->getSon(0),v2->getSon(1))+recupCoutC0(v1->getSon(1),v2->getSon(0)),
	     recupCoutC1(v1->getSon(0),v2->getSon(0))+recupCoutC1(v1->getSon(1),v2->getSon(1))+
	     recupCoutC0(v1->getSon(0),v2->getSon(1))+recupCoutC1(v1->getSon(1),v2->getSon(0))+Crea,
	     recupCoutC1(v1->getSon(0),v2->getSon(0))+recupCoutC1(v1->getSon(1),v2->getSon(1))+
	     recupCoutC1(v1->getSon(0),v2->getSon(1))+recupCoutC0(v1->getSon(1),v2->getSon(0))+Crea,
	     recupCoutC1(v1->getSon(0),v2->getSon(0))+recupCoutC1(v1->getSon(1),v2->getSon(1))+
	     recupCoutC1(v1->getSon(0),v2->getSon(1))+recupCoutC1(v1->getSon(1),v2->getSon(0))+2*Crea,

	     recupCoutC1(v1->getSon(0),v2->getSon(0))+recupCoutC0(v1->getSon(1),v2->getSon(1))+
	     recupCoutC0(v1->getSon(0),v2->getSon(1))+recupCoutC0(v1->getSon(1),v2->getSon(0))+Break,
	     recupCoutC1(v1->getSon(0),v2->getSon(0))+recupCoutC0(v1->getSon(1),v2->getSon(1))+
	     recupCoutC0(v1->getSon(0),v2->getSon(1))+recupCoutC1(v1->getSon(1),v2->getSon(0))+Break+Crea,
	     recupCoutC1(v1->getSon(0),v2->getSon(0))+recupCoutC0(v1->getSon(1),v2->getSon(1))+
	     recupCoutC1(v1->getSon(0),v2->getSon(1))+recupCoutC0(v1->getSon(1),v2->getSon(0))+Break+Crea,
	     recupCoutC0(v1->getSon(0),v2->getSon(0))+recupCoutC1(v1->getSon(1),v2->getSon(1))+
	     recupCoutC0(v1->getSon(0),v2->getSon(1))+recupCoutC0(v1->getSon(1),v2->getSon(0))+Break,

	     recupCoutC0(v1->getSon(0),v2->getSon(0))+recupCoutC1(v1->getSon(1),v2->getSon(1))+
	     recupCoutC0(v1->getSon(0),v2->getSon(1))+recupCoutC1(v1->getSon(1),v2->getSon(0))+Break+Crea,
	     recupCoutC0(v1->getSon(0),v2->getSon(0))+recupCoutC1(v1->getSon(1),v2->getSon(1))+
	     recupCoutC1(v1->getSon(0),v2->getSon(1))+recupCoutC0(v1->getSon(1),v2->getSon(0))+Break+Crea,
	     recupCoutC0(v1->getSon(0),v2->getSon(0))+recupCoutC0(v1->getSon(1),v2->getSon(1))+
	     recupCoutC1(v1->getSon(0),v2->getSon(1))+recupCoutC1(v1->getSon(1),v2->getSon(0)),
	     recupCoutC0(v1->getSon(0),v2->getSon(0))+recupCoutC1(v1->getSon(1),v2->getSon(1))+
	     recupCoutC1(v1->getSon(0),v2->getSon(1))+recupCoutC1(v1->getSon(1),v2->getSon(0))+Crea,

	     recupCoutC1(v1->getSon(0),v2->getSon(0))+recupCoutC0(v1->getSon(1),v2->getSon(1))+
	     recupCoutC1(v1->getSon(0),v2->getSon(1))+recupCoutC1(v1->getSon(1),v2->getSon(0))+Crea,
	     recupCoutC0(v1->getSon(0),v2->getSon(0))+recupCoutC0(v1->getSon(1),v2->getSon(1))+
	     recupCoutC1(v1->getSon(0),v2->getSon(1))+recupCoutC0(v1->getSon(1),v2->getSon(0))+Break,
	     recupCoutC0(v1->getSon(0),v2->getSon(0))+recupCoutC0(v1->getSon(1),v2->getSon(1))+
	     recupCoutC0(v1->getSon(0),v2->getSon(1))+recupCoutC1(v1->getSon(1),v2->getSon(0))+Break,
	     recupCoutC0(v1->getSon(0),v2->getSon(0))+recupCoutC0(v1->getSon(1),v2->getSon(1))+
	     recupCoutC0(v1->getSon(0),v2->getSon(1))+recupCoutC0(v1->getSon(1),v2->getSon(0))+2*Break
	     );
}

// 5
float C1GDupWithGDup(Node * v1, Node * v2) {
  if(affich_CalculCout)
    cout <<"Je suis dans C1GDupWithGDup"<<endl;
  return min(
	     D1(v1,v2),
	     D2(v1,v2),
	     D12(v1,v2)
	     );
}

// 6
float C1SpecWithSpec(Node * v1, Node * v2) {
  if(affich_CalculCout)
    cout <<"Je suis dans C1SpecWithSpec"<<endl;
  Node * fgV1=v1->getSon(0); Node * fdV1=v1->getSon(1);
  Node * fgV2; Node * fdV2;
  if (Esp(fgV1)==Esp(v2->getSon(0))) { fgV2=v2->getSon(0); fdV2=v2->getSon(1); }
  else { fgV2=v2->getSon(1); fdV2=v2->getSon(0); }


  if(affich_CalculCout) {
    cout << "C1(x,y) = min {" << endl;
    cout << "  C1(xa,ya) + C1(xb,yb) = " << recupCoutC1(fgV1,fgV2) << " + " << recupCoutC1(fdV1,fdV2) << endl;
    cout << "  C1(xa,ya) + C0(xb,yb) + Br = " << recupCoutC1(fgV1,fgV2) << " + " << recupCoutC0(fdV1,fdV2) << " + " << Break << endl;
    cout << "  C0(xa,ya) + C1(xb,yb) + Br = " << recupCoutC0(fgV1,fgV2) << " + " << recupCoutC1(fdV1,fdV2) << " + " << Break << endl;
    cout << "  C0(xa,ya) + C0(xb,yb) + 2*Br = " << recupCoutC0(fgV1,fgV2) << " + " << recupCoutC0(fdV1,fdV2) << " + " << 2*Break << endl;
    cout << "}" << endl;
  }

  return min(
	     recupCoutC1(fgV1,fgV2) + recupCoutC1(fdV1,fdV2),
	     recupCoutC1(fgV1,fgV2) + recupCoutC0(fdV1,fdV2) + Break,
	     recupCoutC0(fgV1,fgV2) + recupCoutC1(fdV1,fdV2) + Break,
	     recupCoutC0(fgV1,fgV2) + recupCoutC0(fdV1,fdV2) + 2*Break
	     );
}

// 7
float C1BClassWithTClass(Node * v1, Node * v2) {
  if(affich_CalculCout)
    cout << "Je suis dans C1BClassWithTClass" << endl;
  return recupCoutC1(v1,v2->getSon(0));
}

// 8
float C1TOutWithBClassGDupOut(Node * v1, Node * v2) {
  if(affich_CalculCout)
    cout << "Je suis dans C1TOutWithBClassGDupOut" << endl;
  return 0.0;
}

// 9
float C1NullWithNull(Node * v1, Node * v2) {
  if(affich_CalculCout)
    cout << "Je suis dans C1NullWithNull" << endl;  
  return recupCoutC1(v1->getSon(0),v2->getSon(0));
}

// 10
float C1RecWithRecTB(Node * v1, Node * v2) {
  if(affich_CalculCout)
    cout << "Je suis dans C1RecWithRecTB" << endl;
  if(Esp(v1)==Esp(v2))
    return min(
	       recupCoutC1(v1->getSon(0),v2->getSon(0)),
	       recupCoutC0(v1->getSon(0),v2->getSon(0))
	       );
  else
    return 0.0;
}

// 11
float C1RecWithTrans(Node * v1, Node * v2) {
  if(affich_CalculCout)
    cout << "Je suis dans C1RecWithTrans" << endl;  
  return recupCoutC1(v1->getSon(0),v2->getSon(0));
}

// 12
float C1TransWithTransTB(Node * v1, Node * v2) {
  if(affich_CalculCout)
    cout << "Je suis dans C1TransWithTransTB" << endl;
//Modification by W
  return min(
		recupCoutC1(v1->getSon(0),v2->getSon(0)),
		recupCoutC0(v1->getSon(0),v2->getSon(0)) + Break
		)
	 + recupCoutC1(v1->getSon(1),v2->getSon(1));

//end modification by W
}

// 13
float C1TBWithTB(Node * v1, Node * v2) {
  if(affich_CalculCout)
    cout << "Je suis dans C1TBWithTB" << endl;
  if(Esp(v1)==Esp(v2))
    return min(
    	       recupCoutC1(v1->getSon(0),v2->getSon(0)),
    	       recupCoutC0(v1->getSon(0),v2->getSon(0))
    	       )
      + recupCoutC1(v1->getSon(1),v2->getSon(1));
  else
    return 0.0;
}

// 14
float C1TransWithTOut(Node * v1, Node * v2) {
  if(affich_CalculCout)
    cout << "Je suis dans C1TransWithTOut" << endl;
  return recupCoutC1(v1->getSon(1),v2->getSon(0));
}

// O1
float O1(Node * v1, Node * v2) {
  return min(
	     recupCoutC0(v1->getSon(0),v2)+recupCoutC0(v1->getSon(1),v2),
	     recupCoutC1(v1->getSon(0),v2)+recupCoutC0(v1->getSon(1),v2),
	     recupCoutC0(v1->getSon(0),v2)+recupCoutC1(v1->getSon(1),v2),
	     recupCoutC1(v1->getSon(0),v2)+recupCoutC1(v1->getSon(1),v2)+Crea
	     );
}

// 15
float C1OutWithRecTB(Node * v1, Node * v2) {
  if(affich_CalculCout)
    cout << "Je suis dans C1OutWithRecTB" << endl;
  return O1(v1,v2);
}

// 16
float C1TOutWithTOut(Node * v1, Node * v2) {
  if(affich_CalculCout)
    cout << "Je suis dans C1TOutWithTOut" << endl;
  return recupCoutC1(v1->getSon(0),v2->getSon(0));
}

// O2
float O2(Node * v1, Node * v2) {
  return min(
	     recupCoutC0(v1,v2->getSon(0))+recupCoutC0(v1,v2->getSon(1)),
	     recupCoutC1(v1,v2->getSon(0))+recupCoutC0(v1,v2->getSon(1)),
	     recupCoutC0(v1,v2->getSon(0))+recupCoutC1(v1,v2->getSon(1)),
	     recupCoutC1(v1,v2->getSon(0))+recupCoutC1(v1,v2->getSon(1))+Crea
	     );
}

// O12
float O12(Node * v1, Node * v2) {
  return min(
	     recupCoutC1(v1->getSon(0),v2->getSon(0))+recupCoutC1(v1->getSon(1),v2->getSon(1))+
	     recupCoutC0(v1->getSon(0),v2->getSon(1))+recupCoutC0(v1->getSon(1),v2->getSon(0)),
	     recupCoutC1(v1->getSon(0),v2->getSon(0))+recupCoutC1(v1->getSon(1),v2->getSon(1))+
	     recupCoutC0(v1->getSon(0),v2->getSon(1))+recupCoutC1(v1->getSon(1),v2->getSon(0))+Crea,
	     recupCoutC1(v1->getSon(0),v2->getSon(0))+recupCoutC1(v1->getSon(1),v2->getSon(1))+
	     recupCoutC1(v1->getSon(0),v2->getSon(1))+recupCoutC0(v1->getSon(1),v2->getSon(0))+Crea,
	     recupCoutC1(v1->getSon(0),v2->getSon(0))+recupCoutC1(v1->getSon(1),v2->getSon(1))+
	     recupCoutC1(v1->getSon(0),v2->getSon(1))+recupCoutC1(v1->getSon(1),v2->getSon(0))+2*Crea,

	     recupCoutC1(v1->getSon(0),v2->getSon(0))+recupCoutC0(v1->getSon(1),v2->getSon(1))+
	     recupCoutC0(v1->getSon(0),v2->getSon(1))+recupCoutC0(v1->getSon(1),v2->getSon(0)),
	     recupCoutC1(v1->getSon(0),v2->getSon(0))+recupCoutC0(v1->getSon(1),v2->getSon(1))+
	     recupCoutC0(v1->getSon(0),v2->getSon(1))+recupCoutC1(v1->getSon(1),v2->getSon(0))+Crea,
	     recupCoutC1(v1->getSon(0),v2->getSon(0))+recupCoutC0(v1->getSon(1),v2->getSon(1))+
	     recupCoutC1(v1->getSon(0),v2->getSon(1))+recupCoutC0(v1->getSon(1),v2->getSon(0))+Crea,
	     recupCoutC0(v1->getSon(0),v2->getSon(0))+recupCoutC1(v1->getSon(1),v2->getSon(1))+
	     recupCoutC0(v1->getSon(0),v2->getSon(1))+recupCoutC0(v1->getSon(1),v2->getSon(0)),

	     recupCoutC0(v1->getSon(0),v2->getSon(0))+recupCoutC1(v1->getSon(1),v2->getSon(1))+
	     recupCoutC0(v1->getSon(0),v2->getSon(1))+recupCoutC1(v1->getSon(1),v2->getSon(0))+Crea,
	     recupCoutC0(v1->getSon(0),v2->getSon(0))+recupCoutC1(v1->getSon(1),v2->getSon(1))+
	     recupCoutC1(v1->getSon(0),v2->getSon(1))+recupCoutC0(v1->getSon(1),v2->getSon(0))+Crea,
	     recupCoutC0(v1->getSon(0),v2->getSon(0))+recupCoutC0(v1->getSon(1),v2->getSon(1))+
	     recupCoutC1(v1->getSon(0),v2->getSon(1))+recupCoutC1(v1->getSon(1),v2->getSon(0)),
	     recupCoutC0(v1->getSon(0),v2->getSon(0))+recupCoutC1(v1->getSon(1),v2->getSon(1))+
	     recupCoutC1(v1->getSon(0),v2->getSon(1))+recupCoutC1(v1->getSon(1),v2->getSon(0))+Crea,

	     recupCoutC1(v1->getSon(0),v2->getSon(0))+recupCoutC0(v1->getSon(1),v2->getSon(1))+
	     recupCoutC1(v1->getSon(0),v2->getSon(1))+recupCoutC1(v1->getSon(1),v2->getSon(0))+Crea,
	     recupCoutC0(v1->getSon(0),v2->getSon(0))+recupCoutC0(v1->getSon(1),v2->getSon(1))+
	     recupCoutC1(v1->getSon(0),v2->getSon(1))+recupCoutC0(v1->getSon(1),v2->getSon(0)),
	     recupCoutC0(v1->getSon(0),v2->getSon(0))+recupCoutC0(v1->getSon(1),v2->getSon(1))+
	     recupCoutC0(v1->getSon(0),v2->getSon(1))+recupCoutC1(v1->getSon(1),v2->getSon(0)),
	     recupCoutC0(v1->getSon(0),v2->getSon(0))+recupCoutC0(v1->getSon(1),v2->getSon(1))+
	     recupCoutC0(v1->getSon(0),v2->getSon(1))+recupCoutC0(v1->getSon(1),v2->getSon(0))
	     );
}

// 17
float C1OutWithOut(Node * v1, Node * v2) {
  if(affich_CalculCout)
    cout << "Je suis dans C1OutWithOut" << endl;
  return min(
	     O1(v1,v2),
	     O2(v1,v2),
	     O12(v1,v2)
	     );
}

// 18
float C1TOutWithRecTB(Node * v1, Node * v2) {
  if(affich_CalculCout)
    cout << "Je suis dans C1TOutWithRecTB" << endl;
  return recupCoutC1(v1->getSon(0),v2);
}

//
// cases for C0
//

// 1
float C0ExtantWithExtant(Node * v1, Node * v2, vector<adjacence> * Adj_classe) {
  if(affich_CalculCout)
    cout <<"Je suis dans C0ExtantWithExtant"<<endl;
  string nomV1 = v1->getName();
  string nomV2 = v2->getName();
  string nom_gene1=nomV1.substr(0,nomV1.rfind(sep,nomV1.length()-1));
  string nom_gene2=nomV2.substr(0,nomV2.rfind(sep,nomV2.length()-1));
  //On cherche d'adj dans les deux sens g1-g2 ou g2-g1
  adjacence a12,a21;
  a12.gene1=nom_gene1;
  a12.gene2=nom_gene2;
  a21.gene1=nom_gene2;
  a21.gene2=nom_gene1;
  if (appAdj(a12,Adj_classe) || appAdj(a21,Adj_classe))
    return INFINI;//Crea;
  else
    return 0.0;
}

// 2
float C0GLosWithAny(Node * v1, Node * v2) {
  if(affich_CalculCout)
    cout <<"Je suis dans C0GLosWithAny"<<endl;
  return 0.0;
}

// 3
float C0GLosWithGLos(Node * v1, Node * v2) {
  if(affich_CalculCout)
    cout <<"Je suis dans C0GLosWithGLos"<<endl;
  return 0.0;
}

// D0
float D0(Node * v1, Node * v2) {
  return min(
	     recupCoutC0(v1->getSon(0),v2) + recupCoutC0(v1->getSon(1),v2),
	     recupCoutC0(v1->getSon(0),v2) + recupCoutC1(v1->getSon(1),v2)+Crea,
	     recupCoutC1(v1->getSon(0),v2) + recupCoutC0(v1->getSon(1),v2)+Crea,
	     recupCoutC1(v1->getSon(0),v2) + recupCoutC1(v1->getSon(1),v2)+2*Crea
	     );
}

// 4
float C0GDupWithBTClass(Node * v1, Node * v2) {
  if(affich_CalculCout)
    cout <<"Je suis dans C0GDupWithBTClass"<<endl;
  return D0(v1,v2);
}

// D00
float D00(Node * v1, Node * v2) {
  return min(
	     D0(v1,v2),
	     recupCoutC0(v1,v2->getSon(0)) + recupCoutC0(v1,v2->getSon(1)),
	     recupCoutC0(v1,v2->getSon(0)) + recupCoutC1(v1,v2->getSon(1))+Crea,
	     recupCoutC1(v1,v2->getSon(0)) + recupCoutC0(v1,v2->getSon(1))+Crea,
	     recupCoutC1(v1,v2->getSon(0)) + recupCoutC1(v1,v2->getSon(1))+2*Crea
	     );
}

// 5
float C0GDupWithGDup(Node * v1, Node * v2) {
  if(affich_CalculCout)
    cout <<"Je suis dans C0GDupWithGDup"<<endl;
  return D00(v1,v2);
}

// 6
float C0SpecWithSpec(Node * v1, Node * v2) {
  if(affich_CalculCout)
    cout <<"Je suis dans C0SpecWithSpec"<<endl;
  Node * fgV1=v1->getSon(0); Node * fdV1=v1->getSon(1);
  Node * fgV2; Node * fdV2;
  if (Esp(fgV1)==Esp(v2->getSon(0))) { fgV2=v2->getSon(0); fdV2=v2->getSon(1); }
  else { fgV2=v2->getSon(1); fdV2=v2->getSon(0); }

  /*
  cout << "C0(ca(v1),ca(v2)) + C0(cb(v1),cb(v2)) : " <<
    recupCoutC0(fgV1,fgV2) + recupCoutC0(fdV1,fdV2) << endl;
  cout << "C1(ca(v1),ca(v2)) + C0(cb(v1),cb(v2)) + Gain : " <<
    recupCoutC1(fgV1,fgV2) + recupCoutC0(fdV1,fdV2) + Crea << endl;
  cout << "C0(ca(v1),ca(v2)) + C1(cb(v1),cb(v2)) + Gain : " <<
    recupCoutC0(fgV1,fgV2) + recupCoutC1(fdV1,fdV2) + Crea << endl;
  cout << "C1(ca(v1),ca(v2)) + C1(cb(v1),cb(v2)) + 2*Gain : " <<
    recupCoutC1(fgV1,fgV2) + recupCoutC1(fdV1,fdV2) + 2*Crea << endl << endl;
  */

  return min(
	     recupCoutC0(fgV1,fgV2) + recupCoutC0(fdV1,fdV2),
	     recupCoutC1(fgV1,fgV2) + recupCoutC0(fdV1,fdV2) + Crea,
	     recupCoutC0(fgV1,fgV2) + recupCoutC1(fdV1,fdV2) + Crea,
	     recupCoutC1(fgV1,fgV2) + recupCoutC1(fdV1,fdV2) + 2*Crea
	     );
}

// 7
float C0BClassWithTClass(Node * v1, Node * v2) {
  if(affich_CalculCout)
    cout <<"Je suis dans C0BClassWithTClass"<<endl;
  return recupCoutC0(v1,v2->getSon(0));
}

// 8
float C0TOutWithBClassGDupOut(Node * v1, Node * v2) {
  if(affich_CalculCout)
    cout <<"Je suis dans C0TOutWithBClassGDupOut"<<endl;
  return 0.0;
}

// 9
float C0NullWithNull(Node * v1, Node * v2) {
  if(affich_CalculCout)
    cout << "Je suis dans C0NullWithNull" << endl;  
  return recupCoutC0(v1->getSon(0),v2->getSon(0));
}

// 10
float C0RecWithRecTB(Node * v1, Node * v2) {
  if(affich_CalculCout)
    cout << "Je suis dans C0RecWithRecTB" << endl;
  if(Esp(v1)==Esp(v2))
    return min(
	       recupCoutC0(v1->getSon(0),v2->getSon(0)),
	       recupCoutC1(v1->getSon(0),v2->getSon(0)) + Crea
	       );
  else
    return 0.0;
}

// 11
float C0RecWithTrans(Node * v1, Node * v2) {
  if(affich_CalculCout)
    cout << "Je suis dans C0RecWithTrans" << endl;
  return recupCoutC0(v1->getSon(0),v2->getSon(0));
}

// 12
float C0TransWithTransTB(Node * v1, Node * v2) {
  if(affich_CalculCout)
    cout << "Je suis dans C0TransWithTransTB" << endl;
//modification by W
  return min(
		recupCoutC0(v1->getSon(0),v2->getSon(0)),
		recupCoutC1(v1->getSon(0),v2->getSon(0)) + Crea
	)
 + recupCoutC0(v1->getSon(1),v2->getSon(1));
//end modification by W
}

// 13
float C0TBWithTB(Node * v1, Node * v2) {
  if(affich_CalculCout)
    cout << "Je suis dans C0TBWithTB" << endl;
  if(Esp(v1)==Esp(v2))
    return min(
	       recupCoutC0(v1->getSon(0),v2->getSon(0)),
	       recupCoutC1(v1->getSon(0),v2->getSon(0)) + Crea
	       )
      + recupCoutC0(v1->getSon(1),v2->getSon(1));
  // we do we not have min c0,c1 here as well, because this is handled
  // at the reception node?  maybe you should ask
  else
    return 0.0;
}

// 14
float C0TransWithTOut(Node * v1, Node * v2) {
  if(affich_CalculCout)
    cout << "Je suis dans C0TransWithTOut" << endl;
  return recupCoutC0(v1->getSon(1),v2->getSon(0));
}

// 15
float C0OutWithRecTB(Node * v1, Node * v2) {
  if(affich_CalculCout)
    cout << "Je suis dans C0OutWithRecTB" << endl;
  return D0(v1,v2);
}

// 16
float C0TOutWithTOut(Node * v1, Node * v2) {
  if(affich_CalculCout)
    cout << "Je suis dans C0TOutWithTOut" << endl;
  return min(
	     recupCoutC0(v1->getSon(0),v2->getSon(0)),
	     recupCoutC1(v1->getSon(0),v2->getSon(0)) + Crea
	     );
}

// 17
float C0OutWithOut(Node * v1, Node * v2) {
  if(affich_CalculCout)
    cout << "Je suis dans C0OutWithOut" << endl;
  return D00(v1,v2);
}

// 18
float C0TOutWithRecTB(Node * v1, Node * v2) {
  if(affich_CalculCout)
    cout << "Je suis dans C0TOutWithRecTB" << endl;
  return recupCoutC0(v1->getSon(0),v2);
}

/*********************************************************************************/

// C1
//
// On suppose que les noeuds sont bien de la m�me esp�ce (not any
// more, but rather we compute, based on this, inside the function)
float calculeC1(Node * v1, Node * v2, vector<adjacence> * Adj_classe, bool & Interesting) {

  bool Ii=true;
  bool Ir=true;
  float C1i = calculeC1_asym(v1,v2,Adj_classe,Ii);
  float C1r = calculeC1_asym(v2,v1,Adj_classe,Ir);
  
  Interesting=true;
  if(Ii == false && Ir == false)
    Interesting=false;

  // so that the order of v1 and v2 don't matter (function is
  // symmetric)
  return min(C1i,C1r);
}

// asymmetric version of the calculC1 function (handles only "half" of
// the cases), i.e., if calculcC1_asym(v1,v2) = inf, then
// calculC1_asym(v2,v1) will contain the answer of this (symmetric)
// function ... so the correct (symmetric) answer is always obtained
// by calling min(calculeC1_asym(v1,v2),calculeC1_asym(v2,v1))
float calculeC1_asym(Node * v1, Node * v2, vector<adjacence> * Adj_classe, bool & Interesting) {

  int iv1 = EvtToInt(E(v1));
  int iv2 = EvtToInt(E(v2));
  Interesting=true;

  // the cases where S(v1) = S(v2) need not hold
  if(iv1 == 6) { // Rec
    if(iv2 == 6 || iv2 == 8) { // with Rec or TB
      if(affich_CalculCout) cout << "v1 Rec et v2 Rec ou TB" << endl;
      return C1RecWithRecTB(v1,v2); }
  }
  else if(iv1 == 8 && iv2 == 8) { // TB with TB
    if(affich_CalculCout) cout << "v1 TB et v2 TB" << endl;
    return C1TBWithTB(v1,v2);
  }
  else if(iv1 == 9) { // TOut
    if(iv2 == 6 || iv2 == 8) { // with Rec or TB
      if(affich_CalculCout) cout << "v1 TOut et v2 Rec ou TB" << endl;
      return C1TOutWithRecTB(v1,v2); }
  }
  else if(iv1 == 10) { // Out
    if(iv2 == 6 || iv2 == 8) { // with Rec or TB
      if(affich_CalculCout) cout << "v1 Out et v2 Rec ou TB" << endl;
      return C1OutWithRecTB(v1,v2); }
  }

  // check if it is a case where S(v1) = S(v2) must hold
  if(Esp(v1) == Esp(v2)) {
    if(iv1 == 1 && iv2 == 1) { // Extant with Extant
      if(affich_CalculCout) cout << "v1 Extant et v2 Extant" << endl;
      return C1ExtantWithExtant(v1,v2,Adj_classe);
    }
    else if(iv1 == 5) { // GLos
      if(iv2 == 5) { // with GLos
	if(affich_CalculCout) cout << "v1 GLos et v2 GLos" << endl;
	return C1GLosWithGLos(v1,v2); }
      else { // with Any
	if(affich_CalculCout) cout << "v1 GLos et v2 Any" << endl;
	return C1GLosWithAny(v1,v2); }
    }
    else if(iv1 == 4) { // GDup
      if(BClass(iv2) || TClass(iv2)) { // with BTClass 
	if(affich_CalculCout) cout << "v1 GDup et v2 BTClass" << endl;
	return C1GDupWithBTClass(v1,v2); }
      else if(iv2 == 4) { // with GDup
	if(affich_CalculCout) cout << "v1 GDup et v2 GDup" << endl;
	return C1GDupWithGDup(v1,v2); }
    }
    else if(iv1 == 2 && iv2 == 2) { // Spec with Spec
      if(affich_CalculCout) cout << "v1 Spec et v2 Spec" << endl;
      return C1SpecWithSpec(v1,v2);
    }
    else if(BClass(iv1) && TClass(iv2)) { // BClass with TClass
      if(affich_CalculCout) cout << "v1 BClass et v2 TClass" << endl;
      return C1BClassWithTClass(v1,v2);
    }
    else if(iv1 == 9) { // TOut
      if(BClass(iv2) || iv2 == 4 || iv2 == 10) { // with BClass, GDup or Out
	if(affich_CalculCout) cout << "v1 TOut et v2 BCLass, GDup ou Out" << endl;
	return C1TOutWithBClassGDupOut(v1,v2); }
      else if(iv2 == 9) { // with TOut
	if(affich_CalculCout) cout << "v1 TOut et v2 TOut" << endl;
	return C1TOutWithTOut(v1,v2); }
    }
    else if(iv1 == 3 && iv2 == 3) { // Null with Null
      if(affich_CalculCout) cout << "v1 Null et v2 Null" << endl;
      return C1NullWithNull(v1,v2);
    }
    else if(iv1 == 6) { // Rec
      if(iv2 == 7) { // with Trans
	if(affich_CalculCout) cout << "v1 Rec et v2 Trans" << endl;
	return C1RecWithTrans(v1,v2); }
    }
    else if(iv1 == 7) { // Trans
      if(iv2 == 7 || iv2 == 8) { // with Trans or TB
	if(affich_CalculCout) cout << "v1 Trans et v2 Trans ou TB" << endl;
	return C1TransWithTransTB(v1,v2); }
      else if(iv2 == 9) { // with TOut
	if(affich_CalculCout) cout << "v1 Trans et v2 TOut" << endl;
	return C1TransWithTOut(v1,v2); }
    }  
    else if(iv1 == 10 && iv2 == 10) { // Out with Out
      if(affich_CalculCout) cout << "v1 Out et v2 Out" << endl;
      return C1OutWithOut(v1,v2);
    }
  }
  Interesting=false;
  return INFINI; // default
}

// C0
//
// On suppose que les noeuds sont bien de la m�me esp�ce (not any
// more, but rather we compute, based on this, inside the function)
float calculeC0(Node * v1, Node * v2, vector<adjacence> * Adj_classe, bool & Interesting) {

  bool Ii=true;
  bool Ir=true;
  float C0i = calculeC0_asym(v1,v2,Adj_classe,Ii);
  float C0r = calculeC0_asym(v2,v1,Adj_classe,Ir);
  
  Interesting=true;
  if(Ii == false && Ir == false)
    Interesting=false;

  // so that the order of v1 and v2 don't matter (function is
  // symmetric)
  return min(C0i,C0r);
}

// asymmetric version of the calculC0 function
float calculeC0_asym(Node * v1, Node * v2, vector<adjacence> * Adj_classe, bool & Interesting) {

  int iv1 = EvtToInt(E(v1));
  int iv2 = EvtToInt(E(v2));
  Interesting=true;

  // the cases where S(v1) = S(v2) need not hold
  if(iv1 == 6) { // Rec
    if(iv2 == 6 || iv2 == 8) { // with Rec or TB
      if(affich_CalculCout) cout << "v1 Rec et v2 Rec ou TB" << endl;
      return C0RecWithRecTB(v1,v2); }
  }
  else if(iv1 == 8 && iv2 == 8) { // TB with TB
    if(affich_CalculCout) cout << "v1 TB et v2 TB" << endl;
    return C0TBWithTB(v1,v2);
  }
  else if(iv1 == 9) { // TOut
    if(iv2 == 6 || iv2 == 8) { // with Rec or TB
      if(affich_CalculCout) cout << "v1 TOut et v2 Rec ou TB" << endl;
      return C0TOutWithRecTB(v1,v2); }
  }
  else if(iv1 == 10) { // Out
    if(iv2 == 6 || iv2 == 8) { // with Rec or TB
      if(affich_CalculCout) cout << "v1 Out et v2 Rec ou TB" << endl;
      return C0OutWithRecTB(v1,v2); }
  }
  
  // check if it is a case where S(v1) = S(v2) must hold
  if(Esp(v1) == Esp(v2))  {
    if(iv1 == 1 && iv2 == 1) { // Extant with Extant
      if(affich_CalculCout) cout << "v1 Extant et v2 Extant" << endl;
      return C0ExtantWithExtant(v1,v2,Adj_classe);
    }
    else if(iv1 == 5) { // GLos
      if(iv2 == 5) { // with GLos
	if(affich_CalculCout) cout << "v1 GLos et v2 GLos" << endl;
	return C0GLosWithGLos(v1,v2); }
      else { // with Any
	if(affich_CalculCout) cout << "v1 GLos et v2 Any" << endl;
	return C0GLosWithAny(v1,v2); }
    }
    else if(iv1 == 4) { // GDup
      if(BClass(iv2) || TClass(iv2)) { // with BTClass 
	if(affich_CalculCout) cout << "v1 GDup et v2 BTClass" << endl;
	return C0GDupWithBTClass(v1,v2); }
      else if(iv2 == 4) { // with GDup
	if(affich_CalculCout) cout << "v1 GDup et v2 GDup" << endl;
	return C0GDupWithGDup(v1,v2); }
    }
    else if(iv1 == 2 && iv2 == 2) { // Spec with Spec
      if(affich_CalculCout) cout << "v1 Spec et v2 Spec" << endl;
      return C0SpecWithSpec(v1,v2);
    }
    else if(BClass(iv1) && TClass(iv2)) { // BClass with TClass
      if(affich_CalculCout) cout << "v1 BClass et v2 TClass" << endl;
      return C0BClassWithTClass(v1,v2);
    }
    else if(iv1 == 9) { // TOut
      if(BClass(iv2) || iv2 == 4 || iv2 == 10) { // with BClass, GDup or Out
	if(affich_CalculCout) cout << "v1 TOut et v2 BCLass, GDup ou Out" << endl;
	return C0TOutWithBClassGDupOut(v1,v2); }
      else if(iv2 == 9) { // with TOut
	if(affich_CalculCout) cout << "v1 TOut et v2 TOut" << endl;
	return C0TOutWithTOut(v1,v2); }
    }
    else if(iv1 == 3 && iv2 == 3) { // Null with Null
      if(affich_CalculCout) cout << "v1 Null et v2 Null" << endl;
      return C0NullWithNull(v1,v2);
    }
    else if(iv1 == 6) { // Rec
      if(iv2 == 7) { // with Trans
	if(affich_CalculCout) cout << "v1 Rec et v2 Trans" << endl;
	return C0RecWithTrans(v1,v2); }
    }
    else if(iv1 == 7) { // Trans
      if(iv2 == 7 || iv2 == 8) { // with Trans or TB
	if(affich_CalculCout) cout << "v1 Trans et v2 Trans ou TB" << endl;
	return C0TransWithTransTB(v1,v2); }
      else if(iv2 == 9) { // with TOut
	if(affich_CalculCout) cout << "v1 Trans et v2 TOut" << endl;
	return C0TransWithTOut(v1,v2); }
    }
    else if(iv1 == 10 && iv2 == 10) { // Out with Out
      if(affich_CalculCout) cout << "v1 Out et v2 Out" << endl;
      return C0OutWithOut(v1,v2);
    }
  }
  Interesting=false;
  return INFINI; // default
}
