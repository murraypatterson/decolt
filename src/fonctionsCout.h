#ifndef _FCOUT_H_
#define _FCOUT_H_

#include <Phyl/TreeTemplate.h>
using namespace bpp;

// The STL library
#include <iostream>
#include <vector>
using namespace std;
#include "basicRoutines.h"
#include "Reconcil.h"

// VARIABLES GLOBALES 
// map de co�t en variables globales
extern map<pair<Node *,Node *>, float> C0;
extern map<pair<Node *,Node *>, float> C1;

// renvoie l'�v�nement associ� au noeud n
string E(Node * n);
int Espece(Node * n);
string Bootie(Node * n);
string TimeSl(Node * n);
int EvtToInt(string e);
bool BClass(int ev);
bool TClass(int ev);

// min � plusieurs �l�ments
float min(float x, float y);
float min(float x, float y, float z);
float min(float x, float y, float z, float a);
float min(float x, float y, float z, float a, float b);
float min(float x1, float x2, float x3, float x4, float x5, float x6, float x7, float x8, float x9, float x10, float x11, float x12, float x13, float x14, float x15, float x16);

int min_pos(float x, float y);
int min_pos(float x, float y, float z);
int min_pos(float x, float y, float z, float a);
int min_pos(float x, float y, float z, float a, float b);

bool appAdj(adjacence a, vector<adjacence> * Adjacences);
float recupCoutC1(Node * v1, Node * v2);
float recupCoutC0(Node * v1, Node * v2);

// cases for C1
float C1ExtantWithExtant(Node * v1, Node * v2, vector<adjacence> * Adj_classe);
float C1GLosWithAny(Node * v1, Node * v2);
float C1GLosWithGLosAny(Node * v1, Node * v2);
float D1(Node * v1, Node * v2);
float C1GDupWithBTClass(Node * v1, Node * v2);
float D2(Node * v1, Node * v2);
float D12(Node * v1, Node * v2);
float C1GDupWithGDup(Node * v1, Node * v2);
float C1SpecWithSpec(Node * v1, Node * v2);
float C1BClassWithTClass(Node * v1, Node * v2);
float C1TOutWithBClassGDupOut(Node * v1, Node * v2);
float C1NullWithNull(Node * v1, Node * v2);
float C1RecWithRecTB(Node * v1, Node * v2);
float C1RecWithTrans(Node * v1, Node * v2);
float C1TransWithTransTB(Node * v1, Node * v2);
float C1TBWithTB(Node * v1, Node * v2);
float C1TransWithTOut(Node * v1, Node * v2);
float O1(Node * v1, Node * v2);
float C1OutWithRecTB(Node * v1, Node * v2);
float C1TOutWithTOut(Node * v1, Node * v2);
float O2(Node * v1, Node * v2);
float O12(Node * v1, Node * v2);
float C1OutWithOut(Node * v1, Node * v2);
float C1TOutWithRecTB(Node * v1, Node * v2);

// cases for C0
float C0ExtantWithExtant(Node * v1, Node * v2, vector<adjacence> * Adj_classe);
float C0GLosWithAny(Node * v1, Node * v2);
float C0GLosWithGLosAny(Node * v1, Node * v2);
float D0(Node * v1, Node * v2);
float C0GDupWithBTClass(Node * v1, Node * v2);
float D00(Node * v1, Node * v2);
float C0GDupWithGDup(Node * v1, Node * v2);
float C0SpecWithSpec(Node * v1, Node * v2);
float C0BClassWithTClass(Node * v1, Node * v2);
float C0TOutWithBClassGDupOut(Node * v1, Node * v2);
float C0NullWithNull(Node * v1, Node * v2);
float C0RecWithRecTB(Node * v1, Node * v2);
float C0RecWithTrans(Node * v1, Node * v2);
float C0TransWithTransTB(Node * v1, Node * v2);
float C0TBWithTB(Node * v1, Node * v2);
float C0TransWithTOut(Node * v1, Node * v2);
float C0OutWithRecTB(Node * v1, Node * v2);
float C0TOutWithTOut(Node * v1, Node * v2);
float C0OutWithOut(Node * v1, Node * v2);
float C0TOutWithRecTB(Node * v1, Node * v2);

// functions that do the case analysis
float calculeC0(Node * v1, Node * v2, vector<adjacence> * Adj_classe, bool & Interesting);
float calculeC1(Node * v1, Node * v2, vector<adjacence> * Adj_classe, bool & Interesting);
float calculeC0_asym(Node * v1, Node * v2, vector<adjacence> * Adj_classe, bool & Interesting);
float calculeC1_asym(Node * v1, Node * v2, vector<adjacence> * Adj_classe, bool & Interesting);

#endif //_FCOUT_H_
