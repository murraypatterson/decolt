#ifndef _RECONCIL_H_
#define _RECONCIL_H_

#include <Phyl/Io/Newick.h>
#include <Phyl/Tree.h>
#include <Phyl/TreeTemplate.h>
#include <Phyl/TreeTemplateTools.h>
#include <Phyl/NodeTemplate.h>
#include <Phyl/Node.h>

using namespace bpp;

// The STL library:
#include <cstdlib>
#include <iostream>
//#include <stringstream>
#include <string>
#include <vector>

using namespace std;

#include "basicRoutines.h"

void nommerNoeudsInternes(Node * n);
void affecteInfoSurBrancheNoeud(Node * n);
void affecteInfoSurBrancheNoeuds(Node * n);
Node * preAffecteEspece(Node * n, TreeTemplate<Node> *G, TreeTemplate<Node> S);
void affecteEspece(Node * n, TreeTemplate<Node> S);
void renumeroteId(Node * n, int & id);
void annoteType(Node * n, TreeTemplate<Node> S);
Node * ajouteNoeudPerte(Node * x_S,Node * f_G,  int &id,TreeTemplate<Node> S);
void ajoutePerte(Node * n, int &id, TreeTemplate<Node> S);
void reconcil(TreeTemplate<Node >* S,TreeTemplate<Node >* G);

#endif //_RECONCIL_H_
