#Remplacer ou effacer tout ce qui suit un # y compris cette ligne
fic_arbre #adresse du fichier contenant les arbres de g�nes
fic_gene #adresse du fichier contenant la correspondance esp�ce g�ne 
fic_especes #adresse du fichier contenant l'arbre d'esp�ces
fic_adjacence #adresse du fichier contenant les adjacences
ReconcilDone #false si les arbres sont d�j� r�concili�s, true s'ils ne le sont pas (attention cependant, si pb laisser � false)
fic_SORTIE_GeneDone #false au premier appel, true aux autres (g�n�re le fichier de correspondance pour les g�nes, i.e pour chaque code de g�ne donne ses descendants)
fic_SORTIE_EspeceDone #false au premier appel, true aux autres (g�n�re le fichier de correspondance pour les esp�ces, i.e pour chaque code d'esp�ce donne ses descendants)
INPUT_FORMAT #0 si newick et 1 si nhx
OUTPUT_FORMAT #0 si newick et 1 si nhx
sep #caract�re s�paratieur, j'utilise | 
#co�ts des op�rations, ils ne sont pas forc�ment tous utilis�s dans le prog
Extant  #j'utilise ici 0
Spec #j'utilise ici 0
GDup #j'utilise ici 1
GLos #j'utilise ici 1
ADup #j'utilise ici 1
ALos #j'utilise ici 1
Crea #j'utilise ici 1
Break #j'utilise ici 1
