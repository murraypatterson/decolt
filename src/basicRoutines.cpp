/*
  module containing many basic routines that may be used by more than
  one of the modules in this package
*/

#include <Phyl/Io/Newick.h>
#include <Bpp/Phyl/Io/Nhx.h>
#include <Phyl/Tree.h>
#include <Phyl/TreeTemplate.h>
#include <Phyl/TreeTemplateTools.h>
#include <Phyl/Node.h>

using namespace bpp;

// The STL library:
#include <iostream>
//#include <fstream>
#include <string>
#include <map>
#include <vector>

using namespace std;

#include "basicRoutines.h"

// exit the program (for debugging)
void exeunt() {
  cout << "*****************************************" << endl << "EXIT ... STAGE LEFT" << endl; exit(0);
}

// fill v with with nodes of n in post-order fashion
void ordonnePostfixe(Node* n, vector<Node*>& v) {

  //cout<<"Je suis dans ordonnePostfixe"<<endl;
  for(int i=0; i< n->getNumberOfSons(); ++i)
    ordonnePostfixe(n->getSon(i), v);

  v.push_back(n);
}

// if a node is 'maximal' for its associated specie
bool maxForEsp(Node * v) {

  if(v->hasFather() == false) return true;
  if(memeEspece(v, v->getFather())) return false;
  return true;
}

// output a vector of ints
void afficheVector(vector<int> v) {

  vector<int>::iterator it;
  for(it=v.begin(); it!= v.end(); ++it) { cout << (*it) << "\t"; }
  cout << endl << endl;
}

// output a vector of strings
void afficheVector(vector<string> v) {

  vector<string>::iterator it;
  for(it=v.begin(); it!= v.end(); ++it) { cout << (*it) << "\t"; }
  cout << endl << endl;
}

// true if s is in v
bool findSinV(vector<string> v, string s) {

  vector<string>::iterator it;
  for(it=v.begin(); it!=v.end(); ++it)
    if((*it) == s) return true;
  return false;
}

// output a vector of adjacences
void afficheVector(vector<adjacence> v) {

  vector<adjacence>::iterator it;
  for(it=v.begin(); it!= v.end(); ++it) { cout << (*it).gene1 << " : " << (*it).gene2 << endl; }
  cout << endl << endl;
}

// output a vector of nodes
void afficheVector(vector<Node *> v) {

  vector<Node *>::iterator it;
  for(it=v.begin(); it!= v.end(); ++it) {cout << (*it)->getId() << "\t"; }
  cout << endl << endl;
}


//Pour afficher une map <string, vector<int>>
void afficheMap(map<string, vector<int> > m)
{
  map<string, vector<int> >::iterator it;
  for (it=m.begin();it!=m.end();it++)
    {
      cout<<(*it).first<< " :";
      vector<int> v=(*it).second;
      vector<int>::iterator it2;
      for (it2=v.begin();it2!=v.end();it2++)
	cout<<" "<<*it2;
      cout<<endl;
    }
}
//Pour afficher une map <int, vector<adjacence>>
void afficheMap(map<int, vector<adjacence> > m)
{
  map<int, vector<adjacence> >::iterator it;
  for (it=m.begin();it!=m.end();it++)
    {
      cout<<(*it).first<< " :";
      vector<adjacence> v=(*it).second;
      vector<adjacence>::iterator it2;
      for (it2=v.begin();it2!=v.end();it2++)
	cout<<" "<<(*it2).gene1<<sepAdj<<(*it2).gene2;
      cout<<endl;
    }
  cout<<endl;
}

void afficheMap(map<int, vector<string> > m)
{
  map<int, vector<string> >::iterator it;
  for (it=m.begin();it!=m.end();it++)
    {
      cout<<(*it).first<< " :";
      vector<string> v=(*it).second;
      vector<string>::iterator it2;
      for (it2=v.begin();it2!=v.end();it2++)
	cout<<" "<<(*it2);
      cout<<endl;
    }
  cout<<endl;
}

//Pour afficher une map <string, string>
void afficheMap(map<string, string> m)
{
  map<string, string>::iterator it;
  for (it=m.begin();it!=m.end();it++)
    {
      cout<<(*it).first<< " : ";
      cout<<(*it).second<<endl;
    }
}

//Pour afficher une map <string, int>
void afficheMap(map<string, int> m)
{
  map<string, int>::iterator it;
  for (it=m.begin();it!=m.end();it++)
    {
      cout<<(*it).first<< " : ";
      cout<<(*it).second<<endl;
    }
}

//Pour afficher une map <int, int>
void afficheMap(map<int, int> m)
{
  map<int, int>::iterator it;
  for (it=m.begin();it!=m.end();it++)
    {
      cout<<(*it).first<< " : ";
      cout<<(*it).second<<endl;
    }
}

// specie of a node
int Esp(Node * n) {

  convert<int> c;
  if(n->hasNodeProperty(esp)) {
    BppString * Espece = dynamic_cast<BppString *> (n->getNodeProperty(esp));
    return c.from_s(Espece->toSTL());
  }
  else {
    cout << "error : id " << n->getId() << " is not associated with a specie !!" << endl;
    return -1;
  }
}

/*
  method returns the node of A with species espece
*/
Node * retrieveNode(TreeTemplate<Node> * A, int espece) {

  Node * ret;
  vector<Node *> nodes = A->getNodes();
  vector<Node *>::iterator it;

  for(it=nodes.begin(); it!=nodes.end(); ++it) {
    ret = *it;
    if(ret->hasNodeProperty(esp))
      if(Esp(ret) == espece)
        return ret;
  }

  cout << "node not found !!!" << endl;
  return(NULL);
}

/*                                                                                                                                           
  method returns the Node of S on branch branchLabel at time slice
  timeSlice */
Node* retrieveNode(TreeTemplate<Node>* S, string branchLabel, int timeSlice) {

  bool verbose = false;

  if(verbose) { cout << "inside retrieveNode" << endl; }
  convert<int> C;
  Node* ret;
  vector<Node*> nodes = S->getNodes();
  vector<Node*>::iterator it;

  for(it=nodes.begin(); it!=nodes.end(); ++it) {
    ret = *it;
    if(ret->hasBranchProperty(brn) && ret->hasNodeProperty(tsl))
      if(clToStr(ret->getBranchProperty(brn)) == branchLabel && C.from_s(clToStr(ret->getNodeProperty(tsl))) == timeSlice)
        return ret;
  }

  if(verbose) {
    cout << "branch label : " << branchLabel << endl;
    cout << "time slice : " << timeSlice << endl;
  }

  cout << "node not found !!!" << endl;
  return(NULL);
}

//renvoie vrai si le noeud d'id1 est ancêtre du noeud di'd2 dans A
bool isAncestor(int id1, int id2, TreeTemplate<Node> *A)
{
  vector< int > Id_Genes;
  Id_Genes.push_back(id1);
  Id_Genes.push_back(id2);
  TreeTemplate<Node > A1=*A;
  int id_LCA=TreeTools::getLastCommonAncestor(A1,Id_Genes);
  return id_LCA==id1;
}

// overloaded version where we pass nodes instead
bool isAncestor(Node * n1, Node * n2, TreeTemplate<Node> * A) {

  vector< int > Id_Genes;
  Id_Genes.push_back(n1->getId());
  Id_Genes.push_back(n2->getId());
  TreeTemplate<Node > A1=*A;
  int id_LCA=TreeTools::getLastCommonAncestor(A1,Id_Genes);
  return id_LCA==n1->getId();
}

//Renvoie le numéro d'identifiant du noeud d'espèce s au dessus du
//noeud n dans G
int trouveNoeudSpeSAuDessusDeN(int s, int id_n,TreeTemplate<Node> * G)
{
  //cout<<"On entre dans trouveNoeudSpeSAuDessusDeN avec s="<<s<<", id_n="<<id_n;
  Node * n = G->getNode(id_n);
  convert<int> C;
  BppString * ESPECE = dynamic_cast <BppString *> (n->getNodeProperty(esp));
  string espece = ESPECE->toSTL();
  //cout<<", d'espèce "<<espece<<endl;
  while (n->hasFather()
          &&
	 (C.from_s(espece)!=s))
    {
      n=n->getFather();
      ESPECE = dynamic_cast <BppString *> (n->getNodeProperty(esp));
      espece = ESPECE->toSTL();
      //cout<<"Du WHILE de trouveNoeudSpeSAuDessusDeN avec id_n="<<n->getId()<<", d'espèce "<<espece<<endl;
    }
  return n->getId();
}

void retourneIdSpeSAuDessousDeN(vector<int> & Ids, int s, Node * n, TreeTemplate<Node> * G) {

  convert<int> C;
  BppString * ESPECE = dynamic_cast <BppString *> (n->getNodeProperty(esp));
  string espece = ESPECE->toSTL();
  if(C.from_s(espece)==s)
    Ids.push_back(n->getId());
  else
    for(int i=0; i< n->getNumberOfSons(); ++i)
      retourneIdSpeSAuDessousDeN(Ids,s,n->getSon(i),G);
}

/*
  1 if they are the same species, 0 if not

*/
int memeEspece(Node * n1, Node * n2) {
  BppString * esp1 = dynamic_cast <BppString *> (n1->getNodeProperty(esp));
  BppString * esp2 = dynamic_cast <BppString *> (n2->getNodeProperty(esp));
  string s1 = esp1->toSTL();
  string s2 = esp2->toSTL();
  if(s1==s2) return(1);
  else return(0);
}

/*
  return path nodes in tree T from a to b (we assume that a is
  ancestor of b)

*/
void getPathFromAtoB(vector<Node *> & nodes, Node * a, Node * b, TreeTemplate<Node> * T) {

  nodes.push_back(a);

  if(a != b)
    for(int i=0; i< a->getNumberOfSons(); ++i)
      if(isAncestor(a->getSon(i), b, T))
        getPathFromAtoB(nodes, a->getSon(i), b, T);
}

// two adjacencies are equal
bool equal(adjacence a, adjacence b) {

  return(a.gene1 == b.gene1 && a.gene2 == b.gene2 || a.gene1 == b.gene2 && a.gene2 == b.gene1);
}

// aux subroutine for subdiviseArbe (for outputting info about a node)
void outputNodeInfo(Node* n) {

  cout<<"id: "<<n->getId();

  cout<<"\tnom: ";
  if(n->hasName()) {
    cout<<n->getName()<<"   ";
  }
  else
    cout<<"NONAME";

  cout<<"\tespece: ";
  if(n->hasNodeProperty(esp)) {
    BppString* prop = dynamic_cast<BppString*>(n->getNodeProperty(esp));
    cout<<prop->toSTL();
  }
  else
    cout<<"NOSPECIE";

  cout<<"\ttype: ";
  if(n->hasBranchProperty(typ)) {
    BppString* type = dynamic_cast<BppString*>(n->getBranchProperty(typ));
    cout<<type->toSTL();
  }
  else
    cout<<"NOTYPE";

  cout<<"\tbs: ";
  if(n->hasBranchProperty(esp)) {
    BppString* assoc = dynamic_cast<BppString*>(n->getBranchProperty(esp));
    cout<<assoc->toSTL();
  }
  else
    cout<<"NO-BS";
  cout<<endl;
}

// renvoie l'événement associé au noeud n
string E(Node * n) {
  if (n->hasBranchProperty(typ)) {
    BppString * Evt = dynamic_cast<BppString*> (n->getBranchProperty(typ));
    return Evt->toSTL();
  }
  else {
    cout<<"Problème de E() : le noeud d'id "<<n->getId()<<" n'a pas d'événement associé !!"<<endl;
    return "ND";
  }
}

// renvoie la BSV associee au noeud n
string Bootie(Node * n) {
  if(n->hasBranchProperty(bsv))
    return clToStr(n->getBranchProperty(bsv));
  else
    return "NO BSV";
}

// renvoie la TSL associee au noeud n
string TimeSl(Node * n) {
  if(n->hasNodeProperty(tsl))
    return clToStr(n->getNodeProperty(tsl));
  else
    return "NO TSL";
}


// min à plusieurs éléments ...
float min(float x, float y) { return x > y ? y : x; }
float min(float x, float y, float z) { return x > y ? min(y, z) : min(x, z); }
float min(float x, float y, float z, float a) { return x > y ? min(y,z,a) : min(x,z,a); }
float min(float x, float y, float z, float a, float b) { return min(min(x,y),min(z,a,b)); }
float min(float x1, float x2, float x3, float x4, float x5, float x6, float x7, float x8, float x9, float x10, float x11, float x12, float x13, float x14, float x15, float x16) { return min(min(x1,x2,x3,x4),min(x5,x6,x7,x8),min(x9,x10,x11,x12),min(x13,x14,x15,x16)); }

int min_pos(float x, float y) { return x > y ? 2 : 1; }
int min_pos(float x, float y, float z) { return x < y ? (x < z ? 1 : min_pos(y, z)+1) : min_pos(y, z)+1; }
int min_pos(float x, float y, float z, float a) { return x < y ? (x < z ? 1 : min_pos(y,z,a)+1) : min_pos(y,z,a)+1; }
int min_pos(float x, float y, float z, float a, float b) { return min(x,y) < min(z,a,b) ? min_pos(x,y) : min_pos(z,a,b)+2; }

//Pour extraire le nom du gène du nom de la feuille de l'arbre
string nomGene(string nom)
{
  return nom.substr(0,nom.find(sep));
}

/*
  converts a Bpp Clonable* object into a string; for use with
  getNodeProperty, etc., for making things easier (and cleaner)
*/
string clToStr(Clonable* c) {

  BppString* property = dynamic_cast<BppString*> (c);
  return(property->toSTL());
}

/*
  returns property prop of Node n (when br is true it returns branch
  property, else it returns node property), or "?" if n does not have
  the property

*/
string getProperty(Node * n, string prop, bool br) {

  if(br == true) { if(n->hasBranchProperty(prop)) return(clToStr(n->getBranchProperty(prop))); }
  else if(n->hasNodeProperty(prop)) return(clToStr(n->getNodeProperty(prop)));

  return "?"; // default
}

//Peut être utilisé sur les deux formats nhx et newick ... à tester OUI
//+ ajout de noeud à un seul fils pour les arbres d'adjacences !!
void afficheInfoNoeud(Node * n) {

  convert<int> C;
  if (n->getNumberOfSons()==0)
    cout << n->getId();
  else if (n->getNumberOfSons()==1)
    cout << n->getId() << "(" << n->getSon(0)->getId() << ")";
  else // c'est qu'il en a deux !
    cout << n->getId() << "(" << n->getSon(0)->getId() << "," << n->getSon(1)->getId() << ")";

  cout << ", esp ";
  if (n->hasNodeProperty(esp))
    cout << C.from_s(clToStr(n->getNodeProperty(esp)));
  else
    cout << "?";

  cout << ", typ ";
  if (n->hasBranchProperty(typ))
    cout << clToStr(n->getBranchProperty(typ));
  else
    cout << "?";

  cout << ", tsl ";
  if (n->hasNodeProperty(tsl))
    cout << clToStr(n->getNodeProperty(tsl));
  else
    cout << "?";

  cout << ", brn ";
  if (n->hasBranchProperty(brn))
    cout << clToStr(n->getBranchProperty(brn));
  else
    cout<<"?";

  cout << ", bsv ";
  if(n->hasBranchProperty(bsv))
    cout << clToStr(n->getBranchProperty(bsv));
  else
    cout << "?";

  cout << ", nom ";
  if(n->hasName()) cout << n->getName();
  else cout << "?";

  cout << endl;
}

//Peut être utilisé sur les deux formats nhx et newick ... à tester
void afficheInfoNoeuds(Node * n) {

  for(int i=0; i< n->getNumberOfSons(); ++i)
    afficheInfoNoeuds(n->getSon(i));
  afficheInfoNoeud(n);
}
