#include <Phyl/Io/Newick.h>
#include <Bpp/Phyl/Io/Nhx.h>
#include <Phyl/Tree.h>
#include <Phyl/TreeTemplate.h>
#include <Phyl/TreeTemplateTools.h>
//#include <Phyl/NodeTemplate.h>
#include <Phyl/Node.h>

using namespace bpp;

// The STL library:
#include <iostream>
//#include <fstream>
#include <string>
#include <map>
#include <vector>

using namespace std;

#include "basicRoutines.h"
#include "Reconcil.h"
#include "fonctionsCout.h"
#include "parcoursArriere.h"

//Pour compter les feuilles en dessus du noeud n
int compteFeuilles(Node * n)
{
   if (n->getNumberOfSons()==0)
      return 1;
   else if (n->getNumberOfSons()==1)
      return compteFeuilles(n->getSon(0));
   else //2 fils 
      return compteFeuilles(n->getSon(0))+compteFeuilles(n->getSon(1));
}

void retourneFeuillesId(Node* n, vector<string>& feuilles) {

  convert<int> C;
  if(n->getNumberOfSons() == 0) { feuilles.push_back(C.to_s(n->getId())); }
  else if(n->getNumberOfSons() == 1) { retourneFeuillesId(n->getSon(0), feuilles); }
  else { // 2 fils
    retourneFeuillesId(n->getSon(0), feuilles);
    retourneFeuillesId(n->getSon(1), feuilles);
  }
}

void retourneFeuilles(Node * n, vector<string> & feuilles)
{
   if (n->getNumberOfSons()==0)
      feuilles.push_back(n->getName());
   else if (n->getNumberOfSons()==1)
      retourneFeuilles(n->getSon(0),feuilles);
   else //2 fils 
      {
	 retourneFeuilles(n->getSon(0),feuilles);
	 retourneFeuilles(n->getSon(1),feuilles);
      }
}
//M�me fonction qu'au dessus mais on tronque le nom de la feuille pour ne garder que celui du g�ne
void retourneFeuillesNomsGenes(Node * n, vector<string> & feuilles)
{
   if (n->getNumberOfSons()==0)
      {
	 if (nomGene(n->getName())!=NomPer)
	    feuilles.push_back(nomGene(n->getName()));
      }
   else if (n->getNumberOfSons()==1)
      retourneFeuillesNomsGenes(n->getSon(0),feuilles);
   else //2 fils 
      {
	 retourneFeuillesNomsGenes(n->getSon(0),feuilles);
	 retourneFeuillesNomsGenes(n->getSon(1),feuilles);
      }
}
//�crit l'info sur noeud dans le fichier des adjacences ancestrales sous la forme :
//numEspece G1=num1|id G2=num2|id Nb_adj_filles_feuilles suivant des adj_filles_feuilles
//ATTENTION : que si le noeud est un noeud de sp�ciation !!
void ecritAdjAncestrale(Node * n, int num1, int num2, TreeTemplate<Node> * A1, TreeTemplate<Node> * A2, ofstream & OfficAdj2)
{
   convert<int> c;
   //n� espece
   BppString * Espece = dynamic_cast<BppString*> (n->getNodeProperty(esp));
   OfficAdj2<<c.from_s(Espece->toSTL())<<"\t";
   
   //Les deux g�nes adjacents
   string nom=n->getName();
   string Id1=nom.substr(0,nom.find(sepAdj));
   string Id2=nom.substr(nom.find(sepAdj)+1,nom.size());
   OfficAdj2<<num1<<sep<<Id1<<"\t"<<num2<<sep<<Id2<<"\t";
   
   //Leurs descendants dans l'arbre solution
   int nb_adj_filles_feuilles=compteFeuilles(n);
   OfficAdj2<<nb_adj_filles_feuilles;
   vector<string> feuilles;
   retourneFeuilles(n,feuilles);
   for(int i=0;i<nb_adj_filles_feuilles;i++)
      OfficAdj2<<"\t"<<feuilles[i];
   OfficAdj2<<"\n";
}
//G�re la r�cursion de l'�criture des info des noeuds de l'arbre dans le fic
void ecritAdjAncestrales(Node * n, int num1, int num2, TreeTemplate<Node> * A1, TreeTemplate<Node> * A2, ofstream & OfficAdj2)
{
   //On ne fait rien pour les feuilles
   if (n->getNumberOfSons()!=0)
      {
	 //Est-ce un noeud de sp�ciation ?
	 BppString * TYP = dynamic_cast<BppString*> (n->getBranchProperty(typ));
	 if(TYP->toSTL()==spe)
	    if (n->getNumberOfSons()==1)
	       {
		  ecritAdjAncestrale(n,num1,num2,A1,A2,OfficAdj2);
		  ecritAdjAncestrales(n->getSon(0),num1,num2,A1,A2,OfficAdj2);
	       }
	    else //2 fils 
	       {
		  ecritAdjAncestrale(n,num1,num2,A1,A2,OfficAdj2);
		  ecritAdjAncestrales(n->getSon(0),num1,num2,A1,A2,OfficAdj2);
		  ecritAdjAncestrales(n->getSon(1),num1,num2,A1,A2,OfficAdj2);
	       }
      }
}

//�crit le fic SORTIE_adj (nouvelle version du fic des adjacences ancestrales) sous la forme :
//G1=num1|id G2=num2|id 
//ATTENTION : que si le noeud est un noeud de sp�ciation !!
void ecritSORTIEAdj(Node * n, int num1, int num2, ofstream & OfficAdj2) {

  //On ne fait rien pour les feuilles
  if (n->getNumberOfSons()!=0) {
    //Est-ce un noeud de sp�ciation ?
    BppString * TYP = dynamic_cast<BppString*> (n->getBranchProperty(typ));
    if(TYP->toSTL()==spe) {
      convert<int> c;
      //n� espece
      BppString * Espece = dynamic_cast<BppString*> (n->getNodeProperty(esp));
      OfficAdj2<<c.from_s(Espece->toSTL())<<"\t";
      //Les deux g�nes adjacents
      string nom=n->getName();
      string Id1=nom.substr(0,nom.find(sepAdj));
      string Id2=nom.substr(nom.find(sepAdj)+1,nom.size());
      OfficAdj2<<num1<<sep<<Id1<<"\t"<<num2<<sep<<Id2<<endl;
    }
  }
  
  for(int i=0; i< n->getNumberOfSons(); ++i)
    ecritSORTIEAdj(n->getSon(i), num1, num2, OfficAdj2);
}

void ecritSORTIEDup(vector<adjacence> DUPLI, int num1, TreeTemplate<Node>* Arbre1, int num2, ofstream & OfficSORTIE_dup)
{
   vector<adjacence>::iterator it_adj;
   for(it_adj=DUPLI.begin();it_adj!=DUPLI.end();it_adj++)
      {
	 //n� espece (les deux g�nes sont de la m�me esp�ce --> on travaille sur le 1er)
	 convert<int> c;
	 int Id1 = c.from_s((*it_adj).gene1); //c'est un num�ro d'identifiant
	 Node * n = Arbre1->getNode(Id1);
	 BppString * Espece = dynamic_cast<BppString*> (n->getNodeProperty(esp));
	 OfficSORTIE_dup<<c.from_s(Espece->toSTL())<<"\t";
	 OfficSORTIE_dup<<c.to_s(num1)+sep+(*it_adj).gene1<<"\t";
	 OfficSORTIE_dup<<c.to_s(num2)+sep+(*it_adj).gene2<<endl;
      }
}

void ecritSORTIETrf(vector<adjacence> TRFER, int n1, TreeTemplate<Node>* A1, int n2, ofstream & OfficSORTIE_trf) {

  vector<adjacence>::iterator it;
  convert<int> c;

  for(it=TRFER.begin(); it!=TRFER.end(); ++it) {
    int id1 = c.from_s((*it).gene1);
    Node * n = A1->getNode(id1);
    BppString * sp = dynamic_cast<BppString*> (n->getNodeProperty(esp));
    OfficSORTIE_trf<<c.from_s(sp->toSTL())<<"\t";
    OfficSORTIE_trf<<c.to_s(n1)+sep+(*it).gene1<<"\t";
    OfficSORTIE_trf<<c.to_s(n2)+sep+(*it).gene2<<endl;
  }
}

// if v is a reception or TB node
bool isRecTB(Node * v) {

  int iv = EvtToInt(E(v));
  if(iv == 6 || iv == 8) return true;
  return false;
}

// if v is a transfer or TB node
bool isTransTB(Node * v) {

  int iv = EvtToInt(E(v));
  if(iv == 7 || iv == 8) return true;
  return false;
}

/*
  return, into t, the first ancestor of v that is a transfer (or TB)
  node and return true.  If such an ancestor does not exist (for
  example, the root of the tree is an Outside node), then return false
*/
bool trAnc(Node * & t, Node * v) {

  if(v->hasFather() == false) {
    if(affich_CalculCout||affich_ParcoursArriere)
      cout << "no ancestral transfer node ..." << endl;
    return false;
  }

  //cout << "inside trAnc ..." << endl;
  //cout << "\tid v (ESP; BSV; TSL) = " << v->getId() << " (" << Esp(v) << "; " << Bootie(v) << "; " << TimeSl(v) << ")" << endl;

  Node * vf = v->getFather();
  if(isTransTB(vf)) { t = vf; return true; }
  return trAnc(t,vf);
}

/*
  return true if one of v1 and v2 is a reception (or TB) node, and a
  few other criteria are met, else return false
*/
bool oneIsReception(Node * v1, Node * v2, TreeTemplate<Node> * Arbre1, TreeTemplate<Node> * Arbre2) {

  // if one of v1 or v2 is a reception (or TB) node ...
  if(isRecTB(v1) || isRecTB(v2)) {
    // now we employ a system of checks for some other criteria ...

    // must be same species
    if(!memeEspece(v1,v2)) return false;

    // not if either is already the root node of its tree
    if(v1 == Arbre1->getRootNode()) return false;
    if(v2 == Arbre2->getRootNode()) return false;

    // not if they are both reception nodes of a co-transfer (becuase
    // this is already handled by the recursion)
    if(isRecTB(v1) && isRecTB(v2)) {
      Node * tv1, * tv2;
      if(trAnc(tv1,v1) && trAnc(tv2,v2))
	if(memeEspece(tv1,tv2)) return false;
    }
    
    // not if one of them is not 'maximal' for its associated species
    if(!maxForEsp(v1)) return false;
    if(!maxForEsp(v2)) return false;
    
    // made it through the gauntlet :
    return true;
  }

  return false; // default (neither is reception or TB)
}

/*
  DECO(S,Arbre1,Arbre2,Adj_classe,OfficAdj2,OfficArbresAdj,OfficSORTIE_dup,nom_classe);
*/

// Les deux arbres pass�s en param�tre n'ont pas forc�ment des num�ros
// d'identifiants "complets" (�a peut �tre des sous-arbres !)
void DECO(TreeTemplate<Node>* S, TreeTemplate<Node>* Arbre1, TreeTemplate<Node>* Arbre2, vector<adjacence> * Adj_classe, ofstream& OfficAdj2, ofstream & OfficArbresAdj, ofstream & OfficSORTIE_dup, ofstream & OfficSORTIE_trf, string nom_classe)
{
   if(affich_CalculCout||affich_ParcoursArriere)
      {
	 cout<<"\n\n*******************************\nOn entre dans la fonction DECO !!"<<endl;
	 cout<<"On traite les adjacences "<<endl;
	 vector<adjacence>::iterator it_adj;
	 for(it_adj=Adj_classe->begin();it_adj!=Adj_classe->end();it_adj++)
	    cout<<(*it_adj).gene1<<" - "<<(*it_adj).gene2<<endl;
	 cout<<endl;
      }

   vector<Node *> noeudsA1,noeudsA2;
   vector<Node *>::iterator itv1,itv2;
   ordonnePostfixe(Arbre1->getRootNode(),noeudsA1);
   //cout<<"ordonnePostfixe Arbre1 OK"<<endl;
   ordonnePostfixe(Arbre2->getRootNode(),noeudsA2);
   //cout<<"ordonnePostfixe Arbre2 OK"<<endl;

   if(affich_CalculCout||affich_ParcoursArriere)
     {
	 cout<<"Affichage des noeuds ordonn�s :"<<endl;
	 BppString * espece= dynamic_cast <BppString *> (Arbre1->getRootNode()->getNodeProperty(esp));
	 cout<<"\n\t Arbre 1 de racine d'id "<<Arbre1->getRootNode()->getId()<<", d'esp�ce "<<espece->toSTL()<<endl;
	 afficheInfoNoeuds(Arbre1->getRootNode());   
	 // for(itv1=noeudsA1.begin();itv1!=noeudsA1.end();itv1++)
	 //    cout<<(*itv1)->getId()<<" ";
	 // cout<<endl;
 
	 espece= dynamic_cast <BppString *> (Arbre2->getRootNode()->getNodeProperty(esp));
	 cout<<"\n\t Arbre 2 de racine d'id "<<Arbre2->getRootNode()->getId()<<", d'esp�ce "<<espece->toSTL()<<endl;
	 afficheInfoNoeuds(Arbre2->getRootNode());   
	 // for(itv2=noeudsA2.begin();itv2!=noeudsA2.end();itv2++)
	 //    cout<<(*itv2)->getId()<<" ";
	 // cout<<endl;

	 cout<<"\n\n********************* D�BUT CALCUL *********************"<<endl;
      }

   // Remise � z�ro des map de co�t qui sont des var. glob. pour ce nouveau calcul
   C0.clear();
   C1.clear();
   //cout<<"Taille de C0 : "<<C0.size()<<" et taille de C1 : "<<C1.size()<<endl;

   id_arbres_adj=0;
   vector<Tree *> * ArbresDAdjacences= new vector<Tree *>();
   //cout<<"Taille de ArbresDAdjacences avant les parcours arri�re : "<<ArbresDAdjacences->size()<<endl;
   DUP.clear();
   TRF.clear();

   /*
     what we do now is traverse (pairs) in a post-order fashion --
     since this type of order models (does not violate) the partial
     order given by each gene tree, we can guarantee that 

     (a) scores for all descendents of a pair p have been computed
     when we come to compute the score for p

     (b) in the same loop, we can do the recursion for each pair of
     the form (rec, any) : since, if within the recursion for a given
     tree, we need the info from a lower tree (that we tranfered to),
     this info will already be computed

   */
   //   bool onlyInterestingPairs=false;
   bool onlyInterestingPairs=true;
   bool C0Interesting;
   bool C1Interesting;
   for(itv1=noeudsA1.begin();itv1!=noeudsA1.end();itv1++)
     for(itv2=noeudsA2.begin();itv2!=noeudsA2.end();itv2++) {

       // (a)

       // note : check for S(v1) = S(v2) moved into calculcout
       pair<Node *,Node *> p((*itv1),(*itv2));
       pair<Node *,Node *> p2((*itv2),(*itv1));

       // function calculeC1 is symmetric (returns same value
       // regardless of order of first two arguments)
       C1[p]=calculeC1(*itv1,*itv2,Adj_classe,C1Interesting);
       C1[p2]=C1[p];

       if(affich_CalculCout)
	 if(C1Interesting) cout << endl;

       // function calculeC0 is symmetric
       C0[p]=calculeC0(*itv1,*itv2,Adj_classe,C0Interesting);
       C0[p2]=C0[p];

       if(affich_CalculCout)
	 if(C0Interesting || C1Interesting) {
	   cout << "\tid v1 (ESP; BSV; TSL) = " << (*itv1)->getId() << " (" << Esp(*itv1) << "; " << Bootie(*itv1) << "; " << TimeSl(*itv1) << "), v2 = " << (*itv2)->getId() << " (" << Esp(*itv2) << "; " << Bootie(*itv2) << "; " << TimeSl(*itv1) << ")" << endl;

	   cout << "C1 = " << C1[p] << endl;
	   cout << "C0 = " << C0[p] << endl;
	   if(C0[p]==C1[p] && C0[p] > 1000000.0)
	     cout << "both infinity" << endl;
	   cout << endl;
	 }

       // (b)

       // if one of v1 or v2 is a reception (or TB) node ...
       if(oneIsReception(*itv1,*itv2,Arbre1,Arbre2)) {

	 if(affich_ParcoursArriere) { cout << "matching an internal reception node to another node ..." << endl; }

	 float c1= recupCoutC1((*itv1),(*itv2));
	 float c1g = c1 + Crea;
	 // NOTE : because we do it this way, the gain does not enter the backtracking, but
	 // is only used to choose how we start backtracking (i.e., C0 vs. C1)
	 float c0= recupCoutC0((*itv1),(*itv2));

	 if(affich_CalculCout||affich_ParcoursArriere) {
	   cout<<"\n**********************"<<endl;
	   cout << "\tid v1 (ESP; BSV; TSL) = " << (*itv1)->getId() << " (" << Esp(*itv1) << "; " << Bootie(*itv1) << "; " << TimeSl(*itv1) << ")," << endl;
	   cout << "\tid v2 (ESP; BSV; TSL) = " << (*itv2)->getId() << " (" << Esp(*itv2) << "; " << Bootie(*itv2) << "; " << TimeSl(*itv1) << ")" << endl;
	   cout<<"C1 aux (v1, v2) : "<<c1<<endl;
	   cout<<"C1 aux (v1, v2) + gain : "<<c1g<<endl;
	   cout<<"C0 aux (v1, v2) : "<<c0<<endl;
	   cout<<"\t Diff�rence C0 - (C1 + gain) = "<<c0-c1g<<endl;
	   if(c0-c1g>2)
	     cout<<"\t\t\t Diff�rence STRICTEMENT sup�rieure � 2 !!!"<<endl;
	   cout<<"\n\n\n\n\n\n"<<endl;
	 }
	 if(affich_ParcoursArriere) {
	   cout<<"\n**********************"<<endl;
	   cout<<"Parcours arri�re :"<<endl;
	 }

	 // Pour avoir un nombre al�atoire :
	 float r=rand()/(double)RAND_MAX;
	 bool avantageC1;
	 if (r<=Adj_percentage) // en cas C0=C1 on choisit C1
	   avantageC1=true;
	 else
	   avantageC1=false;

	 // start off an adjacency tree at (v1,v2)
	 if(affich_ParcoursArriere)
	   cout << "c1 : " << c1 << endl << "c1 + gain : " << c1g << endl << "c0 : " << c0 << endl;
	 if (c0>c1g || (c1g==c0 && avantageC1)) {
	   if (c1g==c0) cout << "c1 + gain == c0, je choisis C1" << endl;
	   // Cr�ation du (sous-) arbre d'adjacence
	   nb_Crea++; // since v1 and v2 are surely not root nodes
	   TreeTemplate<Node> * T = new TreeTemplate<Node>();
	   T->setRootNode(parcoursArriereC1(*itv1,*itv2,Adj_classe,ArbresDAdjacences,true));
	   ArbresDAdjacences->push_back(T);
	 }
	 else {
	   if (c1g==c0) cout << "c1 + gain == c0, je choisis C0" << endl;
	   parcoursArriereC0(*itv1,*itv2,Adj_classe,ArbresDAdjacences,true);
	 }
	 if(affich_ParcoursArriere) {
	   cout<<"\n... End Parcours arri�re"<<endl;
	   cout<<"**********************\n\n"<<endl;
	 }
       }
     }

   // now we build the adjacency tree for the roots of a1 and a2

   pair<Node *,Node *> p(Arbre1->getRootNode(),Arbre2->getRootNode());
   pair<Node *,Node *> p2(Arbre2->getRootNode(),Arbre1->getRootNode());

   float c1= recupCoutC1(Arbre1->getRootNode(),Arbre2->getRootNode());
   float c1g = c1 + Crea; // after adding the gain we pay for being the root of an adj tree
   float c0= recupCoutC0(Arbre1->getRootNode(),Arbre2->getRootNode());

   if(affich_CalculCout||affich_ParcoursArriere)
      {
	 cout<<"\n**********************"<<endl;
	 cout<<"C1 aux racines : "<<c1<<endl;
	 cout<<"C1 aux racines + gain : "<<c1g<<endl;
	 cout<<"C0 aux racines : "<<c0<<endl;
	 cout<<"\t Diff�rence C0 - (C1 + gain) = "<<c0-c1g<<endl;
	 if (c0-c1g>2)
	    cout<<"\t\t\t Diff�rence STRICTEMENT sup�rieure � 2 !!!"<<endl;
	 cout<<"\n\n\n\n\n\n"<<endl;
      }
   if(affich_ParcoursArriere)
     {
       cout<<"\n**********************"<<endl;
       cout<<"Parcours arri�re for ROOTS :"<<endl;
     }

   // Pour avoir un nombre al�atoire :
   float r=rand()/(double)RAND_MAX;
   bool avantageC1;
   if (r<=Adj_percentage) // en cas C0=C1 on choisit C1
      avantageC1=true;
   else
      avantageC1=false;

   if(affich_ParcoursArriere)
     cout << "c1 : " << c1 << endl << "c1 + gain : " << c1g << endl << "c0 : " << c0 << endl;
   if (c0>c1g || (c1g==c0 && avantageC1)) {
     if (c1g==c0) cout << "c1 + gain == c0, je choisis C1" << endl;
     // Cr�ation du premier arbre d'adjacence
     TreeTemplate<Node> * T = new TreeTemplate<Node>();
     T->setRootNode(parcoursArriereC1(Arbre1->getRootNode(),Arbre2->getRootNode(),Adj_classe,ArbresDAdjacences,true));
     ArbresDAdjacences->push_back(T);
   }
   else {
     if (c1g==c0) cout << "c1 + gain == c0, je choisis C0" << endl;
     parcoursArriereC0(Arbre1->getRootNode(),Arbre2->getRootNode(),Adj_classe,ArbresDAdjacences,true);
   }

   /*****************************************************************************/

   cout<<"\n\nTaille de ArbresDAdjacences apr�s les parcours arri�re : "<<ArbresDAdjacences->size()<<endl;

   // STAT sur le nb d'arbres d'adjacences solution par classe
   int nb_arbres_adj=ArbresDAdjacences->size();
   nb_arbres_adj_tot+=nb_arbres_adj;
   if(nb_arbres_adj_min>nb_arbres_adj)
      nb_arbres_adj_min=nb_arbres_adj;
   if(nb_arbres_adj_max<nb_arbres_adj)
      nb_arbres_adj_max=nb_arbres_adj;
   
   if(affich_CalculCout||affich_ParcoursArriere)
      cout<<"\n�criture des arbres d'adjacences et des adjacences ancestrales de cette classe dans les fichiers correspondants."<<endl;
   OfficArbresAdj<<"Arbre(s) d'adjacences solution pour la classe "<<nom_classe<<endl;
   vector<Tree *>::iterator it;
   int i=1;
   convert<int> c;
   int num1; int num2;
   num1=c.from_s(nom_classe.substr(0,nom_classe.find(sep)));
   if (nom_classe.find(sep)==nom_classe.rfind(sep))
     num2=c.from_s(nom_classe.substr(nom_classe.find(sep)+1,nom_classe.size()-1));
   else
     //cas o� on a une classe du type 12|12|57
     num2=c.from_s(nom_classe.substr(nom_classe.find(sep)+1,nom_classe.rfind(sep)-1));

   //On range les g�nes dupliqu�s ensemble dans le fichier SORTIE_dup
   ecritSORTIEDup(DUP,num1,Arbre1,num2,OfficSORTIE_dup);

   //on range les genes transferes ensemble dans le fichier SORTIE_trf
   ecritSORTIETrf(TRF,num1,Arbre1,num2,OfficSORTIE_trf);

   for(it=ArbresDAdjacences->begin();it!=ArbresDAdjacences->end();it++)
      {
	 Tree * Gp=*it;
	 TreeTemplate<Node> *G = dynamic_cast <TreeTemplate<Node> *> (Gp);		            
	 if(affich_ParcoursArriere)
	    {
	       //Affichage �cran
	       cout<<"\n****\nArbre "<<i<<" :"<<endl;
	       afficheInfoNoeuds(G->getRootNode());
	       cout<<endl<<endl;
	    }
	 //�criture dans le fichier des adjacences ancestrales
	 OfficArbresAdj<<"\nArbre "<<i<<" :"<<endl;
	 if(OUTPUT_FORMAT==0) {
	   Newick * newickWriter = new Newick(); 
	   newickWriter->enableExtendedBootstrapProperty(esp);
	   affecteInfoSurBrancheNoeuds(G->getRootNode());
	   if (G->getNumberOfLeaves()!=0)
	     newickWriter->write(*G,OfficArbresAdj);
	   else
	     OfficArbresAdj<<"no tree"<<endl;
	     //OfficArbresAdj<<G->getRootNode()->getName()<<endl;
	 }
	 else if(OUTPUT_FORMAT==1) {
	   Nhx * nhxWriter = new Nhx(true);
	   nhxWriter->registerProperty(Nhx::Property("Duplication", "D", true, 0)); 
	   affecteInfoSurBrancheNoeuds(G->getRootNode());
	   if (G->getNumberOfLeaves()!=0)
	     nhxWriter->write(*G,OfficArbresAdj);
	   else
	     OfficArbresAdj<<"no tree"<<endl;
	     //OfficArbresAdj<<G->getRootNode()->getName()<<endl;
	 }
	 else
	   cout<<"Erreur : type de OUTPUT_FORMAT non reconnu"<<endl;
	 
	 i++;
	 //Calcul et �criture dans le fichier des adjacences ancestrales
	 //ecritAdjAncestrales(G->getRootNode(),num1,num2,Arbre1,Arbre2,OfficAdj2);
	 ecritSORTIEAdj(G->getRootNode(),num1,num2,OfficAdj2);
      }
   if(affich_ParcoursArriere)
      cout<<"\n\n\n"<<endl;
   OfficArbresAdj<<"\n***************************************************************************"<<endl;
}
