#include <Phyl/Io/Newick.h>
#include <Phyl/Tree.h>
#include <Phyl/TreeTemplate.h>
#include <Phyl/TreeTemplateTools.h>
#include <Phyl/NodeTemplate.h>
#include <Phyl/Node.h>

using namespace bpp;

// The STL library:
#include <cstdlib>
#include <iostream>
//#include <stringstream>
#include <string>
#include <vector>

using namespace std;

#include "basicRoutines.h"
#include "Reconcil.h"

// À FAIRE
// Passer des pointeurs aux fonctions et non l'arbre des espèces entiers
// Élaguer en plus des espèces non présentes dans S, celles qui n'en sont pas des feuilles --> FAIT
// Voir si on peut éviter les NodeNotFound execption qui ralentissent le prog --> Ne pas fait afficher l'exeption

//Les pré-requis

// 1) G et S doivent être deux arbres binaires

// 2) Toutes les feuilles de S doivent avoir un nom (considéré comme nom d'espèce) ne contenant pas le séparateur utilisé dans G (par défaut _ stocké dans la var sep). Si les noeud internes sont nommés ce nom est récupéré pour la visualisation

// 3) Toutes les feuilles de G doivent avoir un nom composé du nom de gène quelconque suivi d'un séparateur (par défaut _ stocké dans la var sep) suivi d'un nom d'espèce ne contenant pas ce séparateur mais n'appartenant pas forcément à S

//Rappatrie les noms des espèces aux noeuds + mets les distance à 1
//Utilisé sur l'arbre des espèces uniquement
void nommerNoeudsInternes(Node * n)
{
   convert<int> C;
   n->setNodeProperty(esp, BppString(C.to_s(n->getId())));
   //cout<<"J'entre dans nommerNoeudsInternes, id du noeud traité : "<<n->getId();
   if (n->isLeaf())
      {
	 //cout<<" c'est une feuille"<<endl;
	 n->setDistanceToFather(1);
      }
   else
      {
	 if (n->hasFather())
	   n->setDistanceToFather(1);
	 //cout<<"c'est un noeud interne"<<endl;
	 if (n->hasBranchProperty(bsv))
	    {
	       BppString * BootStrap = dynamic_cast<BppString*> (n->getBranchProperty(bsv));
	       n->setName(BootStrap->toSTL());
	       //n->deleteBranchProperties();
	    }
	 //else
	 //cout<<"De nommerNoeudsInternes : le noeud n'a pas de nom !!!"<<endl;

	 // l'arbre des espèces n'est pas binaire en général
	 for(int i=0;i< n->getNumberOfSons();++i)
	   nommerNoeudsInternes(n->getSon(i));
      }
}

//À n'utiliser que si arbre au format Newick a priori
void affecteInfoSurBrancheNoeud(Node * n)
{
   //cout<<"affecteInfoSurBrancheNoeud Id="<<n->getId()<<" a un nom ? "<<n->hasName()<<endl;

   convert<int> C;
   string prop;
   
   prop+="Id";
   prop+=C.to_s(n->getId());
   prop+=sep;

   if (n->hasName())
      prop+=n->getName();
   else
      prop+="NONAME";
   prop+=sep;
   
   prop+="E";
   if (n->hasNodeProperty(esp))
      {
	 BppString * espece = dynamic_cast<BppString*> (n->getNodeProperty(esp));
	 prop+=espece->toSTL();
      }
   else
      prop+="NOSPECIE";
   prop+=sep;
   
   if (n->hasBranchProperty(typ))
      {
	 BppString * type = dynamic_cast<BppString*> (n->getBranchProperty(typ));
	 prop+=type->toSTL();
      }
   else
      prop+="NOTYPE";
   prop+=sep;
   
   prop+="D";
   if (n->hasDistanceToFather())
      prop+=C.to_s(n->getDistanceToFather());
   else
      prop+="NODIST";
   //prop+=sep;
   n->setBranchProperty(esp,BppString(prop));
}
//À n'utiliser que si arbre au format Newick a priori
//cas 1 fils pour les arbres d'adjacences
void affecteInfoSurBrancheNoeuds(Node * n)
{
  for(int i=0; i< n->getNumberOfSons(); ++i)
    affecteInfoSurBrancheNoeuds((n->getSons())[i]);
  affecteInfoSurBrancheNoeud(n);
}

//VÉRIFIER TOUS LES GETNODE AU CAS OÙ UNE ESPÈCE DE L'ARBRE DE GÈNE NE SOIT PAS DANS L'ARBRES DES ESPÈCES --> ON ÉLAGUE L'ARBRE DES GÈNES 
Node * preAffecteEspece(Node * n, TreeTemplate<Node> *G, TreeTemplate<Node> *S)
{
   //n est un noeud de G
   convert<int> C;
   //cout<<"J'entre dans preAffecteEspece, id du noeud traité : "<<n->getId()<<endl;
   if (n->isLeaf())
      {
	 //cout<<"\t C'est une feuille de nom "<<n->getName()<<endl;
	 string nomGene = n->getName();
	 string nomEsp = nomGene.substr(nomGene.rfind(sep,nomGene.length()-1)+1, nomGene.length()-1);
	 string Id = ""; 
	 bool feuille=false;
	 try
	    {
	       Id = C.to_s( S->getNode(nomEsp)->getId());
	       if(S->getNode(nomEsp)->isLeaf())
		  {
		     feuille=true;
		     n->setNodeProperty(esp, BppString("CONNUE"));
		     //cout<<"\t Je la nomme CONNUE"<<endl;
		  }
	    }
	 catch (Exception e) 
	    {
	       //perror(e.what()); 
	    }
	 if (Id=="" || !feuille)
	    {
	       n->setNodeProperty(esp, BppString("INCONNUE"));
	       //cout<<"\t Je la nomme INCONNUE"<<endl;
	    }
	 return n;
      }
   else
      {
	 Node * fg = n->getSon(0);
	 Node * fd = n->getSon(1);
	 //cout<<"\t c'est un noeud interne de fils "<<fg->getId()<<" et "<<fd->getId()<<endl;
	 
	 //cout<<"Je vais pré-affecter fg d'id"<<fg->getId()<<endl;
	 fg=preAffecteEspece(fg,G,S);
	 //cout<<"J'ai pré-affecté fg d'id"<<fg->getId()<<endl;
	 //cout<<"Je vais pré-affecter fd d'id"<<fd->getId()<<endl;
	 fd=preAffecteEspece(fd,G,S); 
	 //cout<<"J'ai pré-affecté fd d'id"<<fd->getId()<<endl;
	 //cout<<"n est d'id "<<n->getId()<<endl;
	 
	 //Après ces pre affectation n peut avoir perdu ses fils !!! (forcément les deux)
	 if (!n->isLeaf()) 
	    {
	       BppString * EspeceFG = dynamic_cast<BppString*> (fg->getNodeProperty(esp));
	       BppString * EspeceFD = dynamic_cast<BppString*> (fd->getNodeProperty(esp));
	       
	       if((EspeceFG->toSTL()=="INCONNUE") &&     
		  (EspeceFD->toSTL()=="INCONNUE"))
		  {
		     //cout<<"Les deux fils de "<<n->getId()<<" sont d'espèces INCONNUES"<<endl;
		     n->setNodeProperty(esp, BppString("INCONNUE"));
		     n->removeSons();
		  }
	       else if (EspeceFG->toSTL()=="INCONNUE")
		  {
		     //cout<<"Le fils gauche de "<<n->getId()<<" est d'espèce INCONNUE"<<endl;
		     if (n->hasFather())
			{
			   if(n->getFather()->getSon(0)->getId()==n->getId())
			      n->getFather()->setSon(0,fd);
			   else
			      n->getFather()->setSon(1,fd);
			   n=fd;
			}   
		     else
			G->setRootNode(fd);
		     //cout<<"Le fils gauche de "<<n->getId()<<" est d'espèce INCONNUE ----> TRAITÉ"<<endl;
		  }
	       else if (EspeceFD->toSTL()=="INCONNUE")
		  {
		     //cout<<"Le fils droit de "<<n->getId()<<" est d'espèce INCONNUE"<<endl;
		     if (n->hasFather())
			{
			   if(n->getFather()->getSon(0)->getId()==n->getId())
			      n->getFather()->setSon(0,fg);
			   else
			      n->getFather()->setSon(1,fg);
			   n=fg;
			}
		     else
			G->setRootNode(fg);
		     //cout<<"Le fils droit de "<<n->getId()<<" est d'espèce INCONNUE ----> TRAITÉ"<<endl;
		  }
	       else
		  {
		     //cout<<"Les deux fils de "<<n->getId()<<" sont d'espèces CONNUES"<<endl;
		     n->setNodeProperty(esp, BppString("CONNUE"));
		  }
	    }
      }
   return n;
   //cout<<"Je sors de preAffecteEspece avec n a pour Id"<<n->getId()<<endl;
}

//Fonctionne pour des arbres binaires dont les feuilles ont été étiquetées
//On affecte les Id des noeuds de l'arbre des espèces (au cas où on ne connaisse pas les noms)
void affecteEspece(Node * n, TreeTemplate<Node> *S)
{
   //n est un noeud de G
   convert<int> C;
   //cout<<"J'entre dans affecteEspece, id du noeud traité : "<<n->getId();
   if (n->isLeaf())
      {
	 //cout<<" c'est une feuille de nom "<<n->getName();
	 string nomGene = n->getName();
	 string nomEsp = nomGene.substr(nomGene.rfind(sep,nomGene.length()-1)+1, nomGene.length()-1);
	 string Id = C.to_s( S->getNode(nomEsp)->getId());
	 BppString * prop = new BppString(Id);
	 n->setNodeProperty(esp, *prop);
      }
   else
      {
	 Node * fg = (n->getSons())[0];
	 Node * fd = (n->getSons())[1];
	 //cout<<" c'est un noeud interne de fils "<<fg->getId()<<" et "<<fd->getId();
	 
	 affecteEspece(fg,S); 
	 affecteEspece(fd,S);  
	 BppString * EspeceFG = dynamic_cast<BppString*> (fg->getNodeProperty(esp));
	 BppString * EspeceFD = dynamic_cast<BppString*> (fd->getNodeProperty(esp));
	 //cout<<"Les espèces des fils sont "<<EspeceFG->toSTL()<<" et "<<EspeceFD->toSTL();
	 vector< int > IdFils;
	 // IdFils.push_back(S->getNode(C.from_s(EspeceFG->toSTL()))->getId());
	 // IdFils.push_back(S->getNode(C.from_s(EspeceFD->toSTL()))->getId());
	 //remplacé par :
	 IdFils.push_back(C.from_s(EspeceFG->toSTL()));
	 IdFils.push_back(C.from_s(EspeceFD->toSTL()));
	 //cout<<"J'ai ajouté les Id dans IdFils"<<endl;
	 //TreeTools * TT = new TreeTools;
	 int IdLCA = TreeTools::getLastCommonAncestor(*S, IdFils);
	 //cout<<"Le LCA dans S a pour Id "<<IdLCA;
	 n->setNodeProperty(esp, BppString(C.to_s(IdLCA)));
	 //Pour permettre de l'afficher comme valeur de bootstap si 
	 //nommerNoeudsInternes à été utilisée
	 //n->setBranchProperty(esp, BppString(S->getNodeName(IdLCA)));
	 
      }
   //cout<<" Et je sors"<<endl;
}

//apparemment il existe une fonction bio++ qui fait ça tout seul
void renumeroteId(Node * n, int & id)
{
   if (n->isLeaf())
      {
	 n->setId(id);
	 id++;
      }
   else
      {
	 renumeroteId(n->getSon(0),id);
	 renumeroteId(n->getSon(1),id);
	 n->setId(id);
	 id++;
      }
}

//devrait fonctionner pour les deux formats
void annoteType(Node * n, TreeTemplate<Node> *S)
{
   convert<int> c;
    if (n->isLeaf())
      {
	 //cout<<" c'est une feuille de nom "<<n->getName();
	 n->setBranchProperty(typ, BppString(ga));
	 n->setBranchProperty("D", BppString("?"));
      }
    else if (n->getNumberOfSons()!=2)
       {
	  cout<<"de affecteEspece : arbre de gène NON BINAIRE !!!"<<endl;
       }
    else
       {
	  Node * fg = (n->getSons())[0];
	  Node * fd = (n->getSons())[1];
	  //cout<<" c'est un noeud interne de fils "<<fg->getId()<<" et "<<fd->getId();
	  annoteType(fg,S); 
	  annoteType(fd,S);  
	  BppString * EspeceN = dynamic_cast<BppString*> (n->getNodeProperty(esp));
	  BppString * EspeceFG = dynamic_cast<BppString*> (fg->getNodeProperty(esp));
	  BppString * EspeceFD = dynamic_cast<BppString*> (fd->getNodeProperty(esp));
	 
	  //cout<<"L'espère du père est "<< c.from_s(EspeceN->toSTL())<<", celles des fils sont "<< c.from_s(EspeceFG->toSTL())<<" et "<< c.from_s(EspeceFD->toSTL())<<endl;
	  if (c.from_s(EspeceN->toSTL()) != c.from_s(EspeceFD->toSTL())
	      &&
	      c.from_s(EspeceN->toSTL()) != c.from_s(EspeceFG->toSTL()))
	     {
		n->setBranchProperty(typ, BppString(spe));
		n->setBranchProperty("D", BppString("F"));
	     }
	  else
	     {
		n->setBranchProperty(typ, BppString(dupl));
		n->setBranchProperty("D", BppString("T"));
	     }
      }
}

//renvoie un noeud dont les fils sont f_G et p_G (une perte) 
//d'espèces correspondant aux fils du noeud x_S
Node * ajouteNoeudPerte(Node * x_S,Node * f_G,  int &id,TreeTemplate<Node> *S)
{
   //cout<<"J'entre dans ajouteNoeudPerte avec comme id : "<<id<<endl;
   convert<int> C;
   Node * p_G = new Node(); //futur noeud perte à ajouter
   p_G->setDistanceToFather(1);
   p_G->setId(id);id++;//cout<<"On ajoute un noeud perte d'id "<<p_G->getId()<<endl;
   p_G->setBranchProperty(typ,BppString(per));
   p_G->setBranchProperty("D", BppString("?"));
   
   BppString * EspeceFils1 = dynamic_cast<BppString*> (x_S->getSon(1)->getNodeProperty(esp));
   BppString * EspeceF_G = dynamic_cast <BppString*> (f_G->getNodeProperty(esp));
   BppString * EspeceFils0 = dynamic_cast<BppString*> (x_S->getSon(0)->getNodeProperty(esp));
   if (EspeceFils0->toSTL()==EspeceF_G->toSTL())
      {
	 p_G->setNodeProperty(esp,*EspeceFils1); 
	 string NomPerte = NomPer;
	 NomPerte+=sep;
	 if (S->getNode(C.from_s(EspeceFils1->toSTL()))->hasName())
	    NomPerte+=S->getNode(C.from_s(EspeceFils1->toSTL()))->getName();
	 else
	    NomPerte+=C.to_s(S->getNode(C.from_s(EspeceFils1->toSTL()))->getId());
	 p_G->setName(NomPerte);
      }
   else
      {
	 p_G->setNodeProperty(esp,*EspeceFils0);	
	 
	 string NomPerte = NomPer;
	 NomPerte+=sep;
	 if (S->getNode(C.from_s(EspeceFils0->toSTL()))->hasName())
	    NomPerte+=S->getNode(C.from_s(EspeceFils0->toSTL()))->getName();
	 else
	    NomPerte+=C.to_s(S->getNode(C.from_s(EspeceFils0->toSTL()))->getId());
	 p_G->setName(NomPerte);
      }	  
   
   Node * t_G = new Node(); //futur noeud père de f_G et de la perte
   t_G->setDistanceToFather(1);

   t_G->setId(id);id++;//cout<<"On ajoute un noeud père d'id "<<t_G->getId()<<endl;
   //j'ajouterai l'id de T_G après avoir raccordé le tout sinon G->getNextId() me donne le même res que précédemment
   t_G->setBranchProperty(typ,BppString(spe));
   t_G->setBranchProperty("D", BppString("F"));
   t_G->setNodeProperty(esp,*(x_S->getNodeProperty(esp))); 
   
   t_G->addSon(f_G);
   t_G->addSon(p_G);
   
   return t_G;
}

void ajoutePerte(Node * n, int &id, TreeTemplate<Node> *S)
{
   //cout<<"J'entre dans ajoutePerte avec comme id : "<<id<<endl;
   convert<int> C;
   if (n->isLeaf())
      {
	 //On ne fait rien
	 //cout<<" c'est une feuille de nom "<<n->getName();
      }
   else if (n->getNumberOfSons()!=2)
      {
	 cout<<"de affecteEspece : arbre de gène NON BINAIRE !!!"<<endl;
      }
   else
      {
	 Node * fg = n->getSon(0);
	 Node * fd = n->getSon(1);
	 //cout<<" c'est un noeud interne de fils "<<fg->getId()<<" et "<<fd->getId();
	 ajoutePerte(fg,id,S);//Normalement id est modifiée à l'issue de cet appel
	 //cout<<"Après appel sur fg : id = "<<id<<endl;
	 ajoutePerte(fd,id,S);
	 //cout<<"Après appel sur fd : id = "<<id<<endl;
	 
	 BppString * EspeceN = dynamic_cast<BppString*> (n->getNodeProperty(esp));
	 Node * NoeudEspN = S->getNode(C.from_s(EspeceN->toSTL()));
	 BppString * typeN = dynamic_cast<BppString*> (n->getBranchProperty(typ));
	 
	 //Il faut traiter 2 arêtes : N-FG et N-FD  
	 
	 BppString * EspeceFG = dynamic_cast<BppString*> (fg->getNodeProperty(esp));
	 Node * NoeudEspFG = S->getNode(C.from_s(EspeceFG->toSTL()));
	 BppString * EspeceFD = dynamic_cast<BppString*> (fd->getNodeProperty(esp));
	 Node * NoeudEspFD = S->getNode(C.from_s(EspeceFD->toSTL()));
	 
	 //TreeTools * TT = new TreeTools;
	 	 
	 //VÉRIF
	 vector< int > IdFils;
	 IdFils.push_back(S->getNode(C.from_s(EspeceFG->toSTL()))->getId());
	 IdFils.push_back(S->getNode(C.from_s(EspeceFD->toSTL()))->getId());
	 IdFils.push_back(S->getNode(C.from_s(EspeceN->toSTL()))->getId());
	 //cout<<"Num espèces N="<<C.from_s(EspeceN->toSTL())<<", noeud correspondant dans S : "<<S->getNode(C.from_s(EspeceN->toSTL()))->getId()<<endl;
	 //cout<<"Num espèces FG="<<C.from_s(EspeceFG->toSTL())<<", noeud correspondant dans S : "<<S->getNode(C.from_s(EspeceFG->toSTL()))->getId()<<endl;
	 //cout<<"Num espèces FD="<<C.from_s(EspeceFD->toSTL())<<", noeud correspondant dans S : "<<S->getNode(C.from_s(EspeceFD->toSTL()))->getId()<<endl;
	 if (S->getNode(C.from_s(EspeceN->toSTL()))->getId()!=TreeTools::getLastCommonAncestor(*S, IdFils))
	    cout<<"De ajoutePerte : erreur, "<<n->getId()<<" n'est pas l'ancêtre de ses fils dans l'arbre des espèces"<<endl;

	 //
	 //N-FG 
	 //
	       
	 if(EspeceFG!=EspeceN)
	    {
	       //toutes les distances sont à 1 après appel de nommerNoeudsInternes
	       int nbAretes=TreeTools::getDistanceBetweenAnyTwoNodes(*S, NoeudEspN->getId(),NoeudEspFG->getId());
	       //cout<<"Distance dans S entre idN"<<NoeudEspN->getId()<<" et idFG"<<NoeudEspFG->getId()<<" est de "<<nbAretes<<endl;
	       //Ajout d'un noeud supplémentaire en cas de duplication
	       if(typeN->toSTL()==dupl)
		  nbAretes++;
	       
	       Node * x_S= NoeudEspFG->getFather();//x_S parcours la branche dans 
	                                          //l'arbre des espèces
	       for(int i=0; i<nbAretes-1;i++)
		  {
		     //cout<<"PERTES DÉTECTÉES !!!"<<endl;
		     fg=ajouteNoeudPerte(x_S,fg,id,S); //fg modifié suite à cet appel fg=t_G;
		     x_S=x_S->getFather();
		  }
	       //"on raccorde"
	       n->setSon(0,fg); //Car t_G n'est pas connu en dehors des boucle alors que fg oui
	       //segfault quand on met 1 au lieu de 0 !!!
	    }
      	     
	 //
	 //N-FD 
	 //
	 if(EspeceFD!=EspeceN)
	    {
	       //toutes les distances sont à 1 après appel de nommerNoeudsInternes
	       int nbAretes=TreeTools::getDistanceBetweenAnyTwoNodes(*S, NoeudEspN->getId(),NoeudEspFD->getId());
	       //cout<<"Distance dans S entre idN"<<NoeudEspN->getId()<<" et idFD"<<NoeudEspFD->getId()<<" est de "<<nbAretes<<endl;
	       //Ajout d'un noeud supplémentaire en cas de duplication
	       if(typeN->toSTL()==dupl)
		  {
		     nbAretes++;
		     //cout<<"Noeud de dupli"<<endl;
		  }
	       
	       Node * x_S= NoeudEspFD->getFather();//x_S parcours la branche dans 
	                                          //l'arbre des espèces
	       //	       cout<<"On a trouve le père de FD dans S"<<endl;
	       for(int i=0; i<nbAretes-1;i++)
		  {
		     //cout<<"PERTES DÉTECTÉES !!!"<<endl;
		     fd=ajouteNoeudPerte(x_S,fd,id,S); //fd modifié suite à cet appel fd=t_G;
		     x_S=x_S->getFather();
		  }
	       //cout<<"On a passé la boucle"<<endl;
	       
	       //"on raccorde"
	       n->setSon(1,fd); //Car t_G n'est pas connu en dehors des boucle alors que fd oui
	       //cout<<"On a raccordé"<<endl;
	    }
      }
   //cout<<"id en fin de fonction : "<<id<<endl;
}


void reconcil(TreeTemplate<Node >* S,TreeTemplate<Node >* G)
{
      
   //
   // 1/ A chaque noeud de l'arbre de gène, on attribue un noeud de l'abre des espèces. 
   //
   //cout<<"G avant pre-affecte :"<<endl;
   //afficheInfoNoeuds(G->getRootNode());
      preAffecteEspece(G->getRootNode(), G, S);
      //cout<<"G après pre-affecte :"<<endl;
      // cout<<"Racine de G : ";
      // if (G->getRootNode()->hasNodeProperty(esp))
      // 	 {
      // 	    BppString * prop = dynamic_cast<BppString*> (G->getRootNode()->getNodeProperty(esp));
      // 	    cout<<prop->toSTL();
      // 	 }
      // else
      // 	 cout<<"NOSPECIE";
      // cout<<endl;
      // cout<<"Racine de G a des enfants ? "<<G->getRootNode()->getNumberOfSons()<<endl;
      // cout<<"Racine de G est une feuille ? "<<G->getRootNode()->isLeaf()<<endl;

      if (G->getRootNode()->getNumberOfSons()>0)
	 {
	    //afficheInfoNoeuds(G->getRootNode());
	    //suite à l'éventuel élagage de G on renumérote tout pour éviter
	    //d'avoir des trous dans la numérotations de s id et surtout
	    //parce qu'il aurait fallu utiliser G->getNextId() qui ne donne
	    //qu'une id libre d'avance et pose pb quand on veut rajouter
	    //plusieurs noeuds dans la structure. D'autre part ça n'aurait
	    //pas garanti qu'il n'y ait pas de trou (elagage>perte ajoutées)
	    int id=0;
	    renumeroteId(G->getRootNode(),id);
	    /**
	    cout<<"G après renumérote :"<<endl;
	    afficheInfoNoeuds(G->getRootNode());
	    cout<<"*********************************************"<<endl;
	    /**/
	    affecteEspece(G->getRootNode(),S);
	    
	    
	    //
	    // 2/ On annote les duplications et spéciations 
	    //
	    
	    annoteType(G->getRootNode(),S);   
	    //cout << "\nL'arbre G mappé aux noeuds de S :"<<endl;
	    //afficheInfoNoeuds(G->getRootNode());
	    //cout << "FIN arbre G mappé aux noeuds de S :"<<endl<<endl;
	    
	    //a priori que pour format newick
	    //affecteInfoSurBrancheNoeuds(G->getRootNode());
	    
	    //On écrit l'arbre G mappé
	    //newickReader->write(*G,ficArbreGenes+"_mapp",true);
	    
	    //
	    // 3/ On ajoute les pertes 
	    //
	    
	    id=G->getNumberOfNodes();
	    ajoutePerte(G->getRootNode(),id,S); //on supposait que les identifiants de G allaient de 0 à nbNoeuds, mais ce n'est plus possible puisque preAffecteEspece peut élaguer G => on se sert de getNextId() => non, revenu au point de départ suite à renumeroteid ...
	    
	    //cout<<"G RÉCONCILIÉ : "<<endl;
	    //afficheInfoNoeuds(G->getRootNode());
	    //cout<<"FIN AFFICH"<<endl;
	    

	    //a priori que pour format newick
	    // if (OUTPUT_FORMAT==0)                           ****
	    //  affecteInfoSurBrancheNoeuds(G->getRootNode()); ****
	    
	    //cout<<"'"<<G->getRootNode()->hasName()<<"'"<<endl;
	    //cout<<"FIN AFFECTE"<<endl;
	    
	    //On écrit l'arbre G mappé
	    //newickReader->write(*G,ficArbreGenes+"_rec",true);
	    //cout<<"FIN WRITER"<<endl;
	    //delete S;
	    //cout<<"après delete S"<<endl;
	    //delete G;
	    //cout<<"après delete G"<<endl;
	    
	    //delete newickReader;
	 }
}
