#ifndef _PARCOURS_ARRIERE_H_
#define _PARCOURS_ARRIERE_H_

#include <Phyl/TreeTemplate.h>
using namespace bpp;

// The STL library:
#include <iostream>
#include <vector>

using namespace std;

#include "basicRoutines.h"
#include "Reconcil.h"

// auxiliary functions
void setNodeProps(Node * &n, string prop, Node * v1, Node * v2, bool v1dansA1);
Node * parcoursArriereCassure(Node * v1, Node * v2, bool v1dansA1);

// cases for C1
Node * parcoursArriereC1ExtantWithExtant(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1);
Node * parcoursArriereC1GLosWithAny(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1);
Node * parcoursArriereC1GLosWithGLos(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1);
Node * parcoursArriereD1(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1);
Node * parcoursArriereC1GDupWithBTClass(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1);
Node * parcoursArriereD2(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1);
Node * parcoursArriereD12(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1);
Node * parcoursArriereC1GDupWithGDup(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1);
Node * parcoursArriereC1SpecWithSpec(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1);
Node * parcoursArriereC1BClassWithTClass(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1);
Node * parcoursArriereC1TOutWithBClassGDupRec(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1);
Node * parcoursArriereC1NullWithNull(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1);
Node * parcoursArriereC1RecWithRecTB(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1);
Node * parcoursArriereC1RecWithTrans(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1);
Node * parcoursArriereC1TransWithTransTB(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1);
Node * parcoursArriereC1TBWithTB(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1);
Node * parcoursArriereC1TransWithTOut(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1);
Node * parcoursArriereC1TOutWithRecTB(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1);
Node * parcoursArriereO1(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1);
Node * parcoursArriereC1OutWithRecTB(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1);
Node * parcoursArriereC1TOutWithTOut(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1);
Node * parcoursArriereO2(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1);
Node * parcoursArriereO12(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1);
Node * parcoursArriereC1OutWithOut(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1);

// cases for C0
void parcoursArriereC0ExtantWithExtant(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1);
void parcoursArriereC0GLosWithAny(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1);
void parcoursArriereC0GLosWithGLos(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1);
void parcoursArriereD0(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1, bool & indicator);
void parcoursArriereC0GDupWithBTClass(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1);
void parcoursArriereD00(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1, bool & indicator);
void parcoursArriereC0GDupWithGDup(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1);
void parcoursArriereC0SpecWithSpec(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1);
void parcoursArriereC0BClassWithTClass(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1);
void parcoursArriereC0TOutWithBClassGDupRec(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1);
void parcoursArriereC0NullWithNull(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1);
void parcoursArriereC0RecWithRecTB(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1);
void parcoursArriereC0RecWithTrans(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1);
void parcoursArriereC0TransWithTransTB(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1);
void parcoursArriereC0TBWithTB(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1);
void parcoursArriereC0TransWithTOut(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1);
void parcoursArriereC0TOutWithRecTB(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1);
void parcoursArriereC0OutWithRecTB(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1);
void parcoursArriereC0TOutWithTOut(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1);
void parcoursArriereC0OutWithOut(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1);

// functions that do the case analysis for the backtracking
Node * parcoursArriereC1(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1);
Node * parcoursArriereC1_asym(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1, bool & indicator);
void parcoursArriereC0(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1);
void parcoursArriereC0_asym(Node * v1, Node * v2, vector<adjacence> * Adj_classe, vector<Tree *> * ArbresDAdjacences, bool v1dansA1, bool & indicator);

#endif //_PARCOURS_ARRIERE_H_
