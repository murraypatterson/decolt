/*
  module containing many basic routines that may be used by more than
  one of the modules in this package
*/

#ifndef _BASIC_ROUTINES_H_
#define _BASIC_ROUTINES_H_

#include <Phyl/Io/Newick.h>
#include <Phyl/Tree.h>
#include <Phyl/TreeTemplate.h>
#include <Phyl/TreeTemplateTools.h>
#include <Phyl/NodeTemplate.h>
#include <Phyl/Node.h>

using namespace bpp;

// The STL library:
#include <cstdlib>
#include <iostream>
//#include <stringstream>
#include <string>
#include <vector>

using namespace std;

// partitions used with Gergely's format
const string part1 = "_";
const string part2 = ".";
const string part3 = "@";
const string part4 = "|";

//Les constantes //caract�res s�parant les noms de g�ne des noms des esp�ces
const string esp = "S"; //nom de la propri�t� des noeuds contenant leur esp�ce
const string typ = "Ev"; //nom de la propri�t� des noeuds contenant leur type
const string NAD = "L"; //nom de la propri�t� des branches contenant le nb d'adj dupl.
const string bsv = "BSV"; // bootstrap value
const string tsl = "TSL"; // timeslice
const string brn = "BRN"; // name of branch

//Les noms des �v�nements :
const string dupl = "GDup"; 
const string Adupl = "ADup"; 
const string spe = "Spec";   
const string bk = "Break"; 
const string ga = "Extant";  
const string Aper = "ALos";   
const string per = "GLos";   
const string null = "Null";
const string rc = "Reception";
const string trn = "Transfer";
const string tb = "TransferBack";
const string to = "TransferOut";
const string out = "Outside";

const string NomPer = "Perte";  
const string NomPerA = "PerteADJ";  
const string NomCass = "Cassure";  
//const string ficArbreEspeces = "UniversNN.nw";
//const string ficArbreGenes = "g5.nw";

const float INFINI=100000000.0;

struct adjacence{
   string gene1;
   string gene2;
};

//VARIABLES GLOBALES
extern map<string,string> genes_especes;
extern char sep;
extern char sepAdj;
extern int INPUT_FORMAT; //valable pour les arbres de g�nes (l'arbre des esp�ces �tant en newick)
extern int OUTPUT_FORMAT;//0 pour newick ; 1 pour NHX 
//Co�t des diff�rents �v�nements
extern float Adj_percentage;
extern float Spec;
extern float GDup;
extern float GLos;
extern float ADup;
extern float ALos;
extern float Crea;
extern float Break;
extern int id_arbres_adj;
extern bool affich_CalculCout;
extern bool affich_ParcoursArriere;
extern int nb_arbres_adj_tot;
extern int nb_arbres_adj_min;
extern int nb_arbres_adj_max;
extern int nb_GDup;
extern int nb_ADup;
extern int nb_Crea;
extern int nb_Bk;
extern vector<adjacence> DUP;
extern int nb_ATrf;
extern vector<adjacence> TRF;

template <class T> class convert
{  
 public:
   T from_s(string s1)
   {
      T res;
      istringstream ftmp(s1);
      ftmp >> res;
      return (res);
   }
   
   string to_s(const T& f1)
   {
      ostringstream ftmp;
      ftmp << f1;
      return (ftmp.str());
   }
};


void exeunt(); // exit program (debug)}
void ordonnePostfixe(Node* n, vector<Node*>& v); // nodes of n into v in post-order fashion
bool maxForEsp(Node * v);

// output a vector
void afficheVector(vector<int> v);
void afficheVector(vector<string> v);
bool findSinV(vector<string> v, string s);
void afficheVector(vector<adjacence> v);
void afficheVector(vector<Node *> v);

// output a map
void afficheMap(map<string, vector<int> > m);
void afficheMap(map<int, vector<adjacence> > m);
void afficheMap(map<int, vector<string> > m);
void afficheMap(map<string, string> m);
void afficheMap(map<string, int> m);
void afficheMap(map<int, int> m);

int Esp(Node * n); // S(n)
Node * retrieveNode(TreeTemplate<Node> * A, int espece); // node in A of species espece
Node* retrieveNode(TreeTemplate<Node>* S, string branchLabel, int timeSlice);

bool isAncestor(int id1, int id2, TreeTemplate<Node> *A); // is id1 ancestor of id2 in A?
bool isAncestor(Node * n1, Node * n2, TreeTemplate<Node> * A);
int trouveNoeudSpeSAuDessusDeN(int s, int id_n,TreeTemplate<Node> * G);
void retourneIdSpeSAuDessousDeN(vector<int> & Ids, int s, Node * n, TreeTemplate<Node> * G);
int memeEspece(Node * n1, Node * n2);
void getPathFromAtoB(vector<Node *> & nodes, Node * a, Node * b, TreeTemplate<Node> * T); // path in T from a to b
bool equal(adjacence a, adjacence b);
void outputNodeInfo(Node* n);

string E(Node * n); // event assocated with n
string Bootie(Node * n); // bsv(n)
string TimeSl(Node * n);

// min à plusieurs éléments ...
float min(float x, float y);
float min(float x, float y, float z);
float min(float x, float y, float z, float a);
float min(float x, float y, float z, float a, float b);
float min(float x1, float x2, float x3, float x4, float x5, float x6, float x7, float x8, float x9, float x10, float x11, float x12, float x13, float x14, float x15, float x16);

int min_pos(float x, float y);
int min_pos(float x, float y, float z);
int min_pos(float x, float y, float z, float a);
int min_pos(float x, float y, float z, float a, float b);

string nomGene(string nom);
string clToStr(Clonable* c);
string getProperty(Node * n, string prop, bool br);
void afficheInfoNoeud(Node * n);
void afficheInfoNoeuds(Node * n);

#endif // _BASIC_ROUTINES_H_
