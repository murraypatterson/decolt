#include <Phyl/Io/Newick.h>
#include <Bpp/Phyl/Io/Nhx.h>
#include <Phyl/Tree.h>
#include <Phyl/TreeTemplate.h>
#include <Phyl/TreeTemplateTools.h>
#include <Phyl/NodeTemplate.h>
#include <Phyl/Node.h>

using namespace bpp;

// The STL library:
#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <set>
#include <vector>

#include <time.h>
#include <cstdlib>//Pour le rand

using namespace std;

#include "basicRoutines.h"
#include "Reconcil.h"
#include "DECO.h"

//À FAIRE
//Arrêter le prog par des retur(0) quand on affiche des erreurs
//Commenter et rendre propre le prog !!

//On prend en entrée un fichier d'arbres de gène et un fichier de correspondance gène-espèce (2colonne la 1re espèce, la 2e gène)
//On sort un fichier d'arbres de gènes où toutes les feuilles ont été ré-étiqueté en y ajoutant un séparateur (sep) et le nom de l'espèce à laquelle elle appartient
  
//VARIABLES GLOBALES 
//déclarées en externes dans le Reconcil.h
map<string,string> genes_especes;
int INPUT_FORMAT; //valable pour les arbres de gènes (l'arbre des espèces étant en newick)
int OUTPUT_FORMAT;//0 pour newick ; 1 pour NHX 
char sep;
char sepAdj='-';
float Adj_percentage;
float Spec;
float GDup;
float GLos;
float ADup;
float ALos;
float Crea;
float Break;
//VARIABLES GLOBALES 
//déclarées en externes dans fonctionsCout.h
map<pair<Node *,Node *>, float> C0;
map<pair<Node *,Node *>, float> C1;
 
bool ArbresRec;
bool fic_SORTIE_GeneDone;
bool fic_SORTIE_EspeceDone;
bool affich_Classes=false;
//bool affich_Classes=true;
bool affich_CalculCout=false;
//bool affich_CalculCout=true;
bool affich_ParcoursArriere=false;
//bool affich_ParcoursArriere=true;
int nb_arbres_adj_tot=0;
int nb_arbres_adj_min=100000;
int nb_arbres_adj_max=0;
int id_arbres_adj;
int nb_GDup=0;//comptés, comme les autres, dans parcoursArriere
int nb_ADup=0;
int nb_Crea=0;
int nb_Bk=0;
int nb_GDup_arbres=0;//comptés sur arbres réconciliés
int nb_Spe_arbres=0;
int nb_Extant_arbres=0;
int nb_Per_arbres=0;
int nb_Trans_arbres=0;
int nb_Tb_arbres=0;

//Pour recueillir les gènes dupliqués ensemble (sert pour des calculs intermédiaires)
vector<adjacence> DUP;
map<int,int> distrib_taille_compo_connexe;

// for genes that transfered together (co-transfers)
vector<adjacence> TRF;
int nb_ATrf=0; // the count

//Pour lire les noms de fichiers dans le fichier de config
int lireNomFic(ifstream& IfficConf, string& fic, string nomfic, char** argv)
{
   string tampon;
   convert<int> c;
   
   IfficConf>>tampon;
   //cout<<"1er tampon : "<<tampon<<endl;
   if (tampon==nomfic)
      {
	 IfficConf>>fic;
	 //cout<<"2e tampon : "<<fic<<endl;
	 if (fic.find("arg")!=-1)
	    {
	       //cout<<"Il contient arg,";
	       int a=c.from_s(fic.substr(3,fic.length()-1));
	       //cout<<" de numéro "<<a<<endl;
	       fic=argv[a];
	    }
	 return(1);
      }
   else
      {
	 cout<<"Fichier de config incorrect au niveau de "<<nomfic<<endl;
	 return(0);
      }
}

//Pour lire les coûts dans le fichier de config
int lireCout(ifstream& IfficConf, float& Cout, string nomcout)
{
   string tampon;
   convert<float> c;
   
   IfficConf>>tampon;
   if (tampon==nomcout)
      {
	 IfficConf>>tampon;
	 Cout=c.from_s(tampon);
	 return(1);
      }
   else
      {
	 cout<<"Fichier de config incorrect au niveau du cout "<<nomcout<<endl;
	 return(0);
      }
}

int lireFicConfig(string& fic_arbre, string& fic_gene, string& fic_especes, string& fic_adjacence, int argc, char** argv)
{
   convert<int> c;
   convert<char*> ch;
   string ficConfig;
   if (argc==1)
     ficConfig="DeCo.conf";
   else if (argc==2)
     ficConfig=argv[1];
   else if (argc==3) // OVERRIDE
     ficConfig=argv[1]; // OVERRIDE
   else
      {
	 cout<<"Mauvaise utilisation de "<<argv[0]<<" :"<<endl;
	 cout<<"\t"<<argv[0]<<endl;
	 cout<<"(le fichier de configuration se nomme par défaut DeCo.conf)"<<endl;
	 cout<<"ou"<<endl;
	 cout<<"\t"<<argv[0]<<" ficConfig"<<endl;
	 cout<<"(pour un autre fichier de configuration)"<<endl;
	 return(0);
      }

   ifstream IfficConf (ficConfig.c_str(), ios::in);
   if (!IfficConf)
      {
	 cout<<endl<<"Impossible d'ouvrir le fichier de configuration "<<ficConfig<<endl;
      } 
   cout<<"Lecture du fichier de configuration "<<ficConfig<<endl;
   
   int bp=0;
   //Les noms des fichiers
   bp+=lireNomFic(IfficConf,fic_arbre,"fic_arbre",argv);
   cout << "fic_arbre : " << fic_arbre << endl;
   if(argc==3) {
     fic_arbre += "_" + ch.to_s(argv[2]); // OVERRIDE
     cout << "fic_arbre : " << fic_arbre << endl; // OVERRIDE
   }
   bp+=lireNomFic(IfficConf,fic_gene,"fic_gene",argv);
   bp+=lireNomFic(IfficConf,fic_especes,"fic_especes",argv);
   bp+=lireNomFic(IfficConf,fic_adjacence,"fic_adjacence",argv);
   
   if (bp!=4)
      return (0);
   
   string tampon;
   //arbres déjà réconciliés ou pas
   IfficConf>>tampon;
   if (tampon=="ReconcilDone")
      {
	 IfficConf>>tampon;
	 if (tampon=="true")
	    ArbresRec=true;
	 else
	    ArbresRec=false;
      }
   else
      {
	 cout<<"Fichier de config incorrect au niveau de ReconcilDone"<<endl;
	 return (0);
      }
   
   //SORTIE correspondance gènes déjà calculée ou pas
   IfficConf>>tampon;
   if (tampon=="fic_SORTIE_GeneDone")
      {
	 IfficConf>>tampon;
	 if (tampon=="true")
	    fic_SORTIE_GeneDone=true;
	 else
	    fic_SORTIE_GeneDone=false;
      }
   else
      {
	 cout<<"Fichier de config incorrect au niveau de fic_SORTIE_GeneDone"<<endl;
	 return (0);
      }
   
   //SORTIE correspondance espèces déjà calculée ou pas
   IfficConf>>tampon;
   if (tampon=="fic_SORTIE_EspeceDone")
      {
	 IfficConf>>tampon;
	 if (tampon=="true")
	    fic_SORTIE_EspeceDone=true;
	 else
	    fic_SORTIE_EspeceDone=false;
      }
   else
      {
	 cout<<"Fichier de config incorrect au niveau de fic_SORTIE_EspeceDone"<<endl;
	 return (0);
      }

   //les formats d'entrée/sortie
   IfficConf>>tampon;
   if (tampon=="INPUT_FORMAT")
      {
	 IfficConf>>tampon;
	 INPUT_FORMAT=c.from_s(tampon);
      }
   else
      {
	 cout<<"Fichier de config incorrect au niveau de INPUT_FORMAT"<<endl;
	 return (0);
      }
   IfficConf>>tampon;
   if (tampon=="OUTPUT_FORMAT")
      {
	 IfficConf>>tampon;
	 OUTPUT_FORMAT=c.from_s(tampon);
      }
   else
      {
	 cout<<"Fichier de config incorrect au niveau de OUTPUT_FORMAT"<<endl;
	 return (0);
      }
   
   //le séparateur
   IfficConf>>tampon;
   if (tampon=="sep")
      {
	 IfficConf>>tampon;
	 sep=tampon[0];
      }
   else
      {
	 cout<<"Fichier de config incorrect au niveau du séparateur"<<endl;
	 return (0);
      }

   //Les coûts
   bp=0;
   bp+=lireCout(IfficConf,Adj_percentage,"Adj_percentage");
   bp+=lireCout(IfficConf,Spec,"Spec");
   bp+=lireCout(IfficConf,GDup,"GDup");
   bp+=lireCout(IfficConf,GLos,"GLos");
   bp+=lireCout(IfficConf,ADup,"ADup");
   bp+=lireCout(IfficConf,ALos,"ALos");
   bp+=lireCout(IfficConf,Crea,"Crea");
   bp+=lireCout(IfficConf,Break,"Break");   

   // verbose switch
   bool isVerbose;
   IfficConf>>tampon;
   if (tampon=="verbose")
     {
       IfficConf>>tampon;
       if (tampon == "true") {
	 affich_Classes=true;
	 affich_CalculCout=true;
	 affich_ParcoursArriere=true;
       }
     }
   else {cout<<"Ficher de config incorrect au niveau du verbose"<< endl; return(0);}

   IfficConf.close();

   if (bp!=8)
      return (0);
   return(1);
}

void generationFicTulip(string fic_tulip,vector<adjacence> v)
{
   convert<int> c;
   int i;
   vector<adjacence>::iterator it_v;
   set<string> genes;
   set<string>::iterator it_g;
   map<string,int> corres_gene_num;
   map<string,int>::iterator it_m;
   
   ofstream OfficTulip;
   OfficTulip.open(fic_tulip.c_str(), ios::out); 
   if (!OfficTulip)
      cout<<"Problème à la création du fichier "<<fic_tulip<<endl;

   //entête fichier TULIP
   OfficTulip<<"(tlp \"2.0\""<<endl;
   
   //Date
   struct tm Today;
   time_t maintenant;
   time(&maintenant);
   Today = *localtime(&maintenant);
   OfficTulip<<"(date \"";
   OfficTulip<<c.to_s(Today.tm_year + 1900)<<"-"<<c.to_s(Today.tm_mon + 1)<<"-"<<c.to_s(Today.tm_mday)<<" "<<c.to_s(Today.tm_hour)<<":"<<c.to_s(Today.tm_min)<<":"<<c.to_s(Today.tm_sec)<<"\")"<<endl<<endl;
   
   //Auteur et commentaires
   OfficTulip<<"(author \"Sèverine Bérard\")"<<endl;
   OfficTulip<<"(comments \"This file was generated by DeCo.\")"<<endl<<endl;
   
   //Les noeuds de 1 à taille de genes
   for(it_v=v.begin();it_v!=v.end();it_v++)
      {
	 genes.insert((*it_v).gene1);
	 genes.insert((*it_v).gene2);
      }
   OfficTulip<<"(nodes"; 
   for(i=1;i<=genes.size();i++)
      OfficTulip<<" "<<c.to_s(i);
   OfficTulip<<")"<<endl<<endl;
   
   //construction de la correspondance gene-numéro
   i=1;
   for(it_g=genes.begin();it_g!=genes.end();it_g++)
      {
	 corres_gene_num[*it_g]=i;      
	 i++;
      }
   
   //Les arêtes
   i=1;
   for(it_v=v.begin();it_v!=v.end();it_v++)
      {
	 OfficTulip<<"(edge "<<i;
	 OfficTulip<<" "<<corres_gene_num[(*it_v).gene1];
	 OfficTulip<<" "<<corres_gene_num[(*it_v).gene2]<<")"<<endl;
	 i++;
      }
   OfficTulip<<endl<<endl;
   
   //La propriété label pour les noms des noeuds
   OfficTulip<<"(property  0 string \"viewLabel\""<<endl;
   OfficTulip<<"(default \"\" \"\" )"<<endl;
   for(it_m=corres_gene_num.begin();it_m!=corres_gene_num.end();it_m++)
      OfficTulip<<"(node "<<c.to_s((*it_m).second)<<" \""<<(*it_m).first<<"\")"<<endl;
   OfficTulip<<")";

   //clôture fichier TULIP
   OfficTulip<<")";
   OfficTulip.close();
}


//Renvoie vrai si la compo connexe v est linéaire
//Càd si tous ses sommets sont de degrès 2 sauf 2 qui sont de degrès 1
bool isLinear(vector<string> v, map<string,int> deg)
{
   vector<string>::iterator it_v;
   bool linear = true;
   int nb_sommets_deg_1=0;
   for(it_v=v.begin();it_v!=v.end();it_v++)
      {
	 if (deg[*it_v]>2)
	    linear=false;
	 else if (deg[*it_v]==1)
	    nb_sommets_deg_1++;
      }
   return linear&&(nb_sommets_deg_1==2);
}

//Renvoie que les compo connexes de DUPLI de taille >=2
vector<adjacence> composantesConnexes(vector<adjacence> DUPLI)
{
   //Pour calculer les degrés
   map<string,int> deg;
   vector<adjacence>::iterator it_v;
   map<string,int> corres_gene_numCompo;
   map<string,int>::iterator it_m;
   int i=1;
   bool compo_lineaire=true;

   //cout<<"On affiche les gènes dupliqués ensembles :"<<endl;
   for(it_v=DUPLI.begin();it_v!=DUPLI.end();it_v++)
      {
	 //cout<<(*it_v).gene1<<" et "<<(*it_v).gene2<<endl;
	 if (corres_gene_numCompo.find((*it_v).gene1)!=corres_gene_numCompo.end()&&
	     corres_gene_numCompo.find((*it_v).gene2)!=corres_gene_numCompo.end()&&
	     corres_gene_numCompo[(*it_v).gene2]!=corres_gene_numCompo[(*it_v).gene1])
	    {
	       //cout<<"On a trouvé g1 et g2 : corres[g2]="<<corres_gene_numCompo[(*it_v).gene2]<<", corres[g1]="<<corres_gene_numCompo[(*it_v).gene1]<<", on FUSIONNES !!"<<endl;
	       //fusion des deux composantes connexes !
	       int c1=corres_gene_numCompo[(*it_v).gene1];
	       int c2=corres_gene_numCompo[(*it_v).gene2];
	       for(it_m=corres_gene_numCompo.begin();it_m!=corres_gene_numCompo.end();it_m++)
		  if ((*it_m).second==c1)
		     (*it_m).second=c2;
	    }
	 else if (corres_gene_numCompo.find((*it_v).gene1)!=corres_gene_numCompo.end()&&
		  corres_gene_numCompo.find((*it_v).gene2)!=corres_gene_numCompo.end())
	    {
	       //Coup de bol, elles ont déjà été classées dans la même compo connexe
	       //Rien à faire !
	    }	 
	 else if (corres_gene_numCompo.find((*it_v).gene1)!=corres_gene_numCompo.end())
	    {
	       corres_gene_numCompo[(*it_v).gene2]=corres_gene_numCompo[(*it_v).gene1];
	       //cout<<"On a trouvé g1 : corres[g1]="<<corres_gene_numCompo[(*it_v).gene1]<<", corres[g2]="<<corres_gene_numCompo[(*it_v).gene2]<<endl;
	    }
	 else if (corres_gene_numCompo.find((*it_v).gene2)!=corres_gene_numCompo.end())
	    {
	       corres_gene_numCompo[(*it_v).gene1]=corres_gene_numCompo[(*it_v).gene2];
	       //cout<<"On a trouvé g2 : corres[g2]="<<corres_gene_numCompo[(*it_v).gene2]<<", corres[g1]="<<corres_gene_numCompo[(*it_v).gene1]<<endl;
	    }
	 else //nouvelle compo connexe
	    {
	       corres_gene_numCompo[(*it_v).gene1]=i;
	       corres_gene_numCompo[(*it_v).gene2]=i;
	       //cout<<"On a trouvé ni g1 ni g2 : corres[g2]="<<corres_gene_numCompo[(*it_v).gene2]<<", corres[g1]="<<corres_gene_numCompo[(*it_v).gene1]<<endl;
	       i++;
	    }
	 //Pour les degrès
	 if (deg.find((*it_v).gene1)!=deg.end())
	    deg[(*it_v).gene1]++;
	 else
	    deg[(*it_v).gene1]=1;
	 if (deg.find((*it_v).gene2)!=deg.end())
	    deg[(*it_v).gene2]++;
	 else
	    deg[(*it_v).gene2]=1;
      }
  
   // cout<<"Affichage des degrès :"<<endl;
   // afficheMap(deg);

   map<int,vector<string> > compoConnexe;
   map<int,vector<string> >::iterator it_c;
   // cout<<"On affiche les correspondances gènes - n° compo connexe :"<<endl;
   for(it_m=corres_gene_numCompo.begin();it_m!=corres_gene_numCompo.end();it_m++)
      {
	 // cout<<(*it_m).first<<" dans comp n°"<<(*it_m).second<<endl;
	 if(compoConnexe.find((*it_m).second)!=compoConnexe.end())
	    compoConnexe[(*it_m).second].push_back((*it_m).first);
	 else
	    {
	       vector<string> v;
	       v.push_back((*it_m).first);
	       compoConnexe[(*it_m).second]=v;
	    }
      }
   int nb_compo_taille2=0;
   //Tous les gènes dans une composante de taille 2 vont être rangé dans la compo 0
   // cout<<"On affiche par compo connexe :"<<endl;
   // afficheMap(compoConnexe);
   for(it_c=compoConnexe.begin();it_c!=compoConnexe.end();it_c++)
      {
	 int taille=((*it_c).second).size();
	 //cout<<"\nCompo n°"<<(*it_c).first<<" :";
	 //for(int j=0;j<((*it_c).second).size();j++)
	    //cout<<" "<<((*it_c).second)[j];
	 if (taille==2)
	    {
	       nb_compo_taille2++;
	       for(int j=0;j<taille;j++)
		  corres_gene_numCompo[((*it_c).second)[j]]=0;
	    }
	 if (distrib_taille_compo_connexe.find(taille)!=distrib_taille_compo_connexe.end())
	    distrib_taille_compo_connexe[taille]++;
	 else
	    distrib_taille_compo_connexe[taille]=1;
	 compo_lineaire=compo_lineaire&&isLinear((*it_c).second,deg);
      }
   //cout<<endl;
   cout<<"Il y a "<<compoConnexe.size()<<" composantes connexes (taille strictement supérieure à 1)."<<endl;
   cout<<"\t"<<nb_compo_taille2<<" de taille 2."<<endl;
   cout<<"\t"<<compoConnexe.size()-nb_compo_taille2<<" de taille supérieure à 2."<<endl;
   cout<<"Toutes les composantes sont linéaires ? "<<compo_lineaire<<endl;
   //On garde de DUPLI tous les gènes impliqués dans des composantes de taille > 2
   vector<adjacence> DUPLI_sup2;
   for(it_v=DUPLI.begin();it_v!=DUPLI.end();it_v++)
      if(corres_gene_numCompo[(*it_v).gene1]!=0)
	 DUPLI_sup2.push_back(*it_v);

   return DUPLI_sup2;
}
//Pour ajouter les noms des espèces aux feuilles des arbres de gènes si nécessaire
void reetiquette(Node * n)
{
   if (n->isLeaf())
      {
	 if (n->getName().find(sep)==-1)
	    {
	       string nom = n->getName() + sep + genes_especes[n->getName()];
	       //cout<<"Feuille de nom : "<<n->getName()<<"  Nouveau nom :"<<nom;
	       n->setName(nom);
	       //cout<<" - n->getName()="<<n->getName()<<endl;
	    }
      }
   else
      {
	 reetiquette(n->getSon(0));
	 reetiquette(n->getSon(1));
      }
}
//Pour compter le nombre de noeud ayant la propriété prop à la valeur val
int countNodesWithBranchProperty(Node * n, string prop, string val)
{
   int p=0;
   if (n->hasBranchProperty(prop))
      {
	 BppString * VAL = dynamic_cast<BppString*> (n->getBranchProperty(prop)); 
	 if(VAL->toSTL()==val)
	    p=1;
      }
   if (n->getNumberOfSons()==0)
      return p;
   else if (n->getNumberOfSons()==1)
      return p+countNodesWithBranchProperty(n->getSon(0),prop,val);
   else //2 fils 
      return p
	 +countNodesWithBranchProperty(n->getSon(0),prop,val)
	 +countNodesWithBranchProperty(n->getSon(1),prop,val);

}
//Pour compter le nombre de noeud ayant la propriété prop à la valeur val et en stockant les résultats dans la map especes_nb_genes
int countNodesWithBranchPropertyBySpecies(Node * n, string prop, string val, map<int,int>& especes_nb_genes)
{
   int p=0;
   if (n->hasBranchProperty(prop))
      {
	 BppString * VAL = dynamic_cast<BppString*> (n->getBranchProperty(prop)); 
	 if(VAL->toSTL()==val)
	    {
	       p=1;
	       //On rempli la map especes_nb_genes si le noeud est associé à une espèce
	       if (n->hasNodeProperty(esp))
		  {
		     convert<int> c;
		     BppString * ESP = dynamic_cast<BppString*> (n->getNodeProperty(esp));
	             int num_esp=c.from_s(ESP->toSTL()); 
	             if(especes_nb_genes.find(num_esp)!=especes_nb_genes.end())
		        especes_nb_genes[num_esp]++;
	             else
		        especes_nb_genes[num_esp]=1;
		  }
	    }
      }
   if (n->getNumberOfSons()==0)
      return p;
   else if (n->getNumberOfSons()==1)
      return p+countNodesWithBranchPropertyBySpecies(n->getSon(0),prop,val,especes_nb_genes);
   else //2 fils 
      return p
	 +countNodesWithBranchPropertyBySpecies(n->getSon(0),prop,val,especes_nb_genes)
	 +countNodesWithBranchPropertyBySpecies(n->getSon(1),prop,val,especes_nb_genes);

}

//À n'utiliser que si arbre au format Newick a priori
void recupInfoSurBrancheNoeud(Node * n) {
  //cout<<"recupInfoSurBrancheNoeud Id="<<n->getId()<<" a un nom ? "<<n->hasName()<<endl;

  convert<int> C;
  if (n->hasBranchProperty(esp)) {
    BppString * PROP = dynamic_cast<BppString*> (n->getBranchProperty(esp)); 
    string prop=PROP->toSTL();
	 
    int Id =C.from_s(prop.substr(2, prop.find(sep)));
    //cout<<"Id="<<Id<<endl;
    //Attention apparemment ça a été renuméroté par le writer !! et oui !!!
    //n->setId(Id);
    
    prop=prop.substr(prop.find(sep)+1,prop.size());
    
    string name=prop.substr(0, prop.find(sep));
    n->setName(name);
    //cout<<"name="<<name<<" - n->getName()="<<n->getName()<<endl;
    
    prop=prop.substr(prop.find(sep)+1,prop.size());
    
    string Espece=prop.substr(1, prop.find(sep)-1);
    //cout<<"Espece="<<Espece<<endl;
    n->setNodeProperty(esp,BppString(Espece));
    
    prop=prop.substr(prop.find(sep)+1,prop.size());
    
    string type=prop.substr(0, prop.find(sep));
    n->setBranchProperty(typ,BppString(type));
    BppString * TYP = dynamic_cast<BppString*> (n->getBranchProperty(typ));
    //cout<<"type="<<type<<" - n prop typ="<<TYP->toSTL()<<endl;
    
    prop=prop.substr(prop.find(sep)+1,prop.size());
    
    string D = prop.substr(1, prop.find(sep)-1);
    //cout<<"D="<<D<<endl;
    if (D!="NODIST") {
      int d=C.from_s(D);
      n->setDistanceToFather(d);
    }
  }
  else {
    cout<<"De recupInfoSurBrancheNoeud : attention pas de propriété récupérée sur les branches !!!"<<endl;
  }
}

//À n'utiliser que si arbre au format Newick a priori
void recupInfoSurBrancheNoeuds(Node * n, TreeTemplate<Node> * S) {
  if (n->isLeaf()) {
    //Traitement spécial car seul son nom et son Id ont été écrit par le writer newick
    convert<int> C;
    string nom=n->getName();
    string gene=nom.substr(0,nom.find(sep));
    //cout<<"Feuille de gene : "<<gene;
    if (gene==NomPer)
      n->setBranchProperty(typ,BppString(per));
    else
      n->setBranchProperty(typ,BppString(ga));
 
    string nomEsp = nom.substr(nom.rfind(sep,nom.length()-1)+1, nom.length()-1);
    string Id = C.to_s((S->getNode(nomEsp))->getId());
    n->setNodeProperty(esp, BppString(Id));
    //cout<<", d'espèce "<<nomEsp<<" d'id "<<Id<<" dans l'arbre d'espèce"<<endl;
  }
  else {
    recupInfoSurBrancheNoeuds(n->getSon(0),S);
    recupInfoSurBrancheNoeud(n);
    recupInfoSurBrancheNoeuds(n->getSon(1),S);
  }
}

/*
  given the name at a leaf of a gene tree, parse the events: (a)
  transfer/loss, (b) loss and (c) reception and place them into events
  (specie is for debugging purposes) */
void parseLeafEvents(vector<string>& events, string name, string specie) {

  bool verbose = false;

  size_t part2_location = name.find(part2);
  size_t part3_location = name.find(part3);
  //cout << "\".\" location: " << part2_location << endl;
  //cout << "\"@\" location: " << part3_location << endl;
  //cout << "found . : " << (part2_location < name.length()) << endl;
  //cout << "found @ : " << (part3_location < name.length()) << endl;
  //cout << ". before @ : " << (part2_location < part3_location) << endl;
  if(part2_location < name.length() || part3_location < name.length()) { // found "." or "@"
    if(part2_location < part3_location) // "." is before "@" (regardless of existence of "@")
      name = name.substr(part2_location, name.length());
    else // "@" is before "." (regardless of existence of ".")
      name = name.substr(part3_location, name.length());
    // chomp off the stuff before the events in either case
  }
  else { // there are no events
    name = ""; // we reflect this by setting name to ""
  }
  
  // at this point, name will consist of a (possibly empty) set of
  // events that starts with either "." or "@"
  if(verbose) {
    if(name.length() > 0) {
      cout << endl << "specie: " << specie << endl;
      cout << "events string: " << name << endl;
    }
  }
  
  while(name.length() > 0) { // while there are events ...
    // since a string can only start with a "@" (reception of a
    // transfer), it is sure that the remaining events (if any) are
    // separated by ".", i.e., each of the remaining is either a (a)
    // loss or a (b) transfer and loss event, so we record the
    // position of this next "."
    part2_location = (name.substr(1, name.length())).find(part2);
    // in the case that "." does indeed fall in the string:
    // (encapsulated by 'if' to avoid setting location to npos+1 == 0,
    // which has dire consequences)
    if(part2_location < name.length()) ++part2_location;
    //cout << "\".\" location: " << part2_location << endl;
    events.push_back(name.substr(0, part2_location));
    //cout << events.back() << endl; 

    if(part2_location < name.length())
      name = name.substr(part2_location, name.length()); // chomp off the event
    else
      name = "";
  }
}

/*
  given the string (stored in bootstrap value) at an internal node of
  a gene tree, parse the events: (a) reception, (b) loss, (c)
  transfer/loss, (d) transfer, (e) bifurcation or (f) duplication and
  place them into events */
void parseInternalNodeEvents(vector<string>& events, string bootie) {

  bool verbose = false;

  if(verbose) { cout << endl << "bootie: " << bootie << endl; }

  size_t part2_location = bootie.find(part2);
  size_t ta_loc = bootie.find("T@");
  size_t tb_loc = bootie.find("Tb");
  size_t d_loc = bootie.find("D@");
  size_t t_location = (ta_loc < tb_loc ? ta_loc : tb_loc);
  size_t e_location = (t_location < d_loc ? t_location : d_loc);
  size_t min_location = (part2_location < e_location ? part2_location : e_location);
  // min_location will contain min location of ".", "T" or "D" (or npos in case none are found)

  if(bootie.substr(0,1) == part3) { // string starts with "@" (reception of a transfer)
    //cout << "starts with a reception" << endl;

    events.push_back(bootie.substr(0, min_location));
    if(verbose) { cout << events.back() << endl; }

    if(min_location < bootie.length())
      bootie = bootie.substr(min_location, bootie.length()); // chomp off the reception event
    else
      bootie = "";
  }

  string bootieSub;
  string transPrf = "" + part2 + "T" + part3; // ".T@"
  int offset;
  while(bootie.length() > 0) { // while there are events ...
    // since a string can only start with a "@", it is sure that the
    // remaining events (if any) are separated by "." or "T", i.e.,
    // each of the remaining is one of a (a) loss, (b) transfer/loss,
    // (c) transfer (d) bifurcation

    // it is a ".T@" event (and how to deal with it)
    if(bootie.substr(0,3) == transPrf) { offset = 3;
      bootieSub = bootie.substr(offset, bootie.length());; // hack off ".T@" (for bootieSub)
      offset += bootieSub.find(part3); // to ensure that we pass beyond a possible "Tb"
      bootieSub = bootie.substr(offset, bootieSub.length());
    }
    else { offset = 1;
      bootieSub = bootie.substr(offset, bootie.length());
    }
    
    part2_location = bootieSub.find(part2); // these three are relative to offset, of course
    ta_loc = bootieSub.find("T@");
    tb_loc = bootieSub.find("Tb");
    d_loc = bootieSub.find("D@");
    t_location = (ta_loc < tb_loc ? ta_loc : tb_loc);
    e_location = (t_location < d_loc ? t_location : d_loc);
    min_location = (part2_location < e_location ? part2_location : e_location);
    // min_location will contain min location of ".", "T" or "D" (or endOfString in case none are found)
    
    if(min_location < bootie.length()) min_location += offset; // to avoid setting to npos+1
    events.push_back(bootie.substr(0, min_location));
    if(verbose) { cout << events.back() << endl; }

    if(min_location < bootie.length())
      bootie = bootie.substr(min_location, bootie.length()); // chomp off the event
    else
      bootie = "";
  }

  if(verbose) { cout << "===" << endl; }
}

/*
  return branch from bootstrap value that is either a transfer or a
  transferback */
int getBranch(Node *n) {

  convert<int> C;
  string bootie = clToStr(n->getBranchProperty(bsv));

  return(C.from_s(bootie.substr(bootie.find(part4)+1, bootie.length())));
}

/*
  quick and dirty routine where, given a bootstrap value that is
  either (a) a transfer, (b) a transferback, (c) a transfer/loss
  transferback or (d) a reception/transfer, it grabs the branch and
  timeslice from it and places it into the second and third parameters
  respectively */
void getBrnTsl(Node* n, string &brn, int &tsl) {

  convert<int> c;
  string bootie = clToStr(n->getBranchProperty(bsv));
  string first = bootie.substr(0,1);

  if(first == part2 || first == part3) { // "." (or "@")
    // chomp twice the "@" becuase it is a ".T@..Tb@" event (or a "@#|#T@.." event)
    bootie = bootie.substr(bootie.find(part3)+1, bootie.length());
    bootie = bootie.substr(bootie.find(part3)+1, bootie.length());
  }
  else // just chomp once becuase it is a "T@#|#" event
    bootie = bootie.substr(bootie.find(part3)+1, bootie.length());

  tsl = c.from_s(bootie.substr(0, bootie.find(part4)));
  brn = bootie.substr(bootie.find(part4)+1, bootie.length());
}

/*
  recursive version of getBrnTsl

*/
void getBrnTslRec(Node* n, string &brn, int &tsl) {

  convert<int> c;
  getBrnTsl(n, brn, tsl);

  // in case the father is also an outside node, keep climbing
  if(c.from_s(brn) == -1) { getBrnTslRec(n->getFather(), brn, tsl); }
}

/*
  given nodes nCur of G and sCur of S (where the assumption is that
  nCur has been mapped onto sCur), routine (recursively) climbs the
  branch (of subdivided nodes) of S while following it with G by:
  subdividing a node of G, associating it with the next subdivied node
  of S (and the "null" event); until it reaches the node on the branch
  of S with time slice "timeSlice".  deb and idCount are for debugging
  purposes.  (Note: the routines that use this will make sure that we
  only climb on a single branch and that time slice "timeSlice" really
  does exist on this branch, and that we start before this time
  slice) */
void climbBranch(Node*& nCur, Node*& sCur, int timeSlice, int& idCount, int deb) {

  bool verbose = false;

  convert<int> C;

  if(sCur->hasFather() == false) return; // in case s is root
  Node* sFather = sCur->getFather(); // else, continue ...
  int sFather_ts = C.from_s(clToStr(sFather->getNodeProperty(tsl)));

  if(verbose) {
    if(sFather_ts > timeSlice || deb == 1) { // output for debugging
      cout << "+++++++++++++++++++++++++++++++++++++++" << endl;
      cout << "the time slice: " << timeSlice << endl;
      cout << "s father id: " << sFather->getId() << ", his time slice: " << sFather_ts << endl;
      cout << "current n(esp): " << C.from_s(clToStr(nCur->getNodeProperty(esp))) << endl;
      cout << "current s id: " << sCur->getId() << endl;
    }
  }
  
  if(sFather_ts <= timeSlice) { // father of s has time slice < timeSlice, so we climb
    
    Node* nFather = nCur->getFather();
    int nPosition = nFather->getSonPosition(nCur);

    // create and position the new subdivided gene tree node
    Node* subdiv = new Node(idCount); idCount++;
    subdiv->addSon(0, nCur);
    nFather->setSon(nPosition, subdiv);

    // set some properties of this new node (associated specie, etc.)
    subdiv->setNodeProperty(esp, BppString(C.to_s(sFather->getId())));
    subdiv->setBranchProperty(brn, BppString(clToStr(sFather->getBranchProperty(brn))));
    subdiv->setNodeProperty(tsl, BppString(clToStr(sFather->getNodeProperty(tsl))));
    subdiv->setBranchProperty(typ, BppString(null));
    subdiv->setDistanceToFather(1);

    // recurse
    //cout << "recurse" << endl;
    nCur = subdiv; // update the pointers to G and S
    sCur = sFather;
    climbBranch(nCur, sCur, timeSlice, idCount, 0);
  }
  // else, we are already there (so we do nothing)
}

/*
  method sets properties (a) species, (b) branch, (c) time slice and
  (d) type to a bpp node.  It also sets n's distance to it's father to
  1 if setDistPere == 1, o.w., it does not change this distance.
  (this method was implimented just becuase we're always setting these
  4 properties to nodes many times) */
void setFourProperties(Node*& n, string id, Clonable* branch, Clonable* timeSlice, string type) {

  n->setNodeProperty(esp, BppString(id));
  n->setBranchProperty(brn, BppString(clToStr(branch)));
  n->setNodeProperty(tsl, BppString(clToStr(timeSlice)));
  n->setBranchProperty(typ, BppString(type));

  if(n->hasFather()) n->setDistanceToFather(1);
}

/*
  method "pins" node n onto node s, that is, it sets the (a) id, (b)
  branch and (c) time slice of n to that of s, and sets n's type to
  type (using setFourProperties).  Also sets distance to father
  according to setFourProperties */
void pinNode(Node*& n, Node*& s, string type) {

  convert<int> C;
  setFourProperties(n, C.to_s(s->getId()), s->getBranchProperty(brn), s->getNodeProperty(tsl), type);
}
  
/*
  routine for recuperating the gene tree in format with transfers
  given by Gergely.  Routine (a) subdivides the tree (according to
  timeslices), (b) adds the loss (GLos) nodes, while assigning events,
  species, etc. to all the nodes */
void recupGeneTreeWithTransfers(Node* n, TreeTemplate<Node>* S, int& idCount, int count) {
  // remember: part1 = "_", part2 = ".", part3 = "@" and part4 = "|"
  bool verbose = true;
  verbose = false;

  if(verbose) {
    cout << "n's id: " << n->getId() << endl;
    cout << "ids of n's children:" << endl;
    for(int i=0; i< n->getNumberOfSons(); ++i)
      cout << n->getSon(i)->getId() << "\t";
    cout << endl;
  }

  // recup the children first (post-order recursion)
  for(int i=0; i< n->getNumberOfSons(); ++i)
    recupGeneTreeWithTransfers(n->getSon(i), S, idCount, count);
    
  // some declarations used in the routine
  convert<int> C;
  vector<string> events; // to store the events at the node
  string event;
  string transPrf = "" + part2 + "T" + part3; // ".T@"
  Node* s; // to store node of S associated with n
  int timeSlice;
  Node* nCur; Node* sCur; // pointers to G and S to keep as we subdivide, add nodes, etc.
  if(n->isLeaf()) {
    // name will contain a bunch of junk: keep chomping off the front
    // for each attribute
    n->setId(idCount); idCount++;
    if(verbose) { cout << "getting name of leaf node with ID " << n->getId() << " and name " << n->getName() << " ..." << endl; }
    string name = n->getName();
    //cout << "name: " << name << endl;

    // first part will contain the specie
    string specie = name.substr(0, name.find(part1));
    s = S->getNode(specie); // retreive the associated leaf in S
    string specieID = C.to_s(s->getId());
    pinNode(n, s, ga); // pin leaf and its specie together (make it a speciation (ga) node, and set distance to father 1
    //cout << "specie: " << specie << "\tID: " << specieID << endl;

    // continue chomping for gene and set of events
    name = name.substr(name.find(part1)+1, name.length());
    name = name.substr(name.find(part1)+1, name.length()); // (skip over chromosome)
 
    string gene = name.substr(0, name.find(part2)); // hack off "." (if it exists)
    gene = name.substr(0, gene.find(part3)); // hack off "@" (if it exists)
    //cout << "gene: " << gene << endl;

    parseLeafEvents(events, name, specie); // get the events assoc with this leaf

    /* THIS CASE NOT NEEDED NOW BECUASE WE HAVE TREES WITH ANNOTATED LEAVES (EASY TO GET)
    // special case to deal with for joint trees (with no 'reception' annotation on leaves)
    if(events.size() == 0) {
      Node* nFather = n->getFather(); // set up a check if father is a transfer or transferback/out
      string boot = clToStr(nFather->getBranchProperty(bsv));
      if(verbose) { cout << "processing internal node with bootstrap value : " << boot << " ..." << endl; }
      vector<string> fatherEvents;
      parseInternalNodeEvents(fatherEvents, boot);
      string fatherEvent = fatherEvents.back();
      if(verbose) { cout << "first event is : " << fatherEvent;
	cout << ", find : " << fatherEvent.find(specie) << endl; }
      
      // if so, then we must subdivide a reception node above this
      // leaf. Note that we just set the timeslice (0) of this rec to
      // the same as s (as a default, since leaf is not annotated)
      if(fatherEvent.find(part3) < fatherEvent.length()) {
	if(fatherEvent.find(specie) < fatherEvent.length()) {
	  if(verbose) { cout << "this is the branch it comes from (the other child is the receiver)" << endl; }
	}
	else {
	  Node* rec = new Node(idCount); ++idCount;
	  nFather->setSon(nFather->getSonPosition(n), rec); // subdivide
	  pinNode(rec, s, rc);
	  rec->addSon(0, n);
	}
      }
    }
    */

    // now, to the set of leaf events we have parsed
    nCur = n; sCur = s;
    while(events.size() > 0) {
      event = events.back();
      if(verbose) { cout << "event: " << event << endl; }
      
      if(event.substr(0,1) == part2) { // "."
	if(event.substr(0,3) == transPrf) { // ".T@"
 	  if(verbose) { cout << "|||||||||||||| transfer/loss ||||||||||||||" << endl; }
	  int tslFrom = C.from_s(event.substr(3, event.find(part4))); // strip off ".T@" and get (as int) upto "|" (time slice transfer came from)
	  event = event.substr(event.find(part4)+1, event.length()); // chomp ".T@#|"
	  string branchFrom = event.substr(0, event.find(part3)); // branch that the transfer came from
	  event = event.substr(event.find(part3)+1, event.length()); // chomp "branchlabel@"
	  int tslRec = C.from_s(event.substr(0, event.find(part4))); // get (as int) upto | (time slice that transfer was received at)
	  if(verbose) { cout << "from branch: " << branchFrom << " at tsl: " << tslFrom << "; received at tsl: " << tslRec << endl; }

	  // climb S upto tslRec (node sCur of S: to which nCur of G is now mapped)
	  climbBranch(nCur, sCur, tslRec, idCount, 1);
	  if(verbose) { cout << "-------------------------------" << endl << "out of climbBranch" << endl;
	    cout << "nCur(esp): " << C.from_s(clToStr(nCur->getNodeProperty(esp)));
	    cout << ", sCur is: " << sCur->getId() << endl; }

	  // now, for the rest of this 'if' clause for the
	  // transfer/loss event: given father (nFather) of nCur, we
	  // (a) create a new subdivided node (rec) of G b/w nFather
	  // and nCur that will be a reception node, that will have
	  // the same species (and hence time slice) as node of S
	  // (sCur) associated with nCur (see recurrence for a
	  // reception node), and (b) create a new subdivided node
	  // (nFrom) b/w nFather and nCur and associate it with the
	  // node of S (sFrom) from branch branchFrom with timslice
	  // tslFrom and make it a transfer node, and then (c)
	  // subdivide below nFrom and make it a loss node, and give
	  // this new node (lossNode) the same species (/time slice)
	  // as nFrom (see recurrence for a transfer node)
	  
	  Node* nFather = nCur->getFather(); // father of nCur

	  // (a) create new reception node of G, position, set attributes, etc.
	  Node* rec = new Node(idCount); idCount++;
	  nFather->setSon(nFather->getSonPosition(nCur), rec);
	  pinNode(rec, sCur, rc);
	  rec->addSon(0, nCur);

	  // (b) create new transfer node of G, associate it with node of S at branch branchTrans, etc.
	  Node* sFrom = retrieveNode(S, branchFrom, tslFrom);
	  if(verbose) { cout << "node retrieved; ID: " << sFrom->getId() << ", branch: " << clToStr(sFrom->getBranchProperty(brn));
	    cout << ", time slice: " << clToStr(sFrom->getNodeProperty(tsl)) << endl; }
	  Node* nFrom = new Node(idCount); idCount++;
	  nFather->setSon(nFather->getSonPosition(rec), nFrom);
	  pinNode(nFrom, sFrom, trn);

	  // (c) subdivide below father and make it a loss node (see recurrence)
	  Node* lossNode = new Node(idCount, "Perte"); idCount++;
	  nFrom->addSon(0, lossNode); // subdivided node (which is also the loss) as second child
	  nFrom->addSon(1, rec); // now : set reception node as second child (see recurrence)
	  pinNode(lossNode, sFrom, per);
	  if(verbose) { cout << "loss node(esp): " << clToStr(lossNode->getNodeProperty(esp)) << " and id: " << lossNode->getId() << endl; }

	  // finally, set the appropriate nCur and sCur, in order to
	  // chain to the next event (if any).  NOTE: since there are
	  // no events in the data that chain a .T@ event to any other
	  // event, I have not tested this aspect of the routine,
	  // however I am confident that it works properly (like with
	  // loss events)
	  nCur = nFrom;
	  sCur = sFrom;
	  if(verbose) { cout << "finally, nCur(esp): " << clToStr(nCur->getNodeProperty(esp)) << " and sCur(Id): " << sCur->getId() << endl; }
	}
	else { // it's a loss event
	  if(verbose) { cout << "|||||||||||||| loss |||||||||||||||" << endl; }
	  event = event.substr(1, event.length()); // strip off the "."
	  timeSlice = C.from_s(event); // time slice where the loss happened
	  if(verbose) { cout << "time slice: " << timeSlice << endl; }
	   
	  // climb S upto timeSlice-1 (node sCur of S: to which nCur of G is now mapped)
	  climbBranch(nCur, sCur, timeSlice-1, idCount, 1);
	  if(verbose) { cout << "-------------------------------" << endl << "out of climbBranch" << endl;
	    cout << "nCur(esp): " << C.from_s(clToStr(nCur->getNodeProperty(esp)));
	    cout << ", sCur is: " << sCur->getId() << endl; }

	  // now, for the rest of this 'else' clause for the loss
	  // event: given father of nCur, we create a new node of G
	  // that will be a speciation node (to correspond to the
	  // father of sCur of S) that will have nCur as one child,
	  // and a loss as the other

	  Node* nFather = nCur->getFather(); // fathers
	  Node* sFather = sCur->getFather();

	  // create new speciation/loss node of G, position and set attributes
	  Node* specLoss = new Node(idCount); idCount++;
	  int nPosition = nFather->getSonPosition(nCur); // position
	  nFather->setSon(nPosition, specLoss);
	  pinNode(specLoss, sFather, spe); // attributes

	  Node* lossNode = new Node(idCount, "Perte"); idCount++; // create loss node (note that, barring some major modification at this point,
	                                                 // any choice of id is weird, so I chose this one)
	  Node* lossNodeAssoc; // node in S to which lossNode is associated

	  // set up children of this new specLoss node so that G mirrors S
	  //cout << "s father has: " << sFather->getNumberOfSons() << " sons" << endl;
	  if(sFather->getNumberOfSons() != 2) { cout << "error: sCur father does not have 2 sons" << endl; exit(0); }
	  if(verbose) { cout << "sCur's position wrt father: " << sFather->getSonPosition(sCur) << endl; }
	  if(sFather->getSonPosition(sCur) == 0) {
	    if(verbose) { cout << "sCur position is 0" << endl; }
	    specLoss->addSon(0, nCur);
	    specLoss->addSon(1, lossNode);
	    lossNodeAssoc = sFather->getSon(1);
	  }
	  else { // else, sCur has position 1
	    if(verbose) { cout << "sCur position is 1" << endl; }
	    specLoss->addSon(0, lossNode);
	    specLoss->addSon(1, nCur);
	    lossNodeAssoc = sFather->getSon(0);
	  }
	  
	  pinNode(lossNode, lossNodeAssoc, per); // set attributes to loss node

	  // finally, set nCur and sCur to their fathers, in order to chain to the next event (if any)
	  nCur = specLoss;
	  sCur = sFather;
	}
      }
      else { // it starts with "@": i.e., it's a reception of a transfer event
	if(verbose) { cout << "|||||||||||||| reception ||||||||||||||" << endl; }
	int tslRec = C.from_s(event.substr(1, event.find(part4))); // strip off "@" and get (as int) upto | (time slice that transfer was received at)
	if(verbose) { cout << "received at tsl: " << tslRec << endl; }

	// climb S upto tslRec (node sCur of S: to which nCur of G is now mapped)
	climbBranch(nCur, sCur, tslRec, idCount, 1);
	if(verbose) { cout << "-------------------------------" << endl << "out of climbBranch" << endl;
	  cout << "nCur(esp): " << C.from_s(clToStr(nCur->getNodeProperty(esp)));
	  cout << ", sCur is: " << sCur->getId() << endl; }

	// now, for the rest of this 'else' clause for the reception
	// event: given father (nFather) of nCur, we create a new
	// subdivided node (rec) of G b/w nFather and nCur that will
	// be a reception node, that will have the same species (and
	// hence time slice) as node of S (sCur) associated with nCur
	// (see recurrence for a reception node), and then leave it at
	// that . . . making the connection to rec's transfer node
	// when we get the parent of rec in G, which will contain the
	// necessary information needed to make this connection, i.e.,
	// parent will surely contain "T@#|..." (or "Tb@#|...") from
	// which we will recognize it as a transfer (back) node, etc.

	Node* nFather = nCur->getFather(); // father of nCur
	
	// create new reception node of G, position, set attributes, etc.
	Node* rec = new Node(idCount); idCount++;
	nFather->setSon(nFather->getSonPosition(nCur), rec);
	pinNode(rec, sCur, rc);
	rec->addSon(0, nCur);

	// and we're done.  Note that we do not update nCur and sCur
	// because, by definition, a reception does not chain to any
	// other event
      }
      
      events.pop_back();
    }
  }
  else { // n is an internal node
    string bootie = clToStr(n->getBranchProperty(bsv));
    n->setId(idCount); idCount++;
    if(verbose) { cout << "processing internal node with bootstrap value : " << bootie << " ..." << endl; }
    parseInternalNodeEvents(events, bootie);

    event = events.back(); // there is always one event for internal node, and this
    events.pop_back(); // event is either a bifurcation ".#", a transfer ("T@" or "Tb@") a duplication "D@" or a transfer back/loss ".T@..Tb@" event
    if(verbose) { cout << "event : " << event << endl; }

    if(event.substr(0,1) == part2) { // "."
      if(event.substr(0,3) == transPrf) { // ".T@", i.e., it is a ".T@..Tb@" event
	if(verbose) { cout << "|||||||||||||| transfer/loss transferback ||||||||||||||" << endl; }
	int tslFrom = C.from_s(event.substr(3, event.find(part4))); // strip off ".T@" ...
	event = event.substr(event.find(part4)+1, event.length()); // chomp ".T@#|"
	string branchFrom = event.substr(0, event.find("Tb"+part3)); // find upto "Tb@"
	event = event.substr(event.find(part3)+1, event.length()); // chomp "branchlabelTb@"
	int tslTb = C.from_s(event.substr(0, event.find(part4)));
	event = event.substr(event.find(part4)+1, event.length()); // chomp ...
	string branchTb = event;
	if(verbose) { cout << "at tsl : " << tslFrom << " from branch : " << branchFrom;
	  cout <<"; received/transferred back at tsl : " << tslTb << " from branch : " << branchTb << endl; }

	Node* sTb = retrieveNode(S, branchTb, tslTb); // retreive corresponding node in S
	string spID = C.to_s(sTb->getId());
	if(verbose) { cout << "node retrieved; ID : " << spID << ", branch : " << clToStr(sTb->getBranchProperty(brn));
	  cout << ", time slice : " << clToStr(sTb->getNodeProperty(tsl)) << endl << endl; }
	pinNode(n, sTb, tb); // pin n to sTb

	// now to the children of n (the transferback node)
	if(n->getNumberOfSons() != 2) { cout << "error : transferback node n does not have 2 sons" << endl; exit(0); }
	// position of the child of n on the branch from which the transfer originates (will always be 0 : see recurrence)
	string lT = clToStr(n->getSon(0)->getBranchProperty(typ));
	string rT = clToStr(n->getSon(1)->getBranchProperty(typ));
	if(lT == rc || lT == tb || lT == to) { n->swap(0,1); } // swap the children
	else if(rT == rc || rT == tb || rT == to) {} // do nothing
	else { cout << "error : neither child of transferback node n is a reception or transferback/out node" << endl; exit(0); }

	// now we climb (its branch) from n's nfPos child up to n
	nCur = n->getSon(0);
	sCur = S->getNode(C.from_s(clToStr(nCur->getNodeProperty(esp))));
	int timeSlice = C.from_s(clToStr(n->getNodeProperty(tsl)));
	if(verbose) { cout << "nCur(esp) : " << clToStr(nCur->getNodeProperty(esp)) << ", sCur : " << sCur->getId();
	  cout << ", timeSlice : " << timeSlice << endl; }
	climbBranch(nCur, sCur, timeSlice, idCount, 1);

	// now we (subdivide in order to) create a new transfer node for the ".T@" part
	Node* nFrom = new Node(idCount); ++idCount;
	Node* nFather = n->getFather(); // subdivide
	nFather->setSon(nFather->getSonPosition(n), nFrom);
	Node* sFrom = retrieveNode(S, branchFrom, tslFrom); // retrieve corresponding node in S
	pinNode(nFrom, sFrom, trn);
	if(verbose) { cout << "nFrom(esp) : " << clToStr(nFrom->getNodeProperty(esp)) << ", sFrom : " << sFrom->getId() << endl; }

	// now we create a loss node for the ".T@" part
	Node* lossNode = new Node(idCount, "Perte"); ++idCount;
	nFrom->addSon(0,lossNode);
	nFrom->addSon(1,n); // now : set reception node as second child (see recurrence)
	pinNode(lossNode, sFrom, per);

	nCur = nFrom; sCur = sFrom; // update positions for next event
      }
      else { // it's a bifurcation
	if(verbose) { cout << "|||||||||||||| bifurcation ||||||||||||||" << endl; }
	string branchLabel = event.substr(1, event.length()); // strip off "."
	timeSlice = C.from_s(branchLabel);
	s = retrieveNode(S, branchLabel, timeSlice);
	string spID = C.to_s(s->getId());
	if(verbose) {cout << "node retrieved; ID : " << spID << ", branch : " << clToStr(s->getBranchProperty(brn));
	  cout << ", time slice : " << clToStr(s->getNodeProperty(tsl)) << endl << endl; }
	pinNode(n, s, spe); // pin n to s
	
	// now to the children
	if(n->getNumberOfSons() != 2) { cout << "error : bifurcation node n does not have 2 sons" << endl; exit(0); }
	if(s->getNumberOfSons() != 2) { cout << "error : bifurcation node s does not have 2 sons" << endl; exit(0); }

	// deal with left child (child 0) of bifurcation node n
	Node* nLeft = n->getSon(0); Node* sLeft = S->getNode(C.from_s(clToStr(nLeft->getNodeProperty(esp)))); // left child
	if(verbose) { cout << "nLeft(esp) : " << clToStr(nLeft->getNodeProperty(esp)) << ", sLeft(id) : " << sLeft->getId() << endl; }
	climbBranch(nLeft, sLeft, timeSlice-1, idCount, 1);
	if(verbose) { cout << "--------------------------------------------------" << endl << "out of climbBranch" << endl;
	  cout << "now nLeft(esp) : " << clToStr(nLeft->getNodeProperty(esp)) << ", now sLeft(id) : " << sLeft->getId() << endl << endl; }
	
	// deal with right child of bifurcation node n
	Node* nRight = n->getSon(1); Node* sRight = S->getNode(C.from_s(clToStr(nRight->getNodeProperty(esp)))); // right child
	if(verbose) { cout << "nRight(esp) : " << clToStr(nRight->getNodeProperty(esp)) << ", sRight(id) : " << sRight->getId() << endl; }
	climbBranch(nRight, sRight, timeSlice-1, idCount, 1);
	if(verbose) { cout << "--------------------------------------------------" << endl << "out of climbBranch" << endl;
	  cout << "now nRight(esp) : " << clToStr(nRight->getNodeProperty(esp)) << ", now sRight(id) : " << sRight->getId() << endl << endl; }

	nCur = n; sCur = s; // update positions for next event
      }
    }
    else if(event.substr(0,2) == "T" + part3) { // it's a transfer
      if(verbose) { cout << "|||||||||||||| transfer ||||||||||||||" << endl; }
      int tslFrom = C.from_s(event.substr(2, event.find(part4))); // strip off "T@" and get (as int) upto "|"
      event = event.substr(event.find(part4)+1, event.length()); // chomp "T@#|"
      string branchFrom = event.substr(0, event.length());
      if(verbose) { cout << "at time slice : " << tslFrom << " from branch : " << branchFrom << endl; }

      if(C.from_s(branchFrom) == -1) { // handle the case when gene is transferred out of the tree
	if(verbose) { cout << "transfer out" << endl; }

	Node* nFather = n->getFather();
	string fBrn; int fTsl; // need this info first ...
	getBrnTslRec(nFather, fBrn, fTsl); // ...
	if(verbose) { cout << "father branch : " << fBrn << ", father time slice : " << fTsl << endl; }
	Node* sTo = retrieveNode(S, fBrn, fTsl); // then we retrieve corresponding node in S
	if(verbose) { cout << "node retrieved; ID : " << sTo->getId() << ", branch : " << clToStr(sTo->getBranchProperty(brn));
	  cout << ", time slice : " << clToStr(sTo->getNodeProperty(tsl)) << endl << endl; }
	pinNode(n,sTo,out); // we pin this outside node to its father in S (to give it a species, tsl, etc.)
	n->setBranchProperty(brn, BppString("-1")); // but give it branch label -1 to mark it as outside the tree

	// subdivide above n to create a transferout node (but only if the father is not another outside node)
	if(getBranch(nFather) != -1) {
	  Node* nTo = new Node(idCount); ++idCount; // subdivide
	  nFather->setSon(nFather->getSonPosition(n), nTo);
	  nTo->addSon(0,n);
	  pinNode(nTo, sTo, to); // pin nTo to sTo
	}
      }
      else { // it's just a normal transfer
	s = retrieveNode(S, branchFrom, tslFrom); // retrieve corresponding node in S
	string spID = C.to_s(s->getId());
	if(verbose) { cout << "node retrieved; ID : " << spID << ", branch : " << clToStr(s->getBranchProperty(brn));
	  cout << ", time slice : " << clToStr(s->getNodeProperty(tsl)) << endl << endl; }
	pinNode(n, s, trn); // pin n to s
       
	// now to the children
	if(n->getNumberOfSons() != 2) { cout << "error : transfer node n does not have 2 sons" << endl; exit(0); }
	// position of the child of n on the branch from which the transfer originates (will always be 0 : see recurrence)
	string lT = clToStr(n->getSon(0)->getBranchProperty(typ));
	string rT = clToStr(n->getSon(1)->getBranchProperty(typ));
	if(lT == rc || lT == tb || lT == to) { n->swap(0,1); } // swap the children
	else if(rT == rc || rT == tb || rT == to) {} // do nothing
	else { cout << "error : neither child of transfer node n is a reception or transferback/out node" << endl; exit(0); }
	
	// now we climb (its branch) from n's nfPos child up to n
	nCur = n->getSon(0);
	sCur = S->getNode(C.from_s(clToStr(nCur->getNodeProperty(esp))));
	int timeSlice = C.from_s(clToStr(n->getNodeProperty(tsl)));
	if(verbose) { cout << "nCur(esp) : " << clToStr(nCur->getNodeProperty(esp)) << ", sCur : " << sCur->getId();
	  cout << ", timeSlice : " << timeSlice << endl; }
	if(sCur->hasFather()) { // can only use climbBranch if sCur is not the root
	  climbBranch(nCur, sCur, timeSlice, idCount, 1);
	}
	else { // sCur is the root of S, so we have to subdivide manually (cannot use climbBranch)
	  if(verbose) { cout << "sCur is the root of S" << endl; }
	}

	nCur = n; sCur = s; // update positions for next event
      }
    }
    else if(event.substr(0,2) == "D" + part3) { // it's a duplication
      if(verbose) { cout << "|||||||||||||| duplication ||||||||||||||" << endl; }
      int timeSlice = C.from_s(event.substr(2, event.find(part4))); // strip off "D@" and get (as int) upto "|"
      event = event.substr(event.find(part4)+1, event.length()); // chomp "D@#|"
      string branchLabel = event.substr(0, event.length());
      if(verbose) { cout << "at time slice : " << timeSlice << " at branch : " << branchLabel << endl; }
      s = retrieveNode(S, branchLabel, timeSlice); // retrieve corresponding node in S
      int spID = s->getId();
      if(verbose) { cout << "node retrieved; ID : " << spID << ", branch : " << clToStr(s->getBranchProperty(brn));
	cout << ", time slice : " << clToStr(s->getNodeProperty(tsl)) << endl << endl; }
      pinNode(n, s, dupl); // pin n to s

      // now to the children
      if(n->getNumberOfSons() != 2) { cout << "error : duplication node n does not have 2 sons" << endl; exit(0); }

      Node * nLeft = n->getSon(0);
      if(Esp(nLeft) != spID) { // set of nulls below left of duplication node
	if(verbose) { cout << "left child is null, we must climb branch" << endl; }
	Node * sLeft = S->getNode(C.from_s(clToStr(nLeft->getNodeProperty(esp))));
	if(verbose) { cout << "nLeft(esp) : " << clToStr(nLeft->getNodeProperty(esp)) << ", sLeft(id) : " << sLeft->getId() << endl ;}
	climbBranch(nLeft, sLeft, timeSlice, idCount, 1);
	if(verbose) { cout << "now nLeft(esp) : " << clToStr(nLeft->getNodeProperty(esp)) << ", now sLeft(id) : " << sLeft->getId() << endl << endl; }
      }

      Node * nRight = n->getSon(1);
      if(Esp(nRight) != spID) { // set of nulls below right of duplication node
	if(verbose) { cout << "left child is null, we must climb branch" << endl; }
	Node * sRight = S->getNode(C.from_s(clToStr(nRight->getNodeProperty(esp))));
	if(verbose) { cout << "nRight(esp) : " << clToStr(nRight->getNodeProperty(esp)) << ", sRight(id) : " << sRight->getId() << endl ;}
	climbBranch(nRight, sRight, timeSlice, idCount, 1);
	if(verbose) { cout << "now nRight(esp) : " << clToStr(nRight->getNodeProperty(esp)) << ", now sLeft(id) : " << sRight->getId() << endl << endl; }
      }

      // sanity check : both children should have same species (s) as n
      if(Esp(n->getSon(0)) != spID) { cout << "error : left child of duplication node n has different species as n" << endl;
	cout << "left child : " << Esp(n->getSon(0)) << endl;
	outputNodeInfo(n->getSon(0));
	cout << "n : " << spID << endl;
	outputNodeInfo(n);
	exit(0); }
      if(Esp(n->getSon(1)) != spID) { cout << "error : right child of duplication node n has different species as n" << endl; exit(0); }
      
      nCur = n; sCur = s; // update positions for next event
    }
    else { // it must be a transfer back "Tb@"
      if(verbose) { cout << "|||||||||||||| transfer back ||||||||||||||" << endl; }
      int tslTb = C.from_s(event.substr(3, event.find(part4))); // strip off "Tb@" ...
      event = event.substr(event.find(part4)+1, event.length()); // chomp "Tb@#|"
      string branchTb = event.substr(0, event.length());
      if(verbose) { cout << "at time slice : " << tslTb << " from branch : " << branchTb << endl; }

      s = retrieveNode(S, branchTb, tslTb); // retreive corresponding node in S
      string spID = C.to_s(s->getId());
      if(verbose) { cout << "node retrieved; ID : " << spID << ", branch : " << clToStr(s->getBranchProperty(brn));
	cout << ", time slice : " << clToStr(s->getNodeProperty(tsl)) << endl << endl; }
      pinNode(n, s, tb); // pin n to s

      // now to the children
      if(n->getNumberOfSons() != 2) { cout << "error : transferback node n does not have 2 sons" << endl; exit(0); }
      int nfPos;
      string lT = clToStr(n->getSon(0)->getBranchProperty(typ));
      string rT = clToStr(n->getSon(1)->getBranchProperty(typ));
      if(lT == rc || lT == tb || lT == to) { n->swap(0,1); } // swap the children
      else if(rT == rc || rT == tb || rT == to) {} // do nothing
      else { cout << "error : neither child of transferback node n is a reception or transferback/out node" << endl; exit(0); }

      // now we climb (its branch) from n's nfPos child up to n
      nCur = n->getSon(0);
      sCur = S->getNode(C.from_s(clToStr(nCur->getNodeProperty(esp))));
      int timeSlice = C.from_s(clToStr(n->getNodeProperty(tsl)));
      if(verbose) { cout << "nCur(esp) : " << clToStr(nCur->getNodeProperty(esp)) << ", sCur : " << sCur->getId();
	cout << ", timeSlice : " << timeSlice << endl; }
      climbBranch(nCur, sCur, timeSlice, idCount, 1);
    }
    
    // now we process the remaining events which can be of (a) transfer/loss, (b) loss or (c) reception
    while(events.size() > 0) {
      event = events.back();
      events.pop_back();
      if(verbose) { cout << "event : " << event << endl; }

      if(event.substr(0,1) == part2) { // "."
	if(event.substr(0,3) == transPrf) { // ".T@"
	  if(verbose) { cout << "|||||||||||||| transfer/loss ||||||||||||||" << endl; }
	  int tslFrom = C.from_s(event.substr(3, event.find(part4))); // strip off ".T@" ...
	  event = event.substr(event.find(part4)+1, event.length()); // chomp ".T@#|"
	  string branchFrom = event.substr(0, event.find(part3));
	  event = event.substr(event.find(part3)+1, event.length()); // chomp ...
	  int tslRec = C.from_s(event.substr(0, event.find(part4)));
	  if(verbose) { cout << "at tsl : " << tslFrom << " from branch : " << branchFrom << "; received at tsl : " << tslRec << endl; }

	  // climb up to tslRec
	  climbBranch(nCur, sCur, tslRec, idCount, 1);
	  if(verbose) { cout << "-------------------------------------" << endl << "out of climbBranch" << endl;
	    cout << "nCur(esp) : " << C.from_s(clToStr(nCur->getNodeProperty(esp)));
	    cout << ", sCur is : " << sCur->getId() << endl; }

	  // (subdivide in order to) create a new reception node above nCur
	  Node* rec = new Node(idCount); ++idCount;
	  Node* nFather = nCur->getFather(); // subdivide
	  nFather->setSon(nFather->getSonPosition(nCur), rec);
	  rec->addSon(0, nCur);
	  pinNode(rec, sCur, rc); // finally, attributes

	  // now we again (subdivide in order to) create a new transfer node above this reception node
	  Node* nFrom = new Node(idCount); ++idCount;
	  nFather->setSon(nFather->getSonPosition(rec), nFrom);
	  Node* sFrom = retrieveNode(S, branchFrom, tslFrom); // get correponding node in S
	  if(verbose) { cout << "node retrieved; ID : " << sFrom->getId() << ", branch : " << clToStr(sFrom->getBranchProperty(brn));
	    cout << "time slice : " << clToStr(sFrom->getNodeProperty(tsl)) << endl; }
	  pinNode(nFrom, sFrom, trn);

	  // finally, create corresponding loss node
	  Node* lossNode = new Node(idCount, "Perte"); ++idCount;
	  nFrom->addSon(0, lossNode);
	  nFrom->addSon(1, rec);

	  pinNode(lossNode, sFrom, per);
	  if(verbose) { cout << "loss node(esp) : " << clToStr(lossNode->getNodeProperty(esp)) << "and id : " << lossNode->getId() << endl; }
	  nCur = nFrom; sCur = sFrom; // update positions for next event
	}
	else { // it's a loss event
	  if(verbose) { cout << "|||||||||||||| loss ||||||||||||||" << endl; }
	  event = event.substr(1, event.length()); // strip off the "."
	  timeSlice = C.from_s(event);
	  if(verbose) { cout << "time slice : " << timeSlice << endl; }

	  // climb up to timeSlice-1
	  climbBranch(nCur, sCur, timeSlice-1, idCount, 1);
	  if(verbose) { cout << "-------------------------------------" << endl << "out of climbBranch" << endl;
	    cout << "nCur(esp) : " << C.from_s(clToStr(nCur->getNodeProperty(esp)));
	    cout << ", sCur is : " << sCur->getId() << endl; }

	  Node* nFather = nCur->getFather(); // fathers
	  Node* sFather = sCur->getFather();

	  // create new speciation/loss node ...
	  Node* specLoss = new Node(idCount); ++idCount;
	  int nPos = nFather->getSonPosition(nCur);
	  nFather->setSon(nPos, specLoss);
	  pinNode(specLoss, sFather, spe); // attributes

	  Node* lossNode = new Node(idCount, "Perte"); ++idCount; // loss node (and its associate in S)
	  Node* lossNodeAssoc;

	  // set up children of this new specLoss node ...
	  if(sFather->getNumberOfSons() != 2) { cout << "error : sCur father does not have 2 sons" << endl; exit(0); }
	  if(verbose) { cout << "sCur's position wrt father : " << sFather->getSonPosition(sCur) << endl; }
	  if(sFather->getSonPosition(sCur) == 0) { //cout << "sCur position is 0" << endl;
	    specLoss->addSon(0, nCur);
	    specLoss->addSon(1, lossNode);
	    lossNodeAssoc = sFather->getSon(1);
	  }
	  else { //cout << "sCur position is 1" << endl;
	    specLoss->addSon(0, lossNode);
	    specLoss->addSon(1, nCur);
	    lossNodeAssoc = sFather->getSon(0);
	  }

	  pinNode(lossNode, lossNodeAssoc, per); // attributes
	  nCur = specLoss; sCur = sFather; // update positions for next event
	}
      }
      else { // it is a reception event
	if(verbose) { cout << "|||||||||||||| reception ||||||||||||||" << endl; }
	int tslRec = C.from_s(event.substr(1, event.find(part4)));
	if(verbose) { cout << "received at time slice : " << tslRec << endl; }
	
	// climb up to tslRec
	climbBranch(nCur, sCur, tslRec, idCount, 1);
	if(verbose) { cout << "-------------------------------------" << endl << "out of climbBranch" << endl;
	  cout << "nCur(esp) : " << C.from_s(clToStr(nCur->getNodeProperty(esp)));
	  cout << ", sCur is : " << sCur->getId() << endl; }

	// now, create a reception node above nCur
	Node* nFather = nCur->getFather();
	Node* rec = new Node(idCount); ++idCount;
	nFather->setSon(nFather->getSonPosition(nCur), rec);
	pinNode(rec, sCur, rc);
	rec->addSon(0, nCur);
      }
    }  
  }
}

/*
  routing "cleans" the leaves of a gene tree (getting rid of the
  annotations added to the end of the name, like "@0|SYNPX" for
  example) */
void cleanTree(Node* n) {

  size_t part2_location;
  size_t part3_location;   
  size_t min_location;
  string name;

  // clean off any annotations
  if(n->isLeaf()) {
    name = n->getName();
    part2_location = name.find(part2);
    part3_location = name.find(part3);
    min_location = part2_location < part3_location ? part2_location : part3_location;
    name = name.substr(0, min_location);
    n->setName(name);
  }

  // recursion
  for(int i=0; i< n->getNumberOfSons(); ++i)
    cleanTree(n->getSon(i));
}

/*
  routine to see if pair p, giving ids (idp1,idp2) really corresponds
  to leaves given by id1,id2, i.e., idp1 (idp2) is ancestor of ida
  (idb) where {a,b} \in {1,2} and a!=b ... this is sufficient to give
  1:1 correspondence between leaves and their representatives, because
  equivalence classes are defined by maximal pairs (i.e., from
  getHigest)
*/
bool correspondsTo(adjacence p, int id1, int id2, TreeTemplate<Node> * a) {

  convert<int> c;
  int idp1 = c.from_s(p.gene1);
  int idp2 = c.from_s(p.gene2);

  if(isAncestor(idp1, id1, a)) // try (1,1) (2,2)
    if(isAncestor(idp2, id2, a))
      return true;

  if(isAncestor(idp1, id2, a)) // try (1,2) (2,1)
    if(isAncestor(idp2, id1, a))
      return true;

  return false; // neither works, it cannot be
}

/*
  get LCA from two paths, created by getPathFromAtoB, ns1 (ns2) from
  root of some tree T to leaf l1 (l2) of this T : auxiliary function
  of getHighest
*/
Node * getLCA(vector<Node *> ns1, vector<Node *> ns2) {

  for(int i=ns1.size()-1; i>= 0; --i) // go from leaves to root to guarantee we find lca
    for(int j=ns2.size()-1; j>= 0; --j)
      if(ns1[i] == ns2[j])
	return(ns1[i]);
}

/*
  get highest pair of nodes in (a1, a2) that has the same species but
  are not the same nodes (in the case that a1 == a2), and is an
  ancestor of (l1, l2)

*/
pair<Node *, Node *> getHighest(Node * l1, Node * l2, TreeTemplate<Node> * a1, TreeTemplate<Node> * a2) {

  bool verbose = true;
  verbose = false;

  vector<Node *> ns1, ns2;
  getPathFromAtoB(ns1, a1->getRootNode(), l1, a1);
  getPathFromAtoB(ns2, a2->getRootNode(), l2, a2);

  if(verbose) {
    cout << "path 1 :" << endl;
    afficheVector(ns1);
    cout << endl;
    cout << "path 2 :" << endl;
    afficheVector(ns2);
    cout << endl;
  }

  if(a1 != a2) {
    for(int i=0; i< ns1.size(); ++i)
      for(int j=0; j< ns2.size(); ++j)
	if(Esp(ns1[i]) == Esp(ns2[j])) { // the first match should be the highest

	  if(verbose) {
	    cout << "returning pair (" << ns1[i]->getId() << ", " << ns2[j]->getId() << ")" << endl << endl;
	  }

	  pair<Node *, Node *> p(ns1[i], ns2[j]);
	  return p;
	}
  }
  else {
    Node * lca = getLCA(ns1, ns2);  // lca in a1
    //cout << "LCA is : " << endl;
    //afficheInfoNoeud(lca);

    // cut paths ns1 (ns2) of a1 right before their lca (in leaves to
    // root traversal)
    int p1=-1;
    int p2=-1;
    for(int i=ns1.size()-1; i>= 0; --i) {
      if(ns1[i] == lca)
	break;
      p1 = i;
    }
    for(int i=ns2.size()-1; i>= 0; --i) {
      if(ns2[i] == lca)
	break;
      p2 = i;
    }
    
    // the two paths ns1, ns2 in a1, properly below lca are now
    // disjoint : do as if they came from two different trees (i.e.,
    // as if a1 != a2)
    for(int i=p1; i< ns1.size(); ++i)
      for(int j=p2; j< ns2.size(); ++j)
	if(Esp(ns1[i]) == Esp(ns2[j])) { // again, first match should be the highest
	  pair<Node *, Node *>p(ns1[i],ns2[j]);
	  return p;
	}
  }

  cout << "error : highest node was not found !!!";
  exit(0);
}

// add a pair of nodes to an adjacency list
void addToList(vector<adjacence> & Ids, pair<Node *, Node *> p) {

  convert<int> c;
  adjacence a;
  a.gene1 = c.to_s(p.first->getId());
  a.gene2 = c.to_s(p.second->getId());

  bool found = false;
  for(int i=0; i< Ids.size(); ++i) // look for a in Ids
    if(equal(Ids[i], a))
      found = true;

  if(!found)
    Ids.push_back(a);
}

/*
  we now get all highest pairs (n_1,n_2) of G_1 x G_2 such that
  S(n_1)=S(n_2) (and n_1 != n_2), and (n_1,n_2) is the ancestor of at
  least one adjacency of the considered set of adjacencies

*/
void retournePairsOfIds(vector<adjacence> & Ids, vector<int> ids1, vector<int> ids2, TreeTemplate<Node> * a1, TreeTemplate<Node> * a2) {

  bool verbose = true;
  verbose = false;

  // find highest pair for each extant adjacency and add it to the list (if it doesn't already exist)
  for(int i=0; i< ids1.size(); ++i) {
    Node * l1 = a1->getNode(ids1[i]);
    Node * l2 = a2->getNode(ids2[i]);

    if(verbose) {
      cout << "(" << l1->getId() << " , " << l2->getId() << ")" << endl;
    }

    pair<Node *, Node *> p = getHighest(l1,l2,a1,a2);
    addToList(Ids, p);
  }
}

void ecritEspeces(Node * n, ofstream & Offic_SORTIE_esp, int tslCount)
{
  //Le numéro de l'espèce 
  Offic_SORTIE_esp<<n->getId()<<" ("<<tslCount<<")";
  //Offic_SORTIE_esp<<"\t"<<n->getName();
  //Les descendants de cette espèce
  vector<string> feuilles;
  retourneFeuilles(n,feuilles);
  for(int i=0;i<feuilles.size();i++)
    Offic_SORTIE_esp<<"\t"<<feuilles[i];
  Offic_SORTIE_esp<<"\n";
  
  if (n->getNumberOfSons()==1)
    ecritEspeces(n->getSon(0),Offic_SORTIE_esp,tslCount-1);
  else if (n->getNumberOfSons()==2) {
    ecritEspeces(n->getSon(0),Offic_SORTIE_esp,tslCount-1);
    ecritEspeces(n->getSon(1),Offic_SORTIE_esp,tslCount-1);
  }
}

//num espèce puis nom gène puis liste des descendants
void ecritGenes(Node * n, int num_arbre, ofstream & Offic_SORTIE_genes) {

   convert<int> c;
   //Le numéro de l'espèce est son identifiant dans l'arbre des espèces
   BppString * ESPECE = dynamic_cast <BppString*> (n->getNodeProperty(esp));
   BppString * TYP = dynamic_cast<BppString*> (n->getBranchProperty(typ));
	 
   //Nom si c'est une feuille ou code du gène si c'est un noeud de spéciation
   if(n->getNumberOfSons()==0)
      {
	 if (TYP->toSTL()!=per)
	    {
	       Offic_SORTIE_genes<<c.from_s(ESPECE->toSTL())<<"\t";
	       Offic_SORTIE_genes<<nomGene(n->getName());
	       Offic_SORTIE_genes<<"\n";
	    }
      }
   else
      {
	 if(TYP->toSTL()==spe)
	    {
	       Offic_SORTIE_genes<<c.from_s(ESPECE->toSTL())<<"\t";
	       Offic_SORTIE_genes<<num_arbre<<sep<<n->getId();
	       //Les descendants de ce gène
	       vector<string> feuilles;
	       retourneFeuillesNomsGenes(n,feuilles);
	       for(int i=0;i<feuilles.size();i++)
		  Offic_SORTIE_genes<<"\t"<<feuilles[i];
	       Offic_SORTIE_genes<<"\n";
	    }
      }

   if (n->getNumberOfSons()==1)
      ecritGenes(n->getSon(0),num_arbre,Offic_SORTIE_genes);
   else if (n->getNumberOfSons()==2)
      {
	 ecritGenes(n->getSon(0),num_arbre,Offic_SORTIE_genes);
	 ecritGenes(n->getSon(1),num_arbre,Offic_SORTIE_genes);
      }
}

/*
  write SORTIE file for subdivided gene tree in the fashion similar to
  that of SORTIE file for the subdivided species tree */
void writeGenes(Node* n, int num_arbre, ofstream& Offic_SORTIE_genes) {

   convert<int> c;
   //Le numéro de l'espèce est son identifiant dans l'arbre des espèces
   BppString * ESPECE = dynamic_cast <BppString*> (n->getNodeProperty(esp));
   BppString * TYP = dynamic_cast<BppString*> (n->getBranchProperty(typ));
   BppString * TSL = dynamic_cast<BppString*> (n->getNodeProperty(tsl));
	 
   // leaf node
   if(n->getNumberOfSons() == 0) {
     Offic_SORTIE_genes << num_arbre << "|" << n->getId() << " \t";
     if(TYP->toSTL() != per) { Offic_SORTIE_genes << "(" << n->getName() << ") :\t"; }
     else { Offic_SORTIE_genes << "(loss) :\t"; }
     Offic_SORTIE_genes << c.from_s(ESPECE->toSTL()) << "\t";
     Offic_SORTIE_genes << "(" << c.from_s(TSL->toSTL()) << ")\t";
     Offic_SORTIE_genes<<"\n";
   }
   else { // internal node
     Offic_SORTIE_genes << num_arbre << "|" << n->getId() << " :\t";
     Offic_SORTIE_genes << c.from_s(ESPECE->toSTL()) << "\t";
     Offic_SORTIE_genes << "(" << c.from_s(TSL->toSTL()) << ")\t";
     //Les descendants de ce gène
     vector<string> feuilles;
     retourneFeuillesId(n, feuilles);
     for(int i=0; i<feuilles.size(); i++)
       Offic_SORTIE_genes << "\t" << feuilles[i];
     Offic_SORTIE_genes<<"\n";
   }

   // the recursion
   if(n->getNumberOfSons() == 1) { writeGenes(n->getSon(0), num_arbre, Offic_SORTIE_genes); }
   else if(n->getNumberOfSons() == 2) { // 2 fils
     writeGenes(n->getSon(0), num_arbre, Offic_SORTIE_genes);
     writeGenes(n->getSon(1), num_arbre, Offic_SORTIE_genes);
   }
}

int lectureFicRelBin(string fic,map<int,vector<adjacence> > & miva)
{
   ifstream Iffic;
   string tampon;
   convert<int> c;
   Iffic.open(fic.c_str(), ios::in); 
   if (!Iffic)
      {
	 cout<<endl<<"Impossible d'ouvrir le fichier "<<fic<<endl;
	 return 0;
      } 
   while (!Iffic.eof())
      {
	 //num espèce
	 Iffic>>tampon;
	 int e=c.from_s(tampon);
	 
	 //Pour éviter un dernier tour de boucle inutile à cause d'un blanc en fin de fichier
	 if(!Iffic.eof())
	    {
	       //Les deux gènes adjacents
	       adjacence Adj;
	       Iffic>>tampon;
	       Adj.gene1=tampon;
	       Iffic>>tampon;
	       Adj.gene2=tampon;
	       
	       //On entre ça dans la map 
	       if (miva.find(e)!=miva.end())
		  miva[e].push_back(Adj);
	       else	
		  {
		     vector<adjacence> v;
		     v.push_back(Adj);
		     miva[e]=v;
		  }
	    }  
      }
   return 1;
}

int genereFicTULIPparEspece(string fic_arbre, string NOM_DOSSIER, TreeTemplate<Node> * S, map<int, vector<adjacence> > miva)
{
   convert<int> c;
   string dossier_tulip;  
   if (fic_arbre.find('/')!=-1)
      dossier_tulip=fic_arbre.substr(0,fic_arbre.rfind('/')+1)+NOM_DOSSIER;
   else
      dossier_tulip=NOM_DOSSIER;
   
   string commande="mkdir "+dossier_tulip;
   int ret=system(commande.c_str());
   if(ret<0)
      {
	 cout<<"Problème à la création du répertoire "<<dossier_tulip<<endl;
	 return(0);
      }
   cout<<"Génération des fichiers Tulip par espèce ancestrale dans le sous-dossier : "<<dossier_tulip<<endl;   
   map<int,vector<adjacence> >::iterator it_miva;
   
   for(it_miva=miva.begin();it_miva!=miva.end();it_miva++)
      if ((*it_miva).second.size()!=0)
	 {
	    string nom_fic;
	    if (fic_arbre.find('/')!=-1)
	       nom_fic=fic_arbre.substr(fic_arbre.rfind('/')+1,fic_arbre.size());
	    else
	       nom_fic=fic_arbre;
	    nom_fic+="_";
	    try{
	       string nom=S->getNode((*it_miva).first)->getName();
	       nom_fic+=nom;
	    }
	    catch(exception e){
	       //cout<<e.what();
	       nom_fic+="espece";
	       nom_fic+=c.to_s((*it_miva).first);
	    }
	    nom_fic+=".tlp";
	    string fic_tulip=dossier_tulip+'/'+nom_fic;
	    
	    cout<<"\t"<<fic_tulip;
	    generationFicTulip(fic_tulip,(*it_miva).second);
	    cout<<" ... construit."<<endl;
	 }
   return 1;
}

/*
  subdivides the edge given by (t,father), and assignes id: idCount,
  time: timeSlice and branch: branchProp to the new node created by
  the subdivision (father and son relationships are updated, where t
  is the sonPosition-th son of father) */
void subdiviseBranche(Node* t, Node* father, int& idCount, int timeSlice, string branchProp) {

  int position = father->getSonPosition(t);
  Node* s = new Node(idCount); idCount++;

  convert<int> C;
  s->setNodeProperty(tsl, BppString(C.to_s(timeSlice)));
  s->setBranchProperty(brn, BppString(branchProp));
  s->addSon(0,t);
  //Node* sMark = new Node(0,"");
  //s->addSon(1,sMark);
  father->setSon(position,s);
  //outputNodeInfo(s);
}

/*
  subdivides a tree according to the timeslices assigned (as bootstrap
  values) to the nodes.  Procedure also renames the entire set of id's
  according to a post-order traversal of the new tree (starting at
  idCount) */
void subdiviseArbre(Node* n, int& idCount) {

  // function operates in post-order fashion
  for(int i=0; i< n->getNumberOfSons(); ++i)
    subdiviseArbre(n->getSon(i),idCount);
  
  convert<int> C;

  // stuff about the node
  int nsBS=0; // default to 0 (for a leaf node)
  if(n->hasBranchProperty(bsv))
    nsBS = C.from_s(clToStr(n->getBranchProperty(bsv)));
  /*  if(nsBS==0) {
      n->setBranchProperty(bsv)*/

  int timeSlice = nsBS;
  n->setNodeProperty(tsl, BppString(C.to_s(timeSlice)));

  string branchProp;
  if(nsBS==0)
    branchProp = n->getName();
  else
    branchProp = C.to_s(timeSlice);
  n->setBranchProperty(brn, BppString(branchProp));

  n->setId(idCount);
  ++idCount;
  //outputNodeInfo(n);

  // stuff about the father of the node
  Node* father;
  int fathersBS;
  if(n->hasFather()) {
    //cout<<"father bs: ";
    father = n->getFather();
    if(father->hasBranchProperty(bsv)) {
      fathersBS = C.from_s(clToStr(father->getBranchProperty(bsv)));
      //cout<<fathersBS;
    }
    /*else
      cout<<"NO-BS";
  }
  else {
     cout<<"root node: no father";*/
  }
  //cout<<endl;
    
  // subdivide edge (n,father) k times where k = father's bs - n's bs - 1
  if(n->hasFather()) {
    Node* t = n;
    for(int k=0; k< fathersBS - nsBS -1; ++k) {
      ++timeSlice; // next subdivided node gets the next time stamp
      subdiviseBranche(t, father, idCount, timeSlice, branchProp);
      t = t->getFather();
    }
  }
}

/*
  traverses tree and adds "dummy branch" with id -1 to (subdivided)
  vertices with only one son so that each vertex has exactly 2 sons
  (unless vertex is a leaf, in which case, it does nothing).  It is so
  that tree can be output in many formats like Newick or NHX */
void ajouteBranches(Node* n) {

  int nSons = n->getNumberOfSons();
  if(nSons==1) {
    Node* dummy = new Node(-1,"");
    n->addSon(1,dummy);
  }
  
  for(int i=0; i< nSons; ++i)
    ajouteBranches(n->getSon(i));

  //outputNodeInfo(n);
}

void catFile(string fileName) {

  cout << fileName << " : " << endl;
  cout << "-------------------------------------" << endl;
  ifstream inFile(fileName.c_str(), ios::in);
  char line[256];

  while(inFile.getline(line,256))
    cout << line << endl;
}

void catFile(string fileName, string ORstring) {

  cout << fileName << " : " << endl;
  cout << "-------------------------------------" << endl;
  ifstream inFile(fileName.c_str(), ios::in);
  char line[256];

  cout << "OVERRIDE: tree " << ORstring << endl; // OVERRIDE
  while(inFile.getline(line,256))
    cout << line << endl;
}

/*
  given gene distribution gd (map<int, vector<int> > : <spec, <gene
  gene gene ...> >), we add distribution for gene family fi

  NOTE : wrong implementation

*/
void addGeneDistr(map<int, vector<int> > & gd, TreeTemplate<Node> * G, int fi) {
  
  vector<Node *> leaves;
  vector<Node *>::iterator il;
  leaves = G->getLeaves();

  for(il = leaves.begin(); il != leaves.end(); ++il) {
    Node * n = *il;
    int spec = 0;
    while(1) {
      if(Esp(n) != spec) { // so that we only put fi once for spec
	spec = Esp(n);
	// put fi to distr[spec]
	if(gd.find(spec) != gd.end()) gd[spec].push_back(fi);
	else { vector<int> v; v.push_back(fi); gd[spec] = v; }
      }
      if(n == G->getRootNode()) break;
      n = n->getFather();
    }
  }
}

// true if E(n) is a transfer, transferback or a transferout
bool isTrTBTOut(Node * n) {

  string e = E(n);
  if((e == trn) || (e == tb) || (e == to)) return true;
  return false;
}

/*
  given gene distribution gd (map<int, vector<int> > : <spec, <gene
  gene gene ...> >), we add distribution for gene family fi

  NOTE : another wrong implementation

*/
void addGeneDistr(map<int, vector<int> > & gd, Node * n, int fi) {
  
  for(int i=0; i< n->getNumberOfSons(); ++i)
    addGeneDistr(gd, n->getSon(i), fi);

  if(maxForEsp(n)) { // only if n is maximal for its specie
    int spec = Esp(n);
    // put fi to distr<spec>
    if(gd.find(spec) != gd.end()) gd[spec].push_back(fi);
    else { vector<int> v; v.push_back(fi); gd[spec] = v; }
    // put it again if n is a trn, tb or to node
    if(isTrTBTOut(n)) gd[spec].push_back(fi);
  }
}

/*
  given gene distribution gd (map<string, vector<int> > : <spec, <gene
  gene gene ...> >), we add distribution for gene family fi

  Note that n will be the root of a *non-subdivided* gene tree
  
*/
void addGeneDistr(map<string, vector<int> > & gd, vector<string> an, Node * n, int fi) {

  for(int i=0; i< n->getNumberOfSons(); ++i)
    addGeneDistr(gd, an, n->getSon(i), fi);
  
  vector<string> events;
  vector<string>::iterator it;

  if(n->isLeaf()) {
    string name = n->getName();
    //cout << "name : " << name << endl;
    string specie = name.substr(0, name.find(part1)); // specie
    //cout << "specie : " << specie << endl;
    parseLeafEvents(events, name, specie);
    // push specie
    //cout << "push : " << specie << endl;
    if(gd.find(specie) != gd.end()) gd[specie].push_back(fi);
    else { vector<int> v; v.push_back(fi); gd[specie] = v; }
  }
  else {
    string bootie = clToStr(n->getBranchProperty(bsv));
    //cout << "bootie : " << bootie << endl;
    parseInternalNodeEvents(events, bootie);
  }

  //cout << "events : ";
  //afficheVector(events);
  // push events (if they are speciation nodes)
  for(it=events.begin(); it!=events.end(); ++it) {
    string e = *it;
    if(e.substr(0,1) == ".") e = e.substr(1,e.length());
    if(findSinV(an,e)) {
      //cout << "push : " << e << endl << endl;
      if(gd.find(e) != gd.end()) gd[e].push_back(fi);
      else { vector<int> v; v.push_back(fi); gd[e] = v; }
    }
  }
}

/*
  write gene distribution gd to file

*/
void writeDistr(map<int, vector<int> > gd, ofstream & of) {

  map<int, vector<int> >::iterator i;
  for(i=gd.begin(); i!= gd.end(); ++i) {
    of << (*i).first << " :";
    vector<int> v = (*i).second;
    vector<int>::iterator j;
    for(j=v.begin(); j!=v.end(); ++j)
      of << " " << *j;
    of << endl;
  }
}

/*
  write gene distribution gd to file

*/
void writeDistr(map<string, vector<int> > gd, ofstream & of) {

  map<string, vector<int> >::iterator i;
  for(i=gd.begin(); i!= gd.end(); ++i) {
    of << (*i).first << " :";
    vector<int> v = (*i).second;
    vector<int>::iterator j;
    for(j=v.begin(); j!=v.end(); ++j)
      of << " " << *j;
    of << endl;
  }
}

// true if n is in ns (in killLeaves context)
bool inNodes(vector<Node *> ns, Node * n, string & o_name) {

  vector<Node *>::iterator it;
  for(it=ns.begin(); it!=ns.end(); ++it) {
    o_name = (*it)->getName();
    string name = n->getName();
    string n_name = o_name.substr(0, o_name.find(part1));
    if(n_name == name) return true;
  }
  return false;
}

// climb leaf to bifurcation point p and set loss just before p
void pruneLeaf(Node * l) {

  Node * f = l->getFather();
  while(f->getNumberOfSons() < 2) { // while f is a null node
    l = f;
    f = l->getFather();
  }
  l->setBranchProperty(typ,BppString(per));
}

/*
  given template gene tree Gc, use leaves to prune the leaves of Gc,
  and then return this pruned tree

*/
TreeTemplate<Node> * pruneLeaves(vector<Node *> leaves, TreeTemplate<Node> * Gc) {

  TreeTemplate<Node> T = *Gc;
  TreeTemplate<Node> * G = new TreeTemplate<Node>();
  G = T.clone();
  vector<Node *> f_leaves = G->getLeaves();
  vector<Node *>::iterator it;
  for(it=f_leaves.begin(); it!=f_leaves.end(); ++it) {
    string o_name;
    if(!inNodes(leaves, *it, o_name)) pruneLeaf(*it);
    else (*it)->setName(o_name);
  }
  return G;
}

/************************************************************************/

/*
  MAIN
*/
int main(int argc, char* argv[]) {

  cout << "\n***************************************************" << endl;
  cout << "ATTENTION : this output file is very long when the number of gene trees is large,\nso the principal steps that the program takes are numbered <1>, <2>, ...\nso that one can jump quickly to these tags with one's favourite editor\n\nThe principal steps are as follows :\n" << endl;
  cout << "<1> conf file" << endl;
  cout << "<2> genes file, species tree" << endl;
  cout << "<3> gene trees" << endl;
  cout << "<4> correspondence between adjacencies and gene trees" << endl;
  cout << "<5> correspondence between genes and species" << endl;
  cout << "<6> adjacencies, and equivalence classes" << endl;
  cout << "<7> refinement of the equivalence classes" << endl;
  cout << "...\n\n***************************************************" << endl;

  cout << "\nBEGIN PROGRAM ...\n\n***************************************************\n<1> conf file\n***************************************************\n" << endl;

  srand(time(NULL)); // initialisation de rand

  cout << "----------------------- conf file -----------------------" << endl;
  catFile(argv[1]);
  cout << "--------------------- end conf file ---------------------" << endl << endl;

  string fic_arbre, fic_gene, fic_especes, fic_adjacence;
  if(!lireFicConfig(fic_arbre, fic_gene, fic_especes, fic_adjacence, argc, argv)) {
    cout << "error : could not read the files listed in conf file" << endl; exit(0);
  }

  cout << endl << "***************************************************" << endl;
  cout << "<2> genes file, species tree" << endl;
  cout << "***************************************************" << endl << endl;

  ifstream IfficGene(fic_gene.c_str(), ios::in);
  if(!IfficGene)
    cout << endl << "Impossible d'ouvrir le fichier : " << fic_gene << endl;
  cout << "Fichier " << fic_gene << " ouvert" << endl;
   
  string espece="";
  string gene="";
  while(!IfficGene.eof()) {
    IfficGene >> espece;
    IfficGene >> gene;
    genes_especes[gene]=espece;
  }
  cout << endl << "Correspondance gène/espèce construite." << endl;
  IfficGene.close();

  /**
  cout << "\n\n\n************************" << endl;
  cout << endl << "genes/especes correspondence : " << endl << endl;
  afficheMap(genes_especes);
  cout << endl << endl;
  /**/

  //Arbres des espèces
  Newick* newickReaderEsp = new Newick(true,true);
  newickReaderEsp->enableExtendedBootstrapProperty(bsv);
  TreeTemplate<Node>* S = newickReaderEsp->read(fic_especes);
  //Les noeuds internes sont lus comme des propriétés de branches
  //La f° ci-dessous permet de nommer les noeud internes et de faire passer la propriété au noeud

  /**
  cout<<"Affichage de tous les Id des noeuds de S :"<<endl;
  afficheInfoNoeuds(S->getRootNode());
  cout<<endl<<"*************************************************************************"<<endl<<endl;
  /**/

  // if it's in this newick format modified for transfers, we need to subdivide S to get an ultrametric tree
  if(INPUT_FORMAT==2 || INPUT_FORMAT == 3) {
    // start from id 1 (we use id 0 for a "don't care" node)
    int idCount = 1;
    subdiviseArbre(S->getRootNode(), idCount);
  }

  /**
  cout<<"Affichage de tous les Id des noeuds de S :"<<endl;
  afficheInfoNoeuds(S->getRootNode());
  cout<<endl<<"*************************************************************************"<<endl<<endl;
  /**/

  nommerNoeudsInternes(S->getRootNode());

  /**
  cout<<"Affichage de tous les Id des noeuds de S :"<<endl;
  afficheInfoNoeuds(S->getRootNode());
  cout<<endl<<"*************************************************************************"<<endl<<endl;
  /**/
 
  //Création du fichier de sortie de correspondance des espèces
  if(!fic_SORTIE_EspeceDone) {
    string fic_SORTIE_espece = fic_arbre + "_SORTIE_esp";
    cout << "Création du fichier de SORTIE pour la correspondance des espèces: " << fic_SORTIE_espece << endl;
    ofstream Offic_SORTIE_esp;
    Offic_SORTIE_esp.open(fic_SORTIE_espece.c_str(), ios::out);
    ecritEspeces(S->getRootNode(), Offic_SORTIE_esp, 35);
    Offic_SORTIE_esp.close();
  }
  
  //Création du fichier de correspondance des espèces subdivisé
  if(INPUT_FORMAT==2 || INPUT_FORMAT == 3) {
    string fic_SUBDIVISE_espece = fic_arbre + "_SUBDIVISE_esp";
    cout << "Création du fichier de SUBDIVISE pour la correspondance des espèces: " << fic_SUBDIVISE_espece << endl;
    // *** IMPORTANT: add "dummy" branches to A COPY OF tree so that it is in (1 father 2 sons) format for output
    TreeTemplate<Node> outS = *S;
    ajouteBranches(outS.getRootNode());

    if(OUTPUT_FORMAT==0) {
      Newick* newickWriter = new Newick();
      newickWriter->enableExtendedBootstrapProperty(bsv);
      Tree* Sp = dynamic_cast <Tree*> (&outS);
      newickWriter->write(*Sp, fic_SUBDIVISE_espece.c_str(), true);
    }
    else if(OUTPUT_FORMAT==1) { // *** NOTE: this mode never tested ***
      Nhx* nhxWriter = new Nhx(true);
      nhxWriter->registerProperty(Nhx::Property("Duplication", "D", true, 0));
      Tree* Sp = dynamic_cast <Tree*> (&outS);
      nhxWriter->write(*Sp, fic_SUBDIVISE_espece.c_str(), true);
    }
  }
  cout << "Traitement de l'arbre des espèces terminé" << endl;
  
  /**/
  cout << endl << "********************************************" << endl;
  cout << endl << "Affichage de tous les Id des noeuds de S :\n" << endl;
  afficheInfoNoeuds(S->getRootNode());
  cout << endl;
  /**/

  map<int,int> especes_ancestrales_nb_gene;
  map<int,int> especes_actuelles_nb_gene;
  map<int,int> especes_nb_pertes;
  vector<Tree*> Arbres;
  vector<Tree*>::iterator it;

  convert<int> c;
  map<string, vector<int> > GeneDistr; // copy number of genes at each spec
  vector<string> Ancestors; // for a species tree with ancestors 1..35
  for(int i=1; i< 36; ++i)
    Ancestors.push_back(c.to_s(i));

  cout << endl << "***************************************************" << endl;
  cout << "<3> gene trees" << endl;
  cout << "***************************************************" << endl << endl;

  if(INPUT_FORMAT==0) {
    cout << "input in Newick format" << endl;
    Newick* newickReader = new Newick(true,true);
    newickReader->enableExtendedBootstrapProperty(bsv);
    newickReader->read(fic_arbre.c_str(), Arbres);
    if(ArbresRec) {
      //Écrire une fonction qui relie notre format newick !!! 
      //et qui permet de réobtenir toutes les informations de la réconciliation
      for(it=Arbres.begin(); it!=Arbres.end(); it++) {
	//cout<<"\n\n****\nDébut traitement arbre "<<endl;
	
	Tree* Gp=*it;
	TreeTemplate<Node> *G = dynamic_cast <TreeTemplate<Node>*> (Gp);

	/**
	cout << "Affichage de tous les Id des noeuds de G:" << endl;
	afficheInfoNoeuds(G->getRootNode());
	cout << "********************************************" << endl;
	/**/

	recupInfoSurBrancheNoeuds(G->getRootNode(),S);

	/**
	cout << "Affichage de tous les Id des noeuds de G:" << endl;
	afficheInfoNoeuds(G->getRootNode());
	cout << "********************************************" << endl;
	/**/

	//Pour remettre à jour d'identifiant
	// NOTE: never needed here, actually
	affecteInfoSurBrancheNoeuds(G->getRootNode());
	
	/**
	cout << "Affichage de tous les Id des noeuds de G:" << endl;
	afficheInfoNoeuds(G->getRootNode());
	cout << "********************************************" << endl;
	/**/
      }
    }
  }
  else if(INPUT_FORMAT==1) { // NOTE: mode never tested in the light of subdivisions
    cout << "Je m'apprête à lire le fichier "<< fic_arbre.c_str() << " au format nhx." << endl;
    Nhx* nhxReader = new Nhx(true);
    nhxReader->read(fic_arbre.c_str(), Arbres);
  }
  else if(INPUT_FORMAT==2) { // newick format modified for transfers
    cout << "input in Newick format modified for transfers" << endl;
    Newick* newickReader = new Newick(true,true);
    newickReader->enableExtendedBootstrapProperty(bsv);
    newickReader->read(fic_arbre.c_str(), Arbres);

    /*
      for this input format, the assumption is that trees are already
      reconcilied, in the format of the trees given by Gergely, i.e.,
      Arbresrec==true (in fact, we ignore this 'Arbesrec' switch).
      Later we will make (LCA) reconciliation a module */
    int count = 0; // tree count
    int idCount = 0; // node id count
    for(it=Arbres.begin(); it!=Arbres.end(); it++) {
      cout << endl << "gene tree " << count << endl;// << "***************************************************" << endl << endl;

      Tree* Gp=*it;
      TreeTemplate<Node> *G = dynamic_cast <TreeTemplate<Node> *> (Gp);
      
      // before subdividing the trees, we collect the gene distribution info
      cout << endl << "add gene distribution for family " << count << " ..." << endl;
      addGeneDistr(GeneDistr, Ancestors, G->getRootNode(), count); // add gene family count to GeneDistr

      // recup the reconciliation (subdivide tree, add losses, etc.)
      cout << endl << "recup gene tree with transfers ..." << endl;
      recupGeneTreeWithTransfers(G->getRootNode(), S, idCount, count);

      //int idCount = 1;
      // renumber the nodes (possibly Severine's renumeroteid is sufficient

      /**/
      cout << endl << "********************************************" << endl;
      cout << endl << "Affichage de tous les Id des noeuds de G :\n" << endl;
      afficheInfoNoeuds(G->getRootNode());
      /**/

      ++count;

      // I define the rest later ...
    }
  }
  else if(INPUT_FORMAT == 3) { // in order to support a null hypothesis

    cout << "supporting null hypothesis ... generate copy Gc of S ..." << endl;

    // we want to create a gene tree that is a copy of S ...
    Newick* newickReaderGc = new Newick(true,true);
    newickReaderGc->enableExtendedBootstrapProperty(bsv);
    string fic_gc = "gt-template";
    TreeTemplate<Node> * Gc = newickReaderGc->read(fic_gc);

    // recup tree as normal ...
    int idCount = 1000; // start it off at 1000 (for debugging ... we will renumber all nodes after this procedure anyway)
    recupGeneTreeWithTransfers(Gc->getRootNode(), S, idCount, 0);
    
    cout << endl << "********************************************" << endl;
    cout << endl << "Affichage de tous les Id des noeuds de Gc:" << endl;
    afficheInfoNoeuds(Gc->getRootNode());
    // done ...

    Newick* newickReader = new Newick(true,true);
    newickReader->enableExtendedBootstrapProperty(bsv);
    newickReader->read(fic_arbre.c_str(), Arbres);

    int count = 0;
    for(it=Arbres.begin(); it!=Arbres.end(); it++) {
      cout << endl << "gene tree " << count << endl;// << "***************************************************" << endl << endl;

      Tree* Gp=*it;
      TreeTemplate<Node> *G = dynamic_cast <TreeTemplate<Node> *> (Gp);
      
      G = pruneLeaves(G->getLeaves(), Gc);
      *it = G;

      /**/
      cout << endl << "********************************************" << endl;
      cout << endl << "Affichage de tous les Id des noeuds de G:" << endl;
      afficheInfoNoeuds(G->getRootNode());
      /**/

      ++count;

      // I define the rest later ...
    }
  }
  else {
    cout << "Erreur : type de INPUT_FORMAT non reconnu" << endl;
  }

  int nb_arbres = Arbres.size();
  cout << "\n********************************************\nLes " << nb_arbres << " arbres du fichier " << fic_arbre << " sont chargés en mémoire\n********************************************\n" << endl;
  
  string fic_geneDistr = fic_arbre+"_spec-gene_distr";
  ofstream OfficGD;
  OfficGD.open(fic_geneDistr.c_str(), ios::out);
  writeDistr(GeneDistr, OfficGD);

  /*
  ofstream sd_log("gt-subdiv.log", ios::app); // log a successful run for the subdivision of the tree
  sd_log << argv[2] << endl;
  sd_log.close();
  */

  // end of the subdivision of the gene trees
  //////////////////////////////////////////////////

  /*
    now we "clean" the leaves of the gene tree (getting rid of the
    annotations added to the end of the name, like "@0|SYNPX" for
    example) */
  if(INPUT_FORMAT == 2 || INPUT_FORMAT == 3) {
    for(it=Arbres.begin(); it!=Arbres.end(); it++) {
      Tree* Gp=*it;
      TreeTemplate<Node> *G = dynamic_cast <TreeTemplate<Node> *> (Gp);		            
      cleanTree(G->getRootNode());
    }
  }    

  int i=1;
  
/*** IGNORE *** ignore this for the time being: later we will move the reconciliation of trees to a module **/
//#define IGNORE_OFF // uncomment if we want to run original routine
#ifdef IGNORE_OFF

   if (!ArbresRec)
      {	      
	 
	 for(it=Arbres.begin();it!=Arbres.end();it++)
	    {
	       //cout<<"Début traitement arbre "<<i<<endl;
	       Tree * Gp=*it;
	       TreeTemplate<Node> *G = dynamic_cast <TreeTemplate<Node> *> (Gp);
	       //cout<<"cast réussi"<<endl;
	       G->setBranchLengths(1);
	       //cout<<"lg de branches à 1"<<endl;
	       //À VÉRIFIER QUE TOUS LES NOEUDS SONT DE DEGRÈs 2 AUSSI !!!
	       //cout<<"G est multifurcating ? "<<TreeTemplateTools::isMultifurcating(*(G->getRootNode()))<<endl;
	       //cout<<"nb de fils à la racine de G ? "<<G->getRootNode()->getNumberOfSons()<<endl;
	       if (! TreeTemplateTools::isMultifurcating(*(G->getRootNode())))
		  {
		    /**
		    cout<<"Affichage de tous les Id des noeuds de G:"<<endl;
		    afficheInfoNoeuds(G->getRootNode());		     
		    cout << "********************************************" << endl;
		    /**/

		     reetiquette(G->getRootNode());
		     cout<<"ré-étiquette réussi"<<endl;

		     /**/
		    cout<<"Affichage de tous les Id des noeuds de G:"<<endl;
		    afficheInfoNoeuds(G->getRootNode());		     
		    cout << "********************************************" << endl;
		    /**/

		     reconcil(S,G);
		      cout<<"réconcil réussi"<<endl;
		     // afficheInfoNoeuds(G->getRootNode());   

		    /**/
		    cout<<"Affichage de tous les Id des noeuds de G:"<<endl;
		    afficheInfoNoeuds(G->getRootNode());		     
		    cout << "********************************************" << endl;
		    /**/
		     
		     //Renumérote les id des noeuds pour qu'il n'y ait pas de différence pour un même
		     //jeu de données AVEC ou SANS étape de réconciliation :
		     G->resetNodesId();
		      cout<<"reset réussi"<<endl;
		     // afficheInfoNoeuds(G->getRootNode());  

		    /**
		    cout<<"Affichage de tous les Id des noeuds de G:"<<endl;
		    afficheInfoNoeuds(G->getRootNode());		     
		    cout << "********************************************" << endl;
		    /**/

		     affecteInfoSurBrancheNoeuds(G->getRootNode());

		    /**
		    cout<<"Affichage de tous les Id des noeuds de G:"<<endl;
		    afficheInfoNoeuds(G->getRootNode());		     
		    cout << "********************************************" << endl;
		    /**/

		  }
	       if (TreeTemplateTools::isMultifurcating(*(G->getRootNode())) || G->getRootNode()->getNumberOfSons()==0)//aucune espèce dans notre arbre d'espèces
		  {
		     it=Arbres.erase(it);  
		     it--;
		     if(TreeTemplateTools::isMultifurcating(*(G->getRootNode())))
			cout<<"\t Arbre "<<i<<" traité NON BINAIRE !!! => EFFACÉ."<<endl;
		     else
			cout<<"\t Arbre "<<i<<" traité et EFFACÉ."<<endl;
		  }
	       else
		  {
		     cout<<"\t Arbre "<<i<<" traité."<<endl;
		  }
	       i++;
	       afficheInfoNoeuds(G->getRootNode());  
	    }
	 nb_arbres=Arbres.size();
	 cout<<"\n\n\n\n\n\nRéannoatation terminée : "<<nb_arbres<<" arbres retenus. \n\n\n"<<endl;
	 
	 string fic_arbre2=fic_arbre+"_reconcil";
	 cout<<"\nÉcriture du fichier d'arbres réconciliés"<<endl;
	 if(OUTPUT_FORMAT==0)
	    {
	       Newick * newickWriter = new Newick(); 
	       newickWriter->enableExtendedBootstrapProperty(esp);
	       newickWriter->write(Arbres,fic_arbre2.c_str());
	    }
	 else if(OUTPUT_FORMAT==1)
	    {
	       Nhx * nhxWriter = new Nhx(true);
	       nhxWriter->registerProperty(Nhx::Property("Duplication", "D", true, 0)); 
	       nhxWriter->write(Arbres,fic_arbre2.c_str());
	    }
	 else
	    {
	       cout<<"Erreur : type de OUTPUT_FORMAT non reconnu"<<endl;
	    }
      }
   else
      {
   	 cout<<"Les arbres sont déjà réconciliés (booléen ReconcilDone à true dans le fichier de configuration)."<<endl;
	 //On retient les arbres qu'on veut re-tester
	 // vector<Tree *> ArbresBizares; 
	 // int j=0;
	 // for(it=Arbres.begin();it!=Arbres.end();it++)
	 //    {
	 //  if(j==1014
	 // 	  ||j==1040
	 // 	  ||j==1043
	 // 	  ||j==1046
	 // 	  ||j==1047
	 // 	  ||j==1060
	 // 	  ||j==1079
	 // 	  ||j==107
	 // 	  ||j==108
	 // 	  ||j==1098
	 // 	  ||j==1099
	 // 	  ||j==1103
	 // 	  ||j==1106
	 // 	  ||j==33325
	 // 	  ||j==3645
	 // 	  ||j==18659
	 // 	  ||j==33311
	 // 	  ||j==1097
	 // 	  ||j==16310
	 // 	  ||j==23631
	 // 	  ||j==12231
	 // 	  ||j==7279
	 // 	  ||j==7281
	 // 	  ||j==17494
	 // 	  ||j==2914
	 // 	  ||j==29761
	 // 	  ||j==1605
	 // 	  ||j==1686
	 // 	  ||j==25904
	 // 	  ||j==16872
	 // 	  ||j==12985)
	 // 	  ArbresBizares.push_back(*it);
	 //       j++;
	 //    }
	 
	 // string fic_arbresBIZ=fic_arbre+"_bizares";
	 // cout<<"\nÉcriture du fichier d'arbres réconciliés bizares"<<endl;
	 // if(OUTPUT_FORMAT==0)
	 //    {
	 //       Newick * newickWriter = new Newick(); 
	 //       newickWriter->enableExtendedBootstrapProperty(esp);
	 //       newickWriter->write(ArbresBizares,fic_arbresBIZ.c_str());
	 //    }
	 // else if(OUTPUT_FORMAT==1)
	 //    {
	 //       Nhx * nhxWriter = new Nhx(true);
	 //       nhxWriter->registerProperty(Nhx::Property("Duplication", "D", true, 0)); 
	 //       nhxWriter->write(ArbresBizares,fic_arbresBIZ.c_str());
	 //    }
	 // else
	 //    {
	 //       cout<<"Erreur : type de OUTPUT_FORMAT non reconnu"<<endl;
	 //    }
      }
   //   */

#endif // end #IGNORE_OFF
/*** END IGNORE *** ignore this for the time being: later we will move the reconciliation of trees to a module **/

   //STATS
   //On compte les noeuds de duplication, de spéciation et autres stats
   string fic_SORTIE_genes=fic_arbre+"_SORTIE_genes";
   ofstream Offic_SORTIE_genes;
   if(!fic_SORTIE_GeneDone) {
     cout << "Création du fichier de SORTIE pour la correspondance des gènes " << fic_SORTIE_genes << endl;
     Offic_SORTIE_genes.open(fic_SORTIE_genes.c_str(), ios::out); 
   }
   
   i=0;
   for(it=Arbres.begin(); it!=Arbres.end(); it++) {
     Tree * Gp=*it;
     TreeTemplate<Node> *G = dynamic_cast <TreeTemplate<Node> *> (Gp);

     nb_GDup_arbres += countNodesWithBranchProperty(G->getRootNode(), typ, dupl);
     nb_Spe_arbres += countNodesWithBranchPropertyBySpecies(G->getRootNode(), typ, spe, especes_ancestrales_nb_gene);
     nb_Extant_arbres += countNodesWithBranchPropertyBySpecies(G->getRootNode(), typ, ga, especes_actuelles_nb_gene);
     nb_Per_arbres += countNodesWithBranchPropertyBySpecies(G->getRootNode(), typ, per, especes_nb_pertes);
     nb_Trans_arbres += countNodesWithBranchProperty(G->getRootNode(), typ, trn);
     nb_Tb_arbres += countNodesWithBranchProperty(G->getRootNode(), typ, tb);

     // Création du fichier de sortie de correspondance des gènes
     if(!fic_SORTIE_GeneDone)
       cout << "---\ntree : " << i << "\n---" << endl;
       writeGenes(G->getRootNode(), i, Offic_SORTIE_genes);
     i++;

     /*
     // Aussi : Création du fichier de correspondance des gènes subdivisé
     if(INPUT_FORMAT==2) {
       string fic_SUBDIVISE_gene = fic_arbre + "_SUBDIVISE_gene_" + c.to_s(i);
       cout << "Création du fichier de SUBDIVISE pour la correspondance des gènes: " << fic_SUBDIVISE_gene << endl;
    // *** IMPORTANT: add "dummy" branches to A COPY OF tree so that it is in (1 father 2 sons) format for output
       TreeTemplate<Node> outG = *G;
       ajouteBranches(outG.getRootNode());

       if(OUTPUT_FORMAT==0) {
	 Newick* newickWriter = new Newick();
	 newickWriter->enableExtendedBootstrapProperty(bsv);
	 Tree* Ge = dynamic_cast <Tree*> (&outG);
	 newickWriter->write(*Ge, fic_SUBDIVISE_gene.c_str(), true);
       }
       else if(OUTPUT_FORMAT==1) { // *** NOTE: this mode never tested ***
	 Nhx* nhxWriter = new Nhx(true);
	 nhxWriter->registerProperty(Nhx::Property("Duplication", "D", true, 0));
	 Tree* Ge = dynamic_cast <Tree*> (&outG);
	 nhxWriter->write(*Ge, fic_SUBDIVISE_gene.c_str(), true);
       }
     }
     */
   }

   Offic_SORTIE_genes.close();

   /*
   cout << "there are :" << endl << "\t" << nb_GDup_arbres << " duplications " << endl;
   cout << "\t" << nb_Spe_arbres << " speciations" << endl;
   cout << "\t" << nb_Extant_arbres << " extant leaves" << endl;
   cout << "\t" << nb_Per_arbres << " loss nodes" << endl;
   cout << "\t" << nb_Trans_arbres << " transfers" << endl;
   cout << "\t" << nb_Tb_arbres << " transfers back" << endl << endl;
   */

   cout << endl << "***************************************************" << endl;
   cout << "<4> correspondence between genes and gene trees" << endl;
   cout << "***************************************************" << endl << endl;

   // A/ Correspondance entre adjacences et arbres de gène : création des classes 
   //      1) Parcours des arbres pour construire la correspondance g_x apparaît dans arbres y, z, t, ...
   //
   // i est le numéro des arbres
   // une map genes_arbres sert à stocker l'info
   map<string,vector<int> > genes_arbres;
   i=0;
   vector<string> noms_feuilles;
   set<string> GENES;
   vector<string>::iterator it_feuilles;
   string nom_gene;
   string nom_gene1;
   string nom_gene2;
   for(it=Arbres.begin(); it!=Arbres.end(); it++) {
     noms_feuilles=(*it)->getLeavesNames();
     //cout<<"On traite l'arbre "<<i<<endl;     
     for(it_feuilles=noms_feuilles.begin(); it_feuilles!=noms_feuilles.end(); it_feuilles++) {
       //nom_gene=(*it_feuilles).substr(0,(*it_feuilles).rfind(sep,(*it_feuilles).length()-1));
       nom_gene = *it_feuilles;
       if(nom_gene!="Perte") {
	 //Je relève les noms de feuilles :
	 GENES.insert(nom_gene);
	 //cout<<"On traite le gène "<<nom_gene<<endl;
	 //vérif que ce noms de feuilles est bien dans genes_especes
	 if(genes_especes.find(nom_gene)!=genes_especes.end()) {
	   //cout<<nom_gene<<" est bien un gène de genes_especes"<<endl;
	   if(genes_arbres.find(nom_gene)!=genes_arbres.end()) {
	     //cout<<nom_gene<<" est déjà dans genes_arbres"<<endl;
	     genes_arbres[nom_gene].push_back(i);
	   }
	   else	{
	     //cout<<nom_gene<<" n'est PAS dans genes_arbres"<<endl;
	     vector<int> v;
	     v.push_back(i);
	     genes_arbres[nom_gene]=v;
	   }
	 }
	 else
	   cout<<"ATTENTION "<<nom_gene<<" n'est PAS un gène de genes_especes"<<endl;
       }
     }
     i++;
   }
   
   //Affichage
   cout<<"\n\n\n***************************************"<<endl;
   afficheMap(genes_arbres);
   
   cout << endl << "***************************************************" << endl;
   cout << "<5> correspondence between genes and species" << endl;
   cout << "***************************************************" << endl << endl;

   //Épurage de la map genes_especes pour ne garder que les gènes présents dans nos arbres
   cout<<"\n\n\n***************************************"<<endl;
   cout<<"Taille de la map genes_especes avant épuration : "<<genes_especes.size()<<endl;
   cout<<"Taille de la map genes_arbres : "<<genes_arbres.size()<<endl;
   cout<<"Nombres de feuilles distinctes des arbres de gènes vus "<<GENES.size()<<endl;

   //cout<<"\n\n\n***************************************"<<endl;
   //afficheMap(genes_especes);
   map<string,string>::iterator it_ge;
   map<string,string>::iterator it_ge_prec;
   int elim=0;
   int gard=0;
   for(it_ge=genes_especes.begin(); it_ge!=genes_especes.end(); it_ge++) {
     //cout<<"Je traite le gène "<<(*it_ge).first;
     if(genes_arbres.find((*it_ge).first)==genes_arbres.end()) {
       if(it_ge!=genes_especes.begin()) {
	 it_ge_prec=it_ge;//dur 
	 it_ge_prec--; //dur !!
       }
       else { it_ge_prec=genes_especes.begin(); } //et re-dur !!!
       genes_especes.erase(it_ge);
       it_ge=it_ge_prec;
       //cout<<", je le jette"<<endl;
       elim++;
     }
     else {
       //cout<<", je le garde"<<endl;
       gard++;
     }
   }
   //cout<<"\n\n\n ***************************************"<<endl;
   cout<<"Taille de la map genes_especes après épuration : "<<genes_especes.size()<<endl;
   cout<<"Gardés : "<<gard<<" + eliminés : "<<elim<<endl;
   cout<<"\n\n\n***************************************"<<endl;
   afficheMap(genes_especes);
   cout << endl << endl;

   cout << endl << "***************************************************" << endl;
   cout << "<6> adjacencies, and equivalence classes" << endl;
   cout << "***************************************************" << endl << endl;

   //2) Parcours des adjacences pour construire les classes
   //NB : si un ou les deux gènes de l'adjacence apparaît dans plusieurs arbres, alors l'adjacence apparaît dans plusieurs classes

   // j est le numéro des adjacences, on les numérote à 0
   // une map classes_adjacences sert à stocker l'info
   map<string,vector<int> > classes_adjacences;
   map<string,vector<int> > classes_adjacences_SUB;
   vector<string> classes_adjacences_A_SUPPR;
   int j=0;
   vector<adjacence> adjacences;
   
   ifstream IfficAdj (fic_adjacence.c_str(), ios::in);
   if (!IfficAdj) { cout<<endl<<"Impossible d'ouvrir le fichier : "<<fic_adjacence<<endl; }
   cout<<"Fichier "<<fic_adjacence<<" ouvert"<<endl << endl;

   string gene1="";
   string gene2="";
   vector<int> ab1,ab2;
   vector<int>::iterator it_ab1,it_ab2;
   adjacence adj;
   string tampon;
   string nom_classe;
   while ( ! IfficAdj.eof())
      {
   	 //on lit la ligne en entier;
   	 IfficAdj>>gene1;
   	 //Pour éviter un dernier tour de trop à cause d'un blanc en fin de fichier
	 if( ! IfficAdj.eof())
	    {
	       IfficAdj>>gene2;
	       //Selon le format du fichier adjacence !!!
	       // IfficAdj>>tampon;
	       // IfficAdj>>tampon;
	       
	       //cout << "On traite l'adjacence " << gene1 << " - " << gene2 << endl;
	       //on crée l'adjacence si les deux gènes sont dans genes_especes (c-à-d dans nos arbres)
	       if (genes_especes.find(gene1)!=genes_especes.end()
		   && genes_especes.find(gene2)!=genes_especes.end())
		  {
		     //cout<<", on la nomme "<<j<<endl;
		     adj.gene1=gene1;
		     adj.gene2=gene2;
		     //on la stocke
		     adjacences.push_back(adj);
		     //on s'intéresse aux classes :
		     ab1=genes_arbres[gene1];
		     ab2=genes_arbres[gene2];
		     // cout<<"ab1 :";
		     // for(it_ab1=ab1.begin();it_ab1!=ab1.end();it_ab1++)
		     // 	cout<<" "<<*it_ab1;
		     // cout<<endl;
		     // cout<<"ab2 :";
		     // for(it_ab2=ab2.begin();it_ab2!=ab2.end();it_ab2++)
		     // 	cout<<" "<<*it_ab2;
		     // cout<<endl;
		     
		     for(it_ab1=ab1.begin();it_ab1!=ab1.end();it_ab1++)
			for(it_ab2=ab2.begin();it_ab2!=ab2.end();it_ab2++)
			   {
			      //cout<<"(*it_ab1)="<<(*it_ab1)<<" et (*it_ab2)="<<(*it_ab2)<<endl;
			      //
			      //
			      //Attention tous les adjacences des classes entre même arbre 
			      //ne peuvent pas forcément être traitées ensemble !!
			      //Il faut qu'elles respectent même noeud ancêtre commun des extrémités !!!
			      //Nom de la classe x|x|idLCA_dans_x
			      //ATTENTION BIS idem pour les classes entre arbres de numéros différents
			      //on les raffinera plus bas car il faut les trier en les comparant !!
			      if((*it_ab2)==(*it_ab1))
			      	 {
				   /*
				     Note : before we did it this way, when we didn't have transfers ...

				    TreeTemplate<Node> *A=dynamic_cast <TreeTemplate<Node> *> (Arbres[(*it_ab1)]);
				    TreeTemplate<Node> Arbre=*A;
				    vector< int > Id_Genes;
			      	    //string g1=gene1+sep+genes_especes[gene1];
				    //cout<<"g1="<<g1<<" d'id "<<Arbre.getLeafId(g1)<<endl;
			      	    Id_Genes.push_back(Arbre.getLeafId(gene1));
			      	    //string g2=gene2+sep+genes_especes[gene2];
			      	    //cout<<"g2="<<g2<<" d'id "<<Arbre.getLeafId(g2)<<endl;
			      	    Id_Genes.push_back(Arbre.getLeafId(gene2));
				    //afficheInfoNoeuds(Arbre.getRootNode());
			      	    //cout<<"Je cherche le lca des 2 gènes"<<endl;
			      	    int id_ancetre=TreeTools::getLastCommonAncestor(Arbre,Id_Genes); //BIZARERIE_1
				    //cout<<"J'ai trouvé le lca des 2 gènes"<<endl;
			      	    nom_classe=c.to_s(*it_ab1)+sep+c.to_s(*it_ab2)+sep+c.to_s(id_ancetre);
				   */

				   // Now we do it this way, where we refine later, just like for the case that it_ab1 != it_ab2
				   nom_classe=c.to_s(*it_ab1)+sep+c.to_s(*it_ab2);
			      	 }
			      else 
				 if((*it_ab2)<(*it_ab1))
				    nom_classe=c.to_s(*it_ab2)+sep+c.to_s(*it_ab1);
				 else
				    nom_classe=c.to_s(*it_ab1)+sep+c.to_s(*it_ab2);
			      //cout<<"\t elle apparatient à la classe "<<nom_classe<<endl;
			      
			      
			      if (classes_adjacences.find(nom_classe)!=classes_adjacences.end())
				 {
				    //cout<<nom_classe<<" est déjà dans classes_adjacences"<<endl;
				    //on vérifie qu'on a pas déjà ajouté cette adjacence
				    //cas où g1 et g2 appartiennent tous deux à ab1 et ab2 (ne devrait pas arriver dans les fichiers compara)
				    if (classes_adjacences[nom_classe].back()!=j)
				       classes_adjacences[nom_classe].push_back(j);
				 }
			      else	
				 {
				    //cout<<nom_classe<<" n'est PAS dans classes_adjacences"<<endl;
				    vector<int> v;
				    v.push_back(j);
				    classes_adjacences[nom_classe]=v;
				 }
			   }
		     j++;
		  }
	       else
		  {
		    //cout<<" --> on la jette."<<endl;
		  }
	    }
      }
   
   //Affichage
   cout<<"Nombres d'adjacences retenues : "<<adjacences.size()<<" (j="<<j<<")"<<endl;
   // vector<adjacence>::iterator it_adjacence;
   // for(it_adjacence=adjacences.begin();it_adjacence!=adjacences.end();it_adjacence++)
   //    cout<<(*it_adjacence).gene1<<" - "<<(*it_adjacence).gene2<<endl;
   //afficheMap(classes_adjacences);
   
   cout<<"Fichier d'adjacences "<<fic_adjacence<<" lu, classes construites."<<endl;
   IfficAdj.close();
   
   float taille_moy =0;
   int taille_tot= 0;
   int taille_min= adjacences.size();
   int taille_max= 0;
   int taille_1= 0;
   int nb_classes=classes_adjacences.size();
   afficheMap(classes_adjacences);
   cout << "\nadjacences" << endl << endl;
   afficheVector(adjacences);
   cout<<"\n\n\n\n\n*****************************\nNombre de classes : "<<nb_classes<<endl;

   map<string, vector<int> >::iterator it_cl;
   string fic_adjacences_seule=fic_arbre+"_"+fic_adjacence+"_seules";
   cout<<"Création du fichier des adjacences seules "<<fic_adjacences_seule<<endl;
   ofstream OfficAdjSeules;
   OfficAdjSeules.open(fic_adjacences_seule.c_str(), ios::out); 
   for(it_cl=classes_adjacences.begin();it_cl!=classes_adjacences.end();it_cl++)
      {
	 int taille=(*it_cl).second.size();
	 taille_tot+=taille;
	 if (taille<taille_min)
	    taille_min=taille;
	 if (taille>taille_max)
	    taille_max=taille;
	 if (taille==1)
	    {
	       taille_1++;
	       adjacence adj=adjacences[((*it_cl).second)[0]];
	       OfficAdjSeules<<adj.gene1<<" "<<adj.gene2<<endl;
	    }
      }
   OfficAdjSeules.close();
  
   taille_moy=taille_tot/float(nb_classes);
   cout<<"le taille moyenne "<<taille_moy<<" (min : "<<taille_min<<", max : "<<taille_max<<")"<<endl;
   cout<<"Nombre de classes de taille 1 : "<<taille_1<<endl;

   taille_tot-=taille_1;
   int num1,num2;
   vector<int>::iterator it_adj;
   TreeTemplate<Node> * Arbre1;//= new  TreeTemplate<Node>();
   TreeTemplate<Node> * Arbre2;//= new  TreeTemplate<Node>();

   cout << endl << "***************************************************" << endl;
   cout << "<7> refinement of the equivalence classes" << endl;
   cout << "***************************************************" << endl << endl;

   // we now get to the stage where we refine the equivalence classes,
   // or more precisely, build the equivalence classes out of
   // collections of adjacencies within either (a) pairs of trees, or
   // (b) just a single tree

   // the old-skool way of doing it (when only duplications and
   // speciations where happening) is commented out with compiler hash
   // tags, while the new one (which generalizes the old one), is
   // found after this

   // the old way of refining
#ifdef OLDREFINE
   //Élimination des classes de taille 1
   //+ ATTENTION BIS raffinage des classes entre deux arbres différents pour que les extrémités des 
   //adjacences ait la même espèce ancestre dnas les arbres
   //Nom de la classe x|x|n°espèce_ancestre_commun
   //+++ ATTENTION TER modif de la définition des classes, on ne raffine plus les classes entre deux 
   //arbres, on regarde simplement qu'il y ait bien un noeud ancêtre de chaque extremité de 
   //même espèce dans chacun des arbres
   //==> l'élagage d'un ou des deux arbres suffit et c'est fait après dans le traitement de chaque classe
   //==> on commente tout le bazar que ne sert plus ...
   //++++ ATTENTION QUATRO après vérif on peut encore raffiner ces classes :
   //soient r1 et r2 les espèces des racines des arbres 1 et 2
   //soient LCA1 et LCA2 les espèces des plus petits ancres communs des extrémités des adj dans arbre 1 et 2
   //si LCA1 = LCA2, on prend les sous arbres enracinés en ces noeuds
   //sinon, supposons LCA1 > LCA2
   //         soit LCA1<=r2, alors on remonte dans arbre2 jusqu'à un noeud d'espèce LC1 (tjs une seule classe)
   //         soit LCA1>r2, alors il faut couper dans arbre 1 au niveau des espèce de LCA1 (raffinage possible)
   //                             puis répartir les adj entre les classes raffinées si nécessaire
   //Codage des noms de classes entre 2 arbres x et y (x!=y)
   //x|y|id1-id2 id1 et id2 sont les racines des sous-arbres des arbres 1 et 2 constituant la classe
  
   map<string, vector<int> >::iterator it_cl_prec;
   for(it_cl=classes_adjacences.begin();it_cl!=classes_adjacences.end();it_cl++)
      {
	 nom_classe=(*it_cl).first;
	 if(affich_Classes)
	    cout<<"\nOn traite la classe "<<nom_classe<<" de taille "<<(*it_cl).second.size()<<endl;
	 //Élimination des classes de taille 1
	 if ((*it_cl).second.size()==1)
	    {
	       if(affich_Classes)
		  cout<<" : on la jette (taille 1)."<<endl;
	       if(it_cl!=classes_adjacences.begin())
		  {
		     it_cl_prec=it_cl;//dur 
		     it_cl_prec--; //dur !!
		  }
	       else
		  it_cl_prec=classes_adjacences.begin(); //et re-dur !!!
	       classes_adjacences.erase(it_cl);
	       it_cl=it_cl_prec;
	    }      

	 else
	    {
	       //raffinage éventuelle des classes entre deux arbres différents
	       if(affich_Classes)
		  cout<<" : on l'examine,";
	       num1=c.from_s(nom_classe.substr(0,nom_classe.find(sep)));
	       if (nom_classe.find(sep)==nom_classe.rfind(sep))
	 	  num2=c.from_s(nom_classe.substr(nom_classe.find(sep)+1,nom_classe.size()-1));
	       else
	 	  //cas où on a une classe du type 12|12|57
	 	  num2=c.from_s(nom_classe.substr(nom_classe.find(sep)+1,nom_classe.rfind(sep)-1));
	       if(affich_Classes)
		  cout<<" numéros extraits : "<<num1<<" et "<<num2<<endl;
	       if(num1!=num2)
		 {
	 	     if(affich_Classes)
			cout<<"\n******* On raffine éventuellement la classe "<<nom_classe<<" de taille "<<(*it_cl).second.size()<<"."<<endl;
		     //On va renommer cette classe quoi qu'il en soit
		     classes_adjacences_A_SUPPR.push_back((*it_cl).first);
	 	     Arbre1= dynamic_cast <TreeTemplate<Node> *> (Arbres[num1]);
	 	     Arbre2= dynamic_cast <TreeTemplate<Node> *> (Arbres[num2]);
	 	     TreeTemplate<Node > A1=*Arbre1;
	 	     TreeTemplate<Node > A2=*Arbre2;
	 	     //On recherche les LCA des extremité des adj dans chaque arbre
	 	     vector< int > Id_Genes1;
	 	     vector< int > Id_Genes2;
	 	     //parcours des adjacences
	 	     for(int a=0;a!=(*it_cl).second.size();a++)
			{
			   //Le numéro de l'adjacance
			   string nom_gene1,nom_gene2;
			   int va=(*it_cl).second[a];
			   //gène 1
			   nom_gene1 =adjacences[va].gene1;//+sep+genes_especes[adjacences[va].gene1];
			   //gène 2
			   nom_gene2 =adjacences[va].gene2;//+sep+genes_especes[adjacences[va].gene2];
			   if(affich_Classes)
			      cout<<"Adj n°"<<va<<" "<<nom_gene1<<" - "<<nom_gene2<<endl;
			   try
			      {
				 Id_Genes1.push_back(Arbre1->getLeafId(nom_gene1));
				 Id_Genes2.push_back(Arbre2->getLeafId(nom_gene2));
			      }
			   catch (Exception e) 
			      {
				 Id_Genes1.push_back(Arbre1->getLeafId(nom_gene2));
				 Id_Genes2.push_back(Arbre2->getLeafId(nom_gene1));
			      }
			}
		     if(affich_Classes)
			{
			   cout<<"Id_Genes1 : ";
			   for(int f=0;f<Id_Genes1.size();f++)
			      cout<<Id_Genes1[f]<<" ";
			   cout<<endl;
			   cout<<"Id_Genes2 : ";
			   for(int f=0;f<Id_Genes2.size();f++)
			      cout<<Id_Genes2[f]<<" ";
			   cout<<endl;
			}
		     int id_LCA1=TreeTools::getLastCommonAncestor(A1,Id_Genes1);//BIZARERIE_1
		     int id_LCA2=TreeTools::getLastCommonAncestor(A2,Id_Genes2);//BIZARERIE_1
		     if(affich_Classes)
			cout<<"id_LCA1="<<id_LCA1<<" et id_LCA2="<<id_LCA2<<endl;

		     BppString * EspLCA1 = dynamic_cast <BppString *> (Arbre1->getNode(id_LCA1)->getNodeProperty(esp));
		     if(affich_Classes)
			cout<<"Espèce du LCA1="<<EspLCA1->toSTL()<<endl;
		     BppString * EspLCA2 = dynamic_cast <BppString *> (Arbre2->getNode(id_LCA2)->getNodeProperty(esp));
		     if(affich_Classes)
			cout<<"Espèce du LCA2="<<EspLCA2->toSTL()<<endl;
		     //Si les deux ancêtres sont de même espèce alors pas de raffinage
		     if(EspLCA1->toSTL()==EspLCA2->toSTL())
			{
			   if(affich_Classes)
			      cout<<"***** Cas LCA1 et LCA2 de même espèce"<<endl;
			   //On change juste le nom :
			   string nom=c.to_s(num1)+sep+c.to_s(num2)+sep+c.to_s(id_LCA1)+sepAdj+c.to_s(id_LCA2);
			   if(affich_Classes)
			      cout<<"Nouveau nom de la classe : "<<nom<<endl;
			   //Une classe à ajouter : nom différent mais mêmes adjacences
			   classes_adjacences_SUB[nom]=(*it_cl).second;
			}
		     else
			{
			   if(affich_Classes)
			      cout<<"***** Cas LCA1 et LCA2 d'espèces différentes, on cherche les espèces des racines"<<endl;
			   BppString * r1 = dynamic_cast <BppString *> (Arbre1->getRootNode()->getNodeProperty(esp));
			   if(affich_Classes)
			      cout<<"Espèce de la racine de l'arbre 1 : r1="<<r1->toSTL()<<endl;
			   BppString * r2 = dynamic_cast <BppString *> (Arbre2->getRootNode()->getNodeProperty(esp));
			   if(affich_Classes)
			      cout<<"Espèce de la racine de l'arbre 2 : r2="<<r2->toSTL()<<endl;
			   if(isAncestor(c.from_s(EspLCA1->toSTL()),c.from_s(EspLCA2->toSTL()),S))
			      {
				 if(affich_Classes)
				    cout<<"\tLCA1 ancêtre de LCA2"<<endl;
				 if(isAncestor(c.from_s(r2->toSTL()),c.from_s(EspLCA1->toSTL()),S))
				    {
				       if(affich_Classes)
					  cout<<"\tr2 ancêtre de LCA1"<<endl;
				       //il faut remonter dans arbre 2, à partir de LCA2 et couper au niveau de l'espèce de LCA1
				       int coupe2=trouveNoeudSpeSAuDessusDeN(c.from_s(EspLCA1->toSTL()), id_LCA2, Arbre2);
				       //On change le nom :
				       string nom=c.to_s(num1)+sep+c.to_s(num2)+sep+c.to_s(id_LCA1)+sepAdj+c.to_s(coupe2);
				       if(affich_Classes)
					  cout<<"Nouveau nom de la classe : "<<nom<<endl;
				       //Une classe à ajouter : nom différent mais mêmes adjacences
				       classes_adjacences_SUB[nom]=(*it_cl).second;
				    }
				 else
				    {
				       if(affich_Classes)
					  cout<<"\tLCA1 > r2 => raffinage obligatoire"<<endl;//À PROUVER
				       //Il faut couper dans arbre 1 pour trouver les noeuds d'espèce LCA2
				       vector<int> Ids;
				       retourneIdSpeSAuDessousDeN(Ids,c.from_s(EspLCA2->toSTL()),Arbre1->getNode(id_LCA1),Arbre1);
				       if(Ids.size()==0)
					  cout<<"On n'a pas trouvé de noeud d'espèce LCA2 en dessous de LCA1, IMPOSSIBLE";
				       else if(Ids.size()==1)
					  cout<<"On a trouvé un seul noeud d'espèce LCA2 en dessous de LCA1, IMPOSSIBLE";
				       else
					  {
					     if(affich_Classes)
						cout<<"On doit raffiner en "<<Ids.size()<<" classes."<<endl;
					     //Les nouveaux noms :
					     for(int k=0;k<Ids.size();k++)
						{
						   string nom=c.to_s(num1)+sep+c.to_s(num2)+sep+c.to_s(Ids[k])+sepAdj+c.to_s(id_LCA2);
						   if(affich_Classes)
						      cout<<"Nouveau nom de la classe raffinée : "<<nom<<endl;
						   //Une classe à ajouter, on l'initialise on y rangera les adj plus tard
						   vector<int> a;
						   classes_adjacences_SUB[nom]=a;
						}
					     //On range les adjacences dans les nouvelles classes
					     //Les identifiants des extremités des adj se trouvent dans l'ordre dans Id_Genes1 et Id_Genes2
					     for(int a=0;a!=(*it_cl).second.size();a++)
						{
						   int cpt=0;
						   while(!isAncestor(Ids[cpt],Id_Genes1[a],Arbre1)&&cpt<Ids.size())
						      cpt++;
						   if(cpt==Ids.size())
						      cout<<"Problème, on n'a pas trouvé de classe pour l'adj "<<a<<endl;
						   else
						      {
							 string nom=c.to_s(num1)+sep+c.to_s(num2)+sep+c.to_s(Ids[cpt])+sepAdj+c.to_s(id_LCA2);
							 classes_adjacences_SUB[nom].push_back((*it_cl).second[a]);
						      }
						}
					  }
				    }
			      }
			   else // *** NB *** with only dups and specs, LCA1 < LCA2 or vice versa . . . but with transfers added to the mix, we can have the case where neither is an ancestor of the other ... have to handle this case
			      {
				 if(affich_Classes)
				    cout<<"\tLCA2 ancêtre de LCA1"<<endl;
				 if(isAncestor(c.from_s(r1->toSTL()),c.from_s(EspLCA2->toSTL()),S))
				    {
				       if(affich_Classes)
					  cout<<"\tr1 ancêtre de LCA2"<<endl;
				       //il faut remonter dans arbre 1, à partir de LCA1 et couper au niveau de l'espèce de LCA2
				       int coupe1=trouveNoeudSpeSAuDessusDeN(c.from_s(EspLCA2->toSTL()), id_LCA1, Arbre1);
				       //On change le nom :
				       string nom=c.to_s(num1)+sep+c.to_s(num2)+sep+c.to_s(coupe1)+sepAdj+c.to_s(id_LCA2);
				       if(affich_Classes)
					  cout<<"Nouveau nom de la classe : "<<nom<<endl;
				       //Une classe à ajouter : nom différent mais mêmes adjacences
				       classes_adjacences_SUB[nom]=(*it_cl).second;
				    }
				 else
				    {
				       if(affich_Classes)
					  cout<<"\tLCA2 > r1 => raffinage obligatoire"<<endl;//À PROUVER
				       //Il faut couper dans arbre 2 pour trouver les noeuds d'espèce LCA1
				       vector<int> Ids;
				       retourneIdSpeSAuDessousDeN(Ids,c.from_s(EspLCA1->toSTL()),Arbre2->getNode(id_LCA2),Arbre2);
				       if(Ids.size()==0)
					  cout<<"On n'a pas trouvé de noeud d'espèce LCA1 en dessous de LCA2, IMPOSSIBLE";
				       else if(Ids.size()==1)
					  cout<<"On a trouvé un seul noeud d'espèce LCA1 en dessous de LCA2, IMPOSSIBLE";
				       else
					  {
					     if(affich_Classes)
						cout<<"On doit raffiner en "<<Ids.size()<<" classes."<<endl;
					     //Les nouveaux noms :
					     for(int k=0;k<Ids.size();k++)
						{
						   string nom=c.to_s(num1)+sep+c.to_s(num2)+sep+c.to_s(id_LCA1)+sepAdj+c.to_s(Ids[k]);
						   if(affich_Classes)
						      cout<<"Nouveau nom de la classe raffiner : "<<nom<<endl;
						   //Une classe à ajouter, on l'initialise on y rangera les adj plus tard
						   vector<int> a;
						   classes_adjacences_SUB[nom]=a;
						}
					     //On range les adjacences dans les nouvelles classes
					     //Les identifiants des extremités des adj se trouvent dans l'ordre dans Id_Genes1 et Id_Genes2
					     for(int a=0;a!=(*it_cl).second.size();a++)
						{
						   int cpt=0;
						   while(!isAncestor(Ids[cpt],Id_Genes2[a],Arbre2)&&cpt<Ids.size())
						      cpt++;
						   if(cpt==Ids.size())
						      cout<<"Problème, on n'a pas trouvé de classe pour l'adj "<<a<<endl;
						   else
						      {
							 string nom=c.to_s(num1)+sep+c.to_s(num2)+sep+c.to_s(id_LCA1)+sepAdj+c.to_s(Ids[cpt]);
							 classes_adjacences_SUB[nom].push_back((*it_cl).second[a]);
						      }
						}
					  }
				    }
			      }
			}
		 }
	    }
      }
#endif // end #OLDREFINE: the old way of refning

   /*
     the current way of building the equivalence classes -- we now
     generate the equivalence classes in the following fashion:

     (a) we first (and still) eliminate classes of size 1

     (b) for classes of the form x|y (resp., x|x) : i.e., b/w two
     different trees (resp., within one tree), we find all highest
     pairs (n_1, n_2) of nodes in the set G_1 x G_2 such that
     S(n_1)=S(n_2) (resp., but n_1 != n_2) and the pair (n_1, n_2) is
     the ancestor of at least one adjacency in considered collection
     of adjacencies (we do not discard the singleton classes created
     at this stage becuase they maybe merged into another tree during
ls     the backtracking ... however, we have not yet implemented this
     merging)

   */

   map<string, vector<int> >::iterator it_cl_prec;
   for(it_cl=classes_adjacences.begin();it_cl!=classes_adjacences.end();it_cl++) {
     nom_classe=(*it_cl).first;
     if(affich_Classes)
       cout<<"\nOn traite la classe "<<nom_classe<<" de taille "<<(*it_cl).second.size()<<endl;

     /*
       taille 1 : Élimination (encore) des classes de taille 1
     */
     if ((*it_cl).second.size()==1) {
       if(affich_Classes)
	 cout<<" : on la jette (taille 1)."<<endl;
       if(it_cl!=classes_adjacences.begin()) {
	 it_cl_prec=it_cl;//dur 
	 it_cl_prec--; //dur !!
       }
       else
	 it_cl_prec=classes_adjacences.begin(); //et re-dur !!!
       classes_adjacences.erase(it_cl);
       it_cl=it_cl_prec;
     }

     /*
       taille > 1 : 'raffinage' éventuelle des classes entre deux
       arbres différents
     */
     else {
       if(affich_Classes)
	 cout<<" : on l'examine,";
       num1=c.from_s(nom_classe.substr(0,nom_classe.find(sep)));
       // if (nom_classe.find(sep)==nom_classe.rfind(sep)) ... this will always hold
       num2=c.from_s(nom_classe.substr(nom_classe.find(sep)+1,nom_classe.size()-1));
       /* ... and this case is not needed anymore
       else //cas où on a une classe du type 12|12|57
	 num2=c.from_s(nom_classe.substr(nom_classe.find(sep)+1,nom_classe.rfind(sep)-1));
       */
       if(affich_Classes)
	 cout<<" numéros extraits : "<<num1<<" et "<<num2<<endl;

       /*
	 now that we have extracted the numbers, we know if the class
	 is of the form x|y or x|x ... so now, to do (b) we do a
	 number of steps, that differ slightly based on whether class
	 is of form x|y or x|x
	*/
       if(affich_Classes)
	 cout<<"\n******* On raffine éventuellement la classe "<<nom_classe<<" de taille "<<(*it_cl).second.size()<<"."<<endl;
       //On va renommer cette classe quoi qu'il en soit
       classes_adjacences_A_SUPPR.push_back((*it_cl).first);
       Arbre1= dynamic_cast <TreeTemplate<Node> *> (Arbres[num1]);
       TreeTemplate<Node > A1=*Arbre1;

       if(num1 != num2) {
	 Arbre2= dynamic_cast <TreeTemplate<Node> *> (Arbres[num2]);
	 TreeTemplate<Node > A2=*Arbre2;
       }

       vector<int> Id_Genes1;
       vector<int> Id_Genes2;

       //parcours des adjacences
       for(int a=0;a!=(*it_cl).second.size();a++) {
	 //Le numéro de l'adjacence
	 string nom_gene1,nom_gene2;
	 int va=(*it_cl).second[a];
	 //gène 1
	 nom_gene1 =adjacences[va].gene1;//+sep+genes_especes[adjacences[va].gene1];
	 //gène 2
	 nom_gene2 =adjacences[va].gene2;//+sep+genes_especes[adjacences[va].gene2];

	 if(affich_Classes)
	   cout<<"Adj n°"<<va<<" "<<nom_gene1<<" - "<<nom_gene2<<endl;

	 if(num1 != num2) {
	   try {
	     Id_Genes1.push_back(Arbre1->getLeafId(nom_gene1));
	     Id_Genes2.push_back(Arbre2->getLeafId(nom_gene2));
	   }
	   catch (Exception e) {
	     Id_Genes1.push_back(Arbre1->getLeafId(nom_gene2));
	     Id_Genes2.push_back(Arbre2->getLeafId(nom_gene1));
	   }
	 }
	 else { // NOTE* : l1,l2 may not correspond to n1,n2 like above
	   Id_Genes1.push_back(Arbre1->getLeafId(nom_gene1));
	   Id_Genes2.push_back(Arbre1->getLeafId(nom_gene2));
	 }
       }

       if(affich_Classes) {
	 cout<<"Id_Genes1 : ";
	 for(int f=0;f<Id_Genes1.size();f++)
	   cout<<Id_Genes1[f]<<" ";
	 cout<<endl;
	 cout<<"Id_Genes2 : ";
	 for(int f=0;f<Id_Genes2.size();f++)
	   cout<<Id_Genes2[f]<<" ";
	 cout<<endl;
       }

       /*
	 we now get all highest pairs (n_1,n_2) of G_1 x G_2 such that
	 S(n_1)=S(n_2) (ensuring that n_1 != n_2 in the case that num1
	 == num2), and (n_1,n_2) is the ancestor of at least one
	 adjacency of the considered set of adjacencies
       */
       vector<adjacence> Ids; // will contain all pairs (n_1, n_2)
       if(num1 != num2)
	 retournePairsOfIds(Ids, Id_Genes1, Id_Genes2, Arbre1, Arbre2);
       else
	 retournePairsOfIds(Ids, Id_Genes1, Id_Genes2, Arbre1, Arbre1);

       if(affich_Classes) {
	 cout << "vector of ids from retournePairsOfIds" << endl;
	 afficheVector(Ids);
       }

       //Les nouveaux noms :
       for(int k=0; k<Ids.size(); ++k) {
	 string nom=c.to_s(num1)+sep+c.to_s(num2)+sep+(Ids[k].gene1)+sepAdj+(Ids[k].gene2);
	 if(affich_Classes)
	   cout<<"Nouveau nom de la classe raffinée : "<<nom<<endl;
	 //Une classe à ajouter, on l'initialise on y rangera les adj plus tard
	 vector<int> a;
	 classes_adjacences_SUB[nom]=a;
       }
	 
       //On range les adjacences dans les nouvelles classes
       //Les identifiants des extremités des adj se trouvent dans l'ordre dans Id_Genes1 et Id_Genes2
       for(int a=0; a!=(*it_cl).second.size(); ++a) {
	 int cpt=0;
	 if(num1!=num2) {
	   while(!isAncestor(c.from_s(Ids[cpt].gene1), Id_Genes1[a], Arbre1) && cpt < Ids.size())
	     cpt++;
	 }
	 else {
	   // it's a bit more complex in the single tree case, so we
	   // invoke an auxiliary routine (its description also there)
	   while(!correspondsTo(Ids[cpt], Id_Genes1[a], Id_Genes2[a], Arbre1) && cpt < Ids.size())
	     ++cpt;
	 }

	 if(cpt == Ids.size())
	   cout << "On n'a pas trouvé de classe pour l'adj " << a << endl;
	 else {
	   string nom=c.to_s(num1)+sep+c.to_s(num2)+sep+(Ids[cpt].gene1)+sepAdj+(Ids[cpt].gene2);
	   classes_adjacences_SUB[nom].push_back((*it_cl).second[a]);
	 }
       }
     }
   }

   // **** we are now at the end of the new way of refining, and
   // continue the original program as before

   cout<<"Nombre de classes éliminées après traitement des classes entre deux arbres disctincts : "<<classes_adjacences_A_SUPPR.size()<<endl;
   //Supprimer les anciennes
   for(it_feuilles=classes_adjacences_A_SUPPR.begin();it_feuilles!=classes_adjacences_A_SUPPR.end();it_feuilles++)
      classes_adjacences.erase(*it_feuilles);
   
   cout<<"Nombre de classes créées après traitement des classes entre deux arbres disctincts : "<<classes_adjacences_SUB.size()<<endl;
   //On peut lors le raffinage créer des classes vides ou à un seul élément
   int taille_0=0;
   int taille_2=0;
   //Ajouter les classes créees ou renommer :
   for(it_cl=classes_adjacences_SUB.begin();it_cl!=classes_adjacences_SUB.end();it_cl++)
     if ((*it_cl).second.size()==0) 
	 taille_0++;
     else if ((*it_cl).second.size()==1) 
	 taille_1++;
     else { //taille>=2
       taille_2++;
       classes_adjacences[(*it_cl).first]=(*it_cl).second;
     }
   cout<<"\t(dont "<<taille_0<<" classes de taille 0 et "<<taille_2<<" classes de taille >=2)"<<endl;
   cout<<"Nouveau nombre de classe de taille 1 : "<<taille_1<<endl;
   
   nb_classes=classes_adjacences.size();

   cout<<"Nombre FINAL de classes À TRAITER après élimination des classes de taille 1 : "<<nb_classes<<endl;
   cout<<"Moyenne corrigée = "<<taille_tot/float(nb_classes)<<endl;
   
   //afficheMap(classes_adjacences);
   //pour voir les noms des gènes adjacents :
   // map<string, vector<int> >::iterator it_cl_adj;
   // for (it_cl_adj=classes_adjacences.begin();it_cl_adj!=classes_adjacences.end();it_cl_adj++)
   //    {
   // 	 cout<<(*it_cl_adj).first<< " :";
   // 	 vector<int> v=(*it_cl_adj).second;
   // 	 vector<int>::iterator it2;
   // 	 for (it2=v.begin();it2!=v.end();it2++)
   // 	    cout<<" "<<*it2<<"("<<adjacences[*it2].gene1<<sepAdj<<adjacences[*it2].gene2<<")";
   // 	 cout<<endl;
   //    }

   // B/ Traitement : 
   //vector<Tree *> ArbresExamines; //pour stocker les paires de sous arbres réellement examinés
   int num_diff,num_ident;
   vector<adjacence> * Adj_classe= new vector<adjacence> ();
   
   string fic_adjacence2=fic_arbre+"_SORTIE_adj";
   string fic_arbresdAdjacences=fic_arbre+"_arbresDAdjacences";
   cout<<"Création du fichier des adjacences ancestrales "<<fic_adjacence2<<" et du fichier des arbres d'adjacences "<<fic_arbresdAdjacences<<"...";
   ofstream OfficAdj2;
   OfficAdj2.open(fic_adjacence2.c_str(), ios::out); 
   ofstream OfficArbresAdj;
   OfficArbresAdj.open(fic_arbresdAdjacences.c_str(), ios::out); 
   cout<<" terminée."<<endl;
   
   //fic sortie_dup des gènes dupliqués ensembles
   cout<<"Ouverture du fichier des gènes dupliqués ensembles ...";
   string fic_SORTIE_dup=fic_arbre+"_SORTIE_dup";
   ofstream OfficSORTIE_dup;
   OfficSORTIE_dup.open(fic_SORTIE_dup.c_str(), ios::out); 
   cout<<" OK."<<endl;

   //fic sortie_trf des genes transferes ensembles
   cout<<"Ouverture du fichier des gènes transferés ensembles ...";
   string fic_SORTIE_trf=fic_arbre+"_SORTIE_trf";
   ofstream OfficSORTIE_trf;
   OfficSORTIE_trf.open(fic_SORTIE_trf.c_str(), ios::out);
   cout<<" OK."<<endl;

   cout<<"Ouverture du fichier des sous-arbres traités ...";
   string fic_sousArbresTraites = fic_arbre+"_traites";
   ofstream OfficSousArbres;
   OfficSousArbres.open(fic_sousArbresTraites.c_str(), ios::out); 
   cout<<" OK."<<endl;
   
   num_diff=num_ident=0;
   //cout<<"Juste avant for"<<endl;
   //    1) Pour chaque classe
   for(it_cl=classes_adjacences.begin();it_cl!=classes_adjacences.end();it_cl++) {
     //cout<<"Juste après for"<<endl;
     Adj_classe->clear();
	 
     //cout<<"Juste après clear"<<endl;
     //    	     a) Récupérer arbres de gènes
     nom_classe=(*it_cl).first;
     //cout<<"Juste après nom_classe"<<endl;
     num1=c.from_s(nom_classe.substr(0,nom_classe.find(sep)));
     num2=c.from_s(nom_classe.substr(nom_classe.find(sep)+1,nom_classe.length()-1));
     //if(affich_Classes)
     cout<<"On traite la classe "<<nom_classe<<endl;
     //", de num extraits : "<<num1<<" et "<<num2<<"."<<

     if(num1!=num2) {
       if(affich_Classes) {
	 cout<<"\tNuméros diffrents."<<endl;
	 cout<<"Extraction des identifiants des noeuds où couper :"<<endl;
       }
       int coupe1,coupe2;
       coupe1=c.from_s(nom_classe.substr(nom_classe.rfind(sep)+1,nom_classe.find(sepAdj)));
       coupe2=c.from_s(nom_classe.substr(nom_classe.find(sepAdj)+1,nom_classe.length()-1));
       if(affich_Classes) {
	 cout<<"\tCoupe dans l'arbre 1 à l'id "<<coupe1<<endl;
	 cout<<"\tCoupe dans l'arbre 2 à l'id "<<coupe2<<endl;
       }
       //Les manips ci-dessous sont-elles bien nécessaires ?
       Arbre1= dynamic_cast <TreeTemplate<Node> *> (Arbres[num1]);
       Arbre2= dynamic_cast <TreeTemplate<Node> *> (Arbres[num2]);
       TreeTemplate<Node > A1=*Arbre1;
       TreeTemplate<Node > A2=*Arbre2;
       Arbre1= new TreeTemplate<Node>();
       Arbre1=A1.cloneSubtree(coupe1);
       Arbre2= new TreeTemplate<Node>();
       Arbre2=A2.cloneSubtree(coupe2);
       num_diff++;
     }
     else {
       if(affich_Classes) {
	 cout<<"\tMême numéros."<<endl;
	 cout<<"Extraction des identifiants des noeuds où couper :"<<endl;
       }
       int coupe1,coupe2;
       coupe1=c.from_s(nom_classe.substr(nom_classe.rfind(sep)+1,nom_classe.find(sepAdj)));
       coupe2=c.from_s(nom_classe.substr(nom_classe.find(sepAdj)+1,nom_classe.length()-1));
       if(affich_Classes)
	 cout << "\tCoupe dans l'arbre à l'id : " << coupe1 << " et " << coupe2 << endl;
       // faire les sous-arbres
       TreeTemplate<Node > ArbreUnique=*(Arbres[num1]);
       //set<string> nom_genes_adjacents;   <-- À virer
       Arbre1= new TreeTemplate<Node>();
       Arbre1=ArbreUnique.cloneSubtree(coupe1);
       cout<<"Sous-arbre 1 : nb feuilles = "<<Arbre1->getNumberOfLeaves()<<endl;
       Arbre2= new  TreeTemplate<Node>();
       Arbre2=ArbreUnique.cloneSubtree(coupe2); 
       cout<<"Sous-arbre 2 : nb feuilles = "<<Arbre2->getNumberOfLeaves()<<endl;
       num_ident++;
     }

     // cout<<"Feuilles de l'arbre 1 par exemple :"<<endl;
     // vector<string> f=Arbre1->getLeavesNames();
     // vector<string>::iterator it_f;
     // for(it_f=f.begin();it_f!=f.end();it_f++)
     //    cout<<*it_f<<" ";
     // cout<<endl;

     // 		     b) Récupérer adjacences
     OfficSousArbres<<"Adjacences à traiter :"<<endl;
     for(it_adj=(*it_cl).second.begin();it_adj!=(*it_cl).second.end();it_adj++)
       {
	 OfficSousArbres<<adjacences[*it_adj].gene1<<" - "<<adjacences[*it_adj].gene2<<endl;
	 Adj_classe->push_back(adjacences[*it_adj]);
       }

	 //cout<<"\nNb d'adjacences dans cette classe : "<<Adj_classe->size()<<endl;
	 
	 // 	     c) Appliquer l'algo DéCo (produit des adjacences ancestrales qu'on accumule dans un fichier de même type que le fichier adjacences d'entrée mais où les noms d'espèces sont ceux des espèces ancestrales)
	 //cout<<"Sous-arbre 1 : nb feuilles = "<<Arbre1->getNumberOfLeaves()<<endl;

	 // BppString * PROP;
	 // if(Arbre1->getRootNode()->hasBranchProperty(esp))
	 //    {
	 //       PROP = dynamic_cast<BppString*> (Arbre1->getRootNode()->getBranchProperty(esp)); 
	 //       cout<<"Propriété racine arbre 1 avant affectation : "<<PROP->toSTL()<<endl;
	 //    }
	 // else
	 //    cout<<"Propriété racine arbre 1 avant affectation : ABSENTE"<<endl;
	 // if(Arbre1->getRootNode()->getNumberOfSons()>0)
	 //    {
	 //       //fils 0
	 //       if(Arbre1->getRootNode()->getSon(0)->hasBranchProperty(esp))
	 // 	  {
	 // 	     PROP = dynamic_cast<BppString*> (Arbre1->getRootNode()->getSon(0)->getBranchProperty(esp)); 
	 // 	     cout<<"\tPropriété fils 0 racine arbre 1 avant affectation : "<<PROP->toSTL()<<endl;
	 // 	  }
	 //       else
	 // 	  cout<<"\tPropriété fils 0 racine arbre 1 avant affectation : ABSENTE"<<endl;
	 //       //fils 1
	 //       if(Arbre1->getRootNode()->getSon(1)->hasBranchProperty(esp))
	 // 	  {
	 // 	     PROP = dynamic_cast<BppString*> (Arbre1->getRootNode()->getSon(1)->getBranchProperty(esp)); 
	 // 	     cout<<"\tPropriété fils 1 racine arbre 1 avant affectation : "<<PROP->toSTL()<<endl;
	 // 	  }
	 //       else
	 // 	  cout<<"\tPropriété fils 1 racine arbre 1 avant affectation : ABSENTE"<<endl;
	 //    }
	 
	 // if(Arbre2->getRootNode()->hasBranchProperty(esp))
	 //    {
	 //       PROP = dynamic_cast<BppString*> (Arbre2->getRootNode()->getBranchProperty(esp)); 
	 //       cout<<"Propriété racine arbre 2 avant affectation : "<<PROP->toSTL()<<endl;
	 //    }
	 // else
	 //    cout<<"Propriété racine arbre 1 avant affectation : ABSENTE"<<endl;
	 
	 // affecteInfoSurBrancheNoeuds(Arbre1->getRootNode());
   	 // cout<<"Sous-arbre 1 : nb feuilles = "<<Arbre1->getNumberOfLeaves()<<endl;
         if(INPUT_FORMAT==1)
         {
	   affecteInfoSurBrancheNoeuds(Arbre1->getRootNode());
	   affecteInfoSurBrancheNoeuds(Arbre2->getRootNode());
         }

	 TreeTemplate<Node> outA1 = *Arbre1;
	 TreeTemplate<Node> outA2 = *Arbre2;

	 if(INPUT_FORMAT==2 || INPUT_FORMAT == 3) { // for format 2 (add branches to subdivided trees)
	   ajouteBranches(outA1.getRootNode());
	   ajouteBranches(outA2.getRootNode());
	 }

	 Newick* newickWriterSousArbres = new Newick();
	 newickWriterSousArbres->enableExtendedBootstrapProperty(bsv);
	 Tree* Sa1 = dynamic_cast <Tree*> (&outA1);
	 Tree* Sa2 = dynamic_cast <Tree*> (&outA2);

	 OfficSousArbres<<"\nSous-arbre 1 (arbre "<<num1<<") :"<<endl;
	 if (outA1.getNumberOfLeaves()!=0)
	   newickWriterSousArbres->write(*Sa1, OfficSousArbres);
	 //newickReaderEsp->write(outA1,OfficSousArbres);
	 else
	    OfficSousArbres<<outA1.getRootNode()->getName()<<endl;
	 //cout<<"Sous-arbre 2 : nb feuilles = "<<Arbre2->getNumberOfLeaves()<<endl;
  	 OfficSousArbres<<"\nSous-arbre 2 (arbre "<<num2<<") :"<<endl;
	 if (outA2.getNumberOfLeaves()!=0)
	   newickWriterSousArbres->write(*Sa2, OfficSousArbres);
	 //newickReaderEsp->write(outA2,OfficSousArbres);
	 else
	    OfficSousArbres<<outA2.getRootNode()->getName()<<endl;
	 OfficSousArbres<<"\n***************************************************************************\n"<<endl;
	 //cout<<"Après écriture de la paire de sous-arbes"<<endl;
	 
	 // PROP = dynamic_cast<BppString*> (Arbre1->getRootNode()->getBranchProperty(esp)); 
	 // cout<<"Propriété racine arbre 1 après affectation : "<<PROP->toSTL()<<endl;
	 // if(Arbre1->getRootNode()->getNumberOfSons()>0)
	 //    {
	 //       //fils 0
	 //       if(Arbre1->getRootNode()->getSon(0)->hasBranchProperty(esp))
	 // 	  {
	 // 	     PROP = dynamic_cast<BppString*> (Arbre1->getRootNode()->getSon(0)->getBranchProperty(esp)); 
	 // 	     cout<<"\tPropriété fils 0 racine arbre 1 après affectation : "<<PROP->toSTL()<<endl;
	 // 	  }
	 //       else
	 // 	  cout<<"\tPropriété fils 0 racine arbre 1 après affectation : ABSENTE"<<endl;
	 //       //fils 1
	 //       if(Arbre1->getRootNode()->getSon(1)->hasBranchProperty(esp))
	 // 	  {
	 // 	     PROP = dynamic_cast<BppString*> (Arbre1->getRootNode()->getSon(1)->getBranchProperty(esp)); 
	 // 	     cout<<"\tPropriété fils 1 racine arbre 1 après affectation : "<<PROP->toSTL()<<endl;
	 // 	  }
	 //       else
	 // 	  cout<<"\tPropriété fils 1 racine arbre 1 après affectation : ABSENTE"<<endl;
	 //    }

	 // PROP = dynamic_cast<BppString*> (Arbre2->getRootNode()->getBranchProperty(esp)); 
	 // cout<<"Propriété racine arbre 2 après affectation : "<<PROP->toSTL()<<endl;

	 DECO(S,Arbre1,Arbre2,Adj_classe,OfficAdj2,OfficArbresAdj,OfficSORTIE_dup,OfficSORTIE_trf,nom_classe);
	 //cout<<"Fin traitement classe "<<nom_classe<<"."<<endl;
	 //cout<<"*****"<<endl;
      }
   
   //À FAIRE
   //FERMER/DÉTRUIRE TOUT CE DONT ON NE SE SERT PLUS
   OfficSousArbres.close();
   OfficAdj2.close();
   OfficArbresAdj.close();
   
   //PARTIE STAT
   //map contenant les adjacences ancestrales par espèces
   map<int,vector<adjacence> > Adj_ancestrales;
   map<int,vector<adjacence> >::iterator it_AA;
   int nb_aa_tot=0;
   int nb_aa_min=100000;
   int nb_aa_max=0;
   int nb_adj_par_geneA_tot=0;
   int nb_adj_par_geneA_min=100000;
   int nb_adj_par_geneA_max=0;
   int nb_adj_par_geneA_sup2=0;
   int nb_genes_a=0;

   cout<<"###########################################################"<<endl;
   cout<<"Lecture du fichier d'adjacences ancestrales : "<<fic_adjacence2<<endl;
   lectureFicRelBin(fic_adjacence2, Adj_ancestrales);
   // cout<<"Affichage des adjacences ancestrales par espèce :"<<endl;
   // afficheMap(Adj_ancestrales);  

   cout << endl << endl;
   afficheMap(Adj_ancestrales);

   //Pour stocker le nb de gènes par espèce ancestrale
   map<int,int> espece_nb_genes;
   map<string,int> genes_ancestraux_nb_adj;
   map<string,int>::iterator it_gana;
   //Stats pour le nb d'adj ancestrales par espèce + nb gènes ancestraux
   for(it_AA=Adj_ancestrales.begin();it_AA!=Adj_ancestrales.end();it_AA++)
      {
	 vector<adjacence> v=(*it_AA).second;
	 vector<adjacence>::iterator it_v;
	 //nb d'adj ancestrales par espèce
	 int n=v.size();
	 nb_aa_tot+=n;
	 if(nb_aa_min>n)
	    nb_aa_min=n; 
	 if(nb_aa_max<n)
	    nb_aa_max=n;
	 //nb gènes ancestraux
	 set<string> genes;
	 for(it_v=v.begin();it_v!=v.end();it_v++)
	    {
	       genes.insert((*it_v).gene1);
	       genes.insert((*it_v).gene2);
	       if(genes_ancestraux_nb_adj.find((*it_v).gene1)!=genes_ancestraux_nb_adj.end())
		  genes_ancestraux_nb_adj[(*it_v).gene1]++;
	       else
		  genes_ancestraux_nb_adj[(*it_v).gene1]=1;
	       if(genes_ancestraux_nb_adj.find((*it_v).gene2)!=genes_ancestraux_nb_adj.end())
		  genes_ancestraux_nb_adj[(*it_v).gene2]++;
	       else
		  genes_ancestraux_nb_adj[(*it_v).gene2]=1;
	    }
	 espece_nb_genes[(*it_AA).first]=genes.size();
	 nb_genes_a+=genes.size();
      } 
   
   for(it_gana=genes_ancestraux_nb_adj.begin();it_gana!=genes_ancestraux_nb_adj.end();it_gana++)
      {
	 int n=(*it_gana).second;
	 nb_adj_par_geneA_tot+=n;
	 if(nb_adj_par_geneA_min>n)
	    nb_adj_par_geneA_min=n; 
	 if(nb_adj_par_geneA_max<n)
	    nb_adj_par_geneA_max=n;
	 if(n>2)
	    nb_adj_par_geneA_sup2++;
      }

   cout<<"\n\n####################\nStatistiques sur les "<<nb_classes<<" classes traitées :"<<endl;
   cout<<"\nNombre d'arbres d'adjacences moyen par classe = "<<nb_arbres_adj_tot/float(nb_classes)<<" (nb arbres d'adj tot = "<<nb_arbres_adj_tot<<")"<<endl;
   cout<<"Nb min = "<<nb_arbres_adj_min<<" ; nb max = "<<nb_arbres_adj_max<<endl;
   cout<<endl;
   cout<<"Nombre d'espèces ancestrales : "<<Adj_ancestrales.size()<<endl;
   cout<<"Nombre moyen d'adjacences par espèce ancestrale : "<<nb_aa_tot/float(Adj_ancestrales.size())<<endl;
   cout<<"Nb min = "<<nb_aa_min<<" ; nb max = "<<nb_aa_max<<endl;
   cout<<endl;
   cout<<"Nombres d'évènements :"<<endl;
   cout<<"\t Nb de créations : "<<nb_Crea<<" ; (nb_arbres_adj_tot - nb_classes="<<nb_arbres_adj_tot - nb_classes<<")"<<endl;
   cout<<"\t Nb de cassures : "<<nb_Bk<<endl;
   cout<<"\t Nb de duplication d'un gène : "<<nb_GDup<<" (compté par DeCo) ; "<<nb_GDup_arbres<<" (ds les arbres réconciliés)"<<endl;
   cout<<"\t Nb de duplication de deux gènes : "<<nb_ADup<<endl;
   cout<<endl;
   cout<<"Nombres de gènes ancestraux comptés ds les arbres réconciliés (noeuds de spéciation) :"<<nb_Spe_arbres<<endl;
   cout<<"Nombres de gènes ancestraux retrouvés appartenant à des adjacences : "<<nb_genes_a<<endl;
   cout<<"\nDétail par espèce ancestrale :"<<endl;
   cout<<"------------------------------"<<endl;
   map<int,int>::iterator it_m;
   for(it_m=espece_nb_genes.begin();it_m!=espece_nb_genes.end();it_m++)
      try{
	 string nom=S->getNode((*it_m).first)->getName();
	 cout<<nom<<" : "<<(*it_m).second<<" gènes - "<<Adj_ancestrales[(*it_m).first].size()<<" adjacences (dans arbres réconciliés : "<<especes_ancestrales_nb_gene[(*it_m).first]<<" gènes)."<<endl;
      }
      catch(exception e){
	 //cout<<e.what();
	 cout<<"Espèce n°"<<(*it_m).first<<" : "<<(*it_m).second<<" gènes - "<<Adj_ancestrales[(*it_m).first].size()<<" adjacences (dans arbres réconciliés : "<<especes_ancestrales_nb_gene[(*it_m).first]<<" gènes)."<<endl;
      }

   cout<<"\nPour les espèces actuelles :"<<endl;
   cout<<"----------------------------"<<endl;
   for(it_m=especes_actuelles_nb_gene.begin();it_m!=especes_actuelles_nb_gene.end();it_m++)
      try{
	 string nom=S->getNode((*it_m).first)->getName();
	 cout<<nom<<" : "<<especes_actuelles_nb_gene[(*it_m).first]<<" gènes dans arbres réconciliés."<<endl;
      }
      catch(exception e){
	 //cout<<e.what();
	 cout<<"Espèce n°"<<(*it_m).first<<" : "<<especes_actuelles_nb_gene[(*it_m).first]<<" gènes dans arbres réconciliés."<<endl;
      }

   cout<<"\nEn ce qui concerne les pertes ("<<nb_Per_arbres<<" au total) :"<<endl;
   cout<<"-------------------------------------------"<<endl;
   for(it_m=especes_nb_pertes.begin();it_m!=especes_nb_pertes.end();it_m++)
      try{
	 string nom=S->getNode((*it_m).first)->getName();
	 cout<<nom<<" : "<<especes_nb_pertes[(*it_m).first]<<" pertes dans arbres réconciliés."<<endl;
      }
      catch(exception e){
	 //cout<<e.what();
	 cout<<"Espèce n°"<<(*it_m).first<<" : "<<especes_nb_pertes[(*it_m).first]<<" pertes dans arbres réconciliés."<<endl;
      }
	
   map<string, int>::iterator it_msi;
   // for (it_msi=genes_ancestraux_nb_adj.begin();it_msi!=genes_ancestraux_nb_adj.end();it_msi++)
   //    {
   // 	 cout<<(*it_msi).first<< " : ";
   // 	 cout<<(*it_msi).second<<" adjacences"<<endl;
   //    }

   cout<<"\nNombre moyen d'adjacences par gène ancestral : "<<nb_adj_par_geneA_tot/float(genes_ancestraux_nb_adj.size())<<" ("<<genes_ancestraux_nb_adj.size()<<" gènes ancestraux)"<<endl;
   cout<<"Nb min = "<<nb_adj_par_geneA_min<<" ; nb max = "<<nb_adj_par_geneA_max<<endl;
   cout<<"Nb de gènes avec plus de deux adj = "<<nb_adj_par_geneA_sup2<<" ; ratio = "<<float(nb_adj_par_geneA_sup2*100.00)/float(genes_ancestraux_nb_adj.size())<<"%"<<endl;
   cout<<endl;

   /*** Création du fichier TULIP des gènes dupliqués ensemble ***/
   map<int,vector<adjacence> > Dup_ancestrales;
   cout<<"Lecture du fichier de SORTIE des gènes dupliqués ensemble "<<fic_SORTIE_dup<<endl;
   lectureFicRelBin(fic_SORTIE_dup, Dup_ancestrales);
   
   // cout<<"Dup_ancestrales :"<<endl;
   // afficheMap(Dup_ancestrales);

   // vector<adjacence>::iterator it_v;
   // set<string> genes;
   // for(it_v=DUP.begin();it_v!=DUP.end();it_v++)
   //    {
   // 	 genes.insert((*it_v).gene1);
   // 	 genes.insert((*it_v).gene2);
   //    }
   // cout<<"\t Nb de duplication de deux gènes : "<<nb_ADup<<endl;
   // cout<<"\t Nb de gènes différents : "<<genes.size()<<endl;

   // generationFicTulip(fic_arbre+"_SORTIE_dup_avant.tlp",DUP);
   // //On ne va garder que les composantes connexes (DUP est modifié) :
   // vector<adjacence> DUP_sup2 = composantesConnexes(DUP);
   
   // cout<<"\t Nb d'arêtes du graphe (après retrait des compo de taille 2) : "<<DUP_sup2.size()<<endl<<endl;
   // generationFicTulip(fic_SORTIE_dup,DUP_sup2);

   /** On parcours Dup_ancestrales pour éliminer les composantes connexes de taille 2 dans chaque espece **/

   //Inscription du nb de gènes dupliqués ensemble sur les branches de
   //l'arbre des espèces et affichage écran
   for(i=1;i<=S->getNumberOfNodes();i++) {
     cout<<"Espèce n°"<<i<<" ";
     try {
       cout<<S->getNode(i)->getName()<<" ";
     }
     catch(exception e){}

     if (Dup_ancestrales.find(i)!=Dup_ancestrales.end()) {
       S->getNode(i)->setDistanceToFather(Dup_ancestrales[i].size());
       S->getNode(i)->setDistanceToFather(Dup_ancestrales[i].size());
       cout<<" dup sur branche au dessus="<<Dup_ancestrales[i].size()<<endl;
     }
     else {
       S->getNode(i)->setDistanceToFather(0);
       S->getNode(i)->setDistanceToFather(0);
       cout<<" dup sur branche au dessus=0"<<endl;
     }
   }

   for(it_AA=Dup_ancestrales.begin();it_AA!=Dup_ancestrales.end();it_AA++)
      {
	 vector<adjacence> v=(*it_AA).second;
	 // cout<<"J'écris "<<v.size()<<" duplications sur la branche de "<<(*it_AA).first<<endl;
	 // S->getNode((*it_AA).first)->setBranchProperty(NAD,(double)v.size());
	 // S->getNode((*it_AA).first)->setBranchProperty(esp,BppString(c.to_s(v.size())));
	 string nom;
	 try{
	    nom=S->getNode((*it_AA).first)->getName();
	 }
	 catch(exception e){
	    //cout<<e.what();
	    nom="Espece "+c.to_s((*it_AA).first);
	 }
	 cout<<endl<<nom<<" : ";
	 (*it_AA).second = composantesConnexes(v);
      }
   cout<<endl;

   /* don't use this for now
   //On l'écrit forcément en newick car en nhx je n'arrive pas à
   //accéder à l'info que j'ajoute (NAD) avec le viewer que j'utilise
   //(archeoptérix)
   cout<<"\nÉcriture du fichier d'arbre d'espèce avec nb dupli sur branches"<<endl;
   string fic_especes2=fic_especes+"_annote";
   Newick * newickWriter = new Newick(); 
   newickWriter->enableExtendedBootstrapProperty(esp);
   newickWriter->write(*S,fic_especes2.c_str());
   //J'écris quand même un fichier nhx au cas où qqun sait comment le lire
   if(OUTPUT_FORMAT==1)
      {
	 string fic_especes3=fic_especes+"_annote.nhx";
	 Nhx * nhxWriter = new Nhx(true);
	 cout<<"Je m'apprête à écrire le format NHX de l'arbre des espèces"<<endl;
	 nhxWriter->registerProperty(Nhx::Property("Nb_Adj_Dup", NAD, true, 2)); 
	 nhxWriter->write(*S,fic_especes3.c_str());
      }
   */
   
   cout<<"Affichage distribution taille des composantes connexes :"<<endl;
   afficheMap(distrib_taille_compo_connexe);


   /*** Génération des fichiers TULIP par espèce ***/
   /* don't use this either (for now)
   //           Les gènes dupliqués ensembles :
   genereFicTULIPparEspece(fic_arbre, "TULIP_DupAncestrales", S, Dup_ancestrales);
   //           Les adjacences ancestrales :
   genereFicTULIPparEspece(fic_arbre, "TULIP_AdjAncestrales", S, Adj_ancestrales);
   */

   //
   // FIN
   //
   cout<<"\nProgramme terminé !!\n"<<endl;
}
